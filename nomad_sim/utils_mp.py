#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import absolute_import
from __future__ import division

__author__ = "Angelo Ziletti"
__copyright__ = "Copyright 2017, The NOMAD Project"
__maintainer__ = "Angelo Ziletti"
__email__ = "ziletti@fhi-berlin.mpg.de"
__date__ = "18/07/17"

import os
import logging
import sys
import stat
import json
import tarfile

import warnings 
from pint import UnitRegistry
# disable DeprecationWarning for scikit learn
warnings.filterwarnings('ignore', category=DeprecationWarning)
# disable warnings from pint
logging.getLogger("pint").setLevel(logging.ERROR)

import nomadcore.unit_conversion.unit_conversion as uc
import nomadcore.parser_backend as parser
import numpy as np
import multiprocessing

from nomad_sim.config import configs

try:
    hdlr = logging.FileHandler(configs["output_file"], mode='a')
except:
    hdlr = logging.FileHandler(configs["output_file"], mode='w')

ureg = UnitRegistry(os.path.normpath(configs["ureg_file"]))
level = logging.getLevelName(configs["log_level_general"])

logger = logging.getLogger(__name__)
logger.setLevel(level)
logging.basicConfig(level=level)
FORMAT = "%(levelname)s: %(message)s"
formatter = logging.Formatter(fmt=FORMAT)
handler = logging.StreamHandler()
handler.setFormatter(formatter)
hdlr.setFormatter(formatter)
logger.addHandler(handler)
logger.addHandler(hdlr)
logger.setLevel(level)
logger.propagate = False

__metainfopath__ = configs["meta_info_file"]


def split_list(l, n_splits):
    """Split l in n_split. Deals with unevenly sized chunks"""
    chunks_list = [item.tolist() for item in np.array_split(np.asarray(l), n_splits)]
    return chunks_list
    

def collect_desc_folders(job_number, desc_folder, desc_file, remove=True):
    """Move the contect of desc_folders from parall processing to a single .tar.gz file"""
    desc_file = os.path.normpath(os.path.join(desc_folder, desc_file))

    logger.info("Collecting descriptor folder: {0}".format(desc_folder))
#    desc_file = os.path.normpath(os.path.join(desc_folder, desc_file))

    desc_file_with_ext = os.path.normpath(os.path.join(desc_folder, desc_file)) +'.tar.gz'
    tar = tarfile.open(desc_file_with_ext, 'w:gz')

    tmp_files = []    
    for i in range(job_number):        
        desc_file_i = desc_file + '_' + str(i) +'.tar.gz'
                
        # open archive
        archive_i = tarfile.open(desc_file_i, 'r')
        archive_i.extractall(desc_folder)

        for member in archive_i.getmembers():
            # for file list I need to member names only
            # because we need to erase the tmp files in desc_folder
            tmp_file_no_path = member.name.rsplit("/")[-1]
            tmp_file = os.path.abspath(os.path.normpath(os.path.join(desc_folder, tmp_file_no_path)))
            tmp_files.append(tmp_file)

            member.name = os.path.abspath(os.path.normpath(os.path.join(desc_folder, member.name)))
            tar.add(member.name)

    tar.close()
    
    if remove:
        for i in range(job_number):        
            desc_file_i = desc_file + '_' + str(i) +'.tar.gz'
            os.remove(desc_file_i)
        for tmp_f in tmp_files:
            try:
                os.remove(tmp_f)
            except:
                 logger.debug("Could not remove file {0}".format(tmp_f))

def dispatch_jobs(target, data, job_number, desc_folder, desc_file):
    slices = split_list(data, job_number)
    jobs = []

    desc_file = os.path.normpath(os.path.join(desc_folder, desc_file))

    for i, s in enumerate(slices):        
        desc_file_i = desc_file + '_' + str(i) +'.tar.gz'
#        j = multiprocessing.Process(target=mp_calc_descriptor, args=(i, s))
        j = multiprocessing.Process(target=target, args=(i, s, desc_folder, desc_file_i))
        jobs.append(j)

    for j in jobs:
        j.start()

    for j in jobs:
        j.join()
        
        
    
    