#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Includes function that reads json files and classifies the stuctures 
included on the basis of encyclopedia and the calculated in the preprocessor 
space_group and normalized_wyckoff, and returns a dictionary with key - json file
 name and value - classification_name (with labels in the same format as 
in the read_prototypes function).
"""

from __future__ import absolute_import

__author__ = "Daria M. Tomecka"
__copyright__ = "Copyright 2017, The NOMAD Project"
__maintainer__ = "Daria M. Tomecka"
__email__ = "tomeckadm@gmail.com;"
__date__ = "18/05/17"

import soap
import sys
import ase.io
import json
import numpy as np
import pickle
import h5py
import time
import datetime
import os
import logging

from nomad_sim.wrappers import get_json_list
from nomad_sim.wrappers import plot, logger
from nomad_sim.utils_crystals import get_spacegroup
from nomad_sim.utils_crystals import create_supercell
from nomad_sim.wrappers import get_json_list
from nomad_sim.nomad_structures import NOMADStructure
from soap.tools import AseConfig


#prototypes_file = os.path.normpath("/nomad-lab-base/analysis-tools/structural-similarity/python-modules/nomad_sim/structure_types.py")

import sys
#print(sys.path)
sys.path.append('/home/beaker/py3k/lib/python3.5/')

import Nomad
import logging
from Nomad.Preprocessing.System import structure, preprocessormaterial3d
from Nomad import ProcessingEngine

#LOGGER = logging.getLogger(__name__)
#logging.basicConfig(level=logging.ERROR)

json_list = beaker.json_list


def classify_by_norm_wyckoff(json_list):
    protos = {}
    for json_file_name in json_list:
        try:  
            #try:
            #    simulation_cell = json_file_name.get('lattice_vectors')
            #    labels = json_file_name.get('atom_labels')
            #    atom_pos = json_file_name.get('atom_positions')
            #    if not simulation_cell or not labels or not atom_pos:
            #        raise Exception("Parsed invalid geometry for space group %d, %s" % (spaceGroupNr, protoDict))

                proc = ProcessingEngine(mode="debug")
                results = proc.process(json_file_name)

                preprocessor = list(results.values())
                if len(preprocessor) == 1:
                    preprocessor = preprocessor[0]
                    spg_nr = preprocessor["space_group"]
                    norm_wyckoff = preprocessor["normalized_wyckoff"]
                    proto2 = preprocessor["structure_type"]
                    protoDict = structure.get_structure_type(spg_nr, norm_wyckoff)
                    #proto = '%d-%s' % (spg_nr, proto2)
                    if protoDict is None:
                        proto = "%d-_" % spg_nr
                    else:
                        #if protoDict.get("Notes","") not in ["", "_", "-", "–"]:
                        #    proto = '%d-%s' % (spg_nr, protoDict)
                        #else:
                        #proto = '%d-%s' % (spg_nr, protoDict)
                        proto = '%d-%s' % (spg_nr, protoDict.get("Prototype","-"))
                    protos[json_file_name] = proto
                else:
                    logging.error("failed for %r",json_file_name)
        except:
            logging.exception("failure while computing for %r",json_file_name)
            
           
    return protos

#classify_by_norm_wyckoff(json_list)
            
