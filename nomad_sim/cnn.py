# encoding: UTF-8
#!/usr/bin/env python
from __future__ import absolute_import
from __future__ import division
#from __future__ import print_function

__author__ = "Angelo Ziletti"
__copyright__ = "Copyright 2016, The NOMAD Project"
__maintainer__ = "Angelo Ziletti"
__email__ = "ziletti@fhi-berlin.mpg.de"
__date__ = "21/10/16"


import tensorflow as tf
import itertools
from keras.layers import Input, Dense, Convolution2D, MaxPooling2D, UpSampling2D
from keras import backend as K
K.set_image_dim_ordering('th')
from keras.models import Model
from keras.callbacks import TensorBoard
from keras.regularizers import l2
from nomad_sim.tensorflowvisu import tf_format_mnist_images
#from keras.datasets import cifar10
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import CSVLogger, ModelCheckpoint, EarlyStopping
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Convolution2D, MaxPooling2D
from keras.layers import Convolution3D, MaxPooling3D
import matplotlib.pyplot as plt
from sklearn.metrics import classification_report, confusion_matrix
from keras.optimizers import SGD, Adam
from keras.utils import np_utils
from keras.models import model_from_json
from keras_tqdm import TQDMCallback, TQDMNotebookCallback
import cPickle as pickle
import sys
import math
import os
import logging
import pandas as pd
import numpy as np

tf.set_random_seed(0)

from nomad_sim.config import configs

try:
    hdlr = logging.FileHandler(configs["output_file"], mode='a')
except:
    hdlr = logging.FileHandler(configs["output_file"], mode='w')


level = logging.getLevelName(configs["log_level_general"])

logger = logging.getLogger(__name__)
logger.setLevel(level)
logging.basicConfig(level=level)
FORMAT = "%(levelname)s: %(message)s"
formatter = logging.Formatter(fmt=FORMAT)
handler = logging.StreamHandler()
handler.setFormatter(formatter)
hdlr.setFormatter(formatter)
logger.addHandler(handler)
logger.addHandler(hdlr)
logger.setLevel(level)
logger.propagate = False

def weight_variable(shape):
  initial = tf.truncated_normal(shape, stddev=0.1)
  return tf.Variable(initial)

def bias_variable(shape):
  initial = tf.constant(0.1, shape=shape)
  return tf.Variable(initial)

def model_shallow_cnn_struct_recognition(conv2d_filters, kernel_sizes, max_pool_strides,
    n_rows, n_columns, img_channels, n_classes):
    """Shallow convolutional neural network model for crystal structure recognition.
    
    Examples
    --------
    Suggested parameters:: 

        partial_model_architecture = partial(
        model_shallow_cnn_struct_recognition,
        conv2d_filters=[32, 16, 8],
        kernel_sizes=[3, 3, 3], 
        max_pool_strides=[2, 2])

    """

    N_CONV_2D = 3
    N_POOL = 2
    if not len(conv2d_filters) == N_CONV_2D: raise Exception( "Wrong number of filters. Give a list of {0} numbers.".format(N_CONV_2D))
    if not len(kernel_sizes) == N_CONV_2D: raise Exception( "Wrong number of kernel sizes. Give a list of {0} numbers.".format(N_CONV_2D))
    if not len(max_pool_strides) == N_POOL: raise Exception( "Wrong number of max pool strides. Give a list of {0} numbers.".format(N_POOL))

    # good model for 64x64
    #function defining the architecture of defined CNN
    model = Sequential()
    model.add(Convolution2D(conv2d_filters[0], kernel_sizes[0], kernel_sizes[0], activation='relu', border_mode='same',init='orthogonal', bias = True, input_shape=(img_channels, n_rows, n_columns)))
    model.add(MaxPooling2D(pool_size=(2, 2), strides = (2,2)))
    model.add(Convolution2D(conv2d_filters[1], kernel_sizes[1], kernel_sizes[1], activation='relu', border_mode='same',init='orthogonal', bias = True))
    model.add(MaxPooling2D(pool_size=(2, 2), strides = (2,2)))
    model.add(Convolution2D(conv2d_filters[2], kernel_sizes[2], kernel_sizes[2], activation='relu', border_mode='same',init='orthogonal', bias = True))

    model.add(Dropout(0.25))
    model.add(Flatten())
    model.add(Dense(32, activation='relu', bias = True))
    model.add(Dense(n_classes))
    model.add(Activation('softmax'))
    
    return model
    
    
def model_deep_cnn_struct_recognition(conv2d_filters, kernel_sizes, max_pool_strides,
    hidden_layer_size,
    n_rows, n_columns, img_channels, n_classes):
    """Deep convolutional neural network model for crystal structure recognition.
    
    Examples
    --------
    Suggested parameters:: 

        partial_model_architecture = partial(
        model_deep_cnn_struct_recognition,
        conv2d_filters=[32, 16, 12, 12, 8, 8],
        kernel_sizes=[3, 3, 3, 3, 3, 3], 
        max_pool_strides=[2, 2])

    """

    N_CONV_2D = 6
    N_POOL = 2
    if not len(conv2d_filters) == N_CONV_2D: raise Exception( "Wrong number of filters. Give a list of {0} numbers.".format(N_CONV_2D))
    if not len(kernel_sizes) == N_CONV_2D: raise Exception( "Wrong number of kernel sizes. Give a list of {0} numbers.".format(N_CONV_2D))
    if not len(max_pool_strides) == N_POOL: raise Exception( "Wrong number of max pool strides. Give a list of {0} numbers.".format(N_POOL))

    # good model for 64x64
    # function defining the architecture of defined CNN 
    model = Sequential()
    model.add(Convolution2D(conv2d_filters[0], kernel_sizes[0], kernel_sizes[0], name='convolution2d_1',
        activation='relu', border_mode='same',init='orthogonal', bias = True, input_shape=(img_channels, n_rows, n_columns)))
    model.add(Convolution2D(conv2d_filters[1], kernel_sizes[1], kernel_sizes[1], name='convolution2d_2',
        activation='relu', border_mode='same',init='orthogonal', bias = True))
    model.add(MaxPooling2D(pool_size=(2, 2), strides = (2,2), name='maxpooling2d_1'))
    model.add(Convolution2D(conv2d_filters[2], kernel_sizes[2], kernel_sizes[2], name='convolution2d_3',
        activation='relu', border_mode='same',init='orthogonal', bias = True))
    model.add(Convolution2D(conv2d_filters[3], kernel_sizes[3], kernel_sizes[3], name='convolution2d_4',
        activation='relu', border_mode='same',init='orthogonal', bias = True))
    model.add(MaxPooling2D(pool_size=(2, 2), strides = (2,2), name='maxpooling2d_2'))
    model.add(Convolution2D(conv2d_filters[4], kernel_sizes[4], kernel_sizes[4], name='convolution2d_5',
        activation='relu', border_mode='same',init='orthogonal', bias = True))
    model.add(Convolution2D(conv2d_filters[5], kernel_sizes[5], kernel_sizes[5], name='convolution2d_6',
        activation='relu', border_mode='same',init='orthogonal', bias = True))

    model.add(Dropout(0.25, name='dropout_1'))
    model.add(Flatten(name='flatten_1'))
    model.add(Dense(hidden_layer_size, name='dense_1', activation='relu', bias = True))


    model.add(Dense(n_classes, name='dense_2'))
    model.add(Activation('softmax', name='activation_1'))
    
    return model

def model_deep_cnn_struct_recognition_32x32(conv2d_filters, kernel_sizes, max_pool_strides,
    n_rows, n_columns, img_channels, n_classes):
    """Deep convolutional neural network model for crystal structure recognition.
    
        Works well for 32x32 images, but does not achieve 100% 
        accuracy on 64x64 images.
        
    Examples
    --------
    Suggested parameters:: 

        partial_model_architecture = partial(
        model_deep_cnn_struct_recognition,
        conv2d_filters=[32, 16, 8, 8],
        kernel_sizes=[5, 5, 5, 5], 
        max_pool_strides=[2, 2])

    """

    N_CONV_2D = 4
    N_POOL = 1
    if not len(conv2d_filters) == N_CONV_2D: raise Exception( "Wrong number of filters. Give a list of {0} numbers.".format(N_CONV_2D))
    if not len(kernel_sizes) == N_CONV_2D: raise Exception( "Wrong number of kernel sizes. Give a list of {0} numbers.".format(N_CONV_2D))
    if not len(max_pool_strides) == N_POOL: raise Exception( "Wrong number of max pool strides. Give a list of {0} numbers.".format(N_POOL))
        
    # good model for 32x32
    model = Sequential()
    model.add(Convolution2D(conv2d_filters[0], kernel_sizes[0], kernel_sizes[0], activation='relu', border_mode='same',init='orthogonal', bias = True, input_shape=(img_channels, n_rows, n_columns)))
    model.add(Convolution2D(conv2d_filters[1], kernel_sizes[1], kernel_sizes[1], activation='relu', border_mode='same',init='orthogonal', bias = True))
    model.add(MaxPooling2D(pool_size=(2, 2), strides = (2,2)))
    model.add(Convolution2D(conv2d_filters[2], kernel_sizes[2], kernel_sizes[2], activation='relu', border_mode='same',init='orthogonal', bias = True))
    model.add(Convolution2D(conv2d_filters[3], kernel_sizes[3], kernel_sizes[3], activation='relu', border_mode='same',init='orthogonal', bias = True))
    
    model.add(Dropout(0.25))
    model.add(Flatten())
    model.add(Dense(128, activation='relu', bias = True))
    model.add(Dense(n_classes))
    model.add(Activation('softmax'))
    
    return model
    
def model_architecture_3d(dim1, dim2, dim3, img_channels, n_classes):

    # shallow model  for 64x64x64
#    function defining the architecture of defined 3D-CNN
    model = Sequential()
    model.add(Convolution3D(32, 3, 3, 3, activation='relu', border_mode='same',init='orthogonal', bias = True, input_shape=(img_channels, dim1, dim2, dim3)))
    model.add(MaxPooling3D(pool_size=(2, 2, 2), strides = (2, 2, 2)))
    model.add(Convolution3D(16, 3, 3, 3, activation='relu', border_mode='same',init='orthogonal', bias = True))
    model.add(MaxPooling3D(pool_size=(2, 2, 2), strides = (2, 2, 2)))
    model.add(Convolution3D(8, 3, 3, 3, activation='relu', border_mode='same',init='orthogonal', bias = True))
    
    model.add(Dropout(0.25))
    model.add(Flatten())
    model.add(Dense(32, activation='relu', bias = True))
    model.add(Dense(n_classes))
    model.add(Activation('softmax'))
    
    return model
    
def train_cnn_keras(data_set, data_set_test,
        batch_size, n_classes, nb_epoch,
        input_dims, img_channels, data_augmentation, 
        partial_model_architecture,
        checkpoint_dir=None, checkpoint_filename=None,
        training_log_file='training.log',
        early_stopping=False):

    filename_no_ext = os.path.abspath(
        os.path.normpath(
            os.path.join(
                checkpoint_dir,
                checkpoint_filename)))   

    training_log_file_path = os.path.abspath(
        os.path.normpath(
            os.path.join(
                checkpoint_dir,
                training_log_file)))   
                
    x_train = data_set.train.images
    y_train = data_set.train.labels
    x_val = data_set.val.images
    y_val = data_set.val.labels
    x_test = data_set_test.images
    y_test = data_set_test.labels    

    # TensorBoard works only with Tensorflow backend
#    tb_callback = TensorBoard(log_dir=checkpoint_dir, histogram_freq=0, 
#        write_graph=True, write_images=True)


#    TensorFlow: [batch, width,height, channels]
#    Theano: [batch,channels, width, height]
            
    #reshapping it according to the keras rule
    if len(input_dims) == 2:
        x_train = x_train.reshape(x_train.shape[0], 1, input_dims[0], input_dims[1]) 
        x_val = x_val.reshape(x_val.shape[0], 1, input_dims[0], input_dims[1]) 
        x_test = x_test.reshape(x_test.shape[0], 1, input_dims[0], input_dims[1])
 
    else:
        raise Exception("Wrong number of dimensions.")
        
    logger.info('Loading datasets.')

    logger.debug('x_train shape: {0}'.format(x_train.shape))
    logger.debug('y_train shape: {0}'.format(y_train.shape))
    logger.debug('x_val shape: {0}'.format(x_val.shape))
    logger.debug('y_val shape: {0}'.format(y_val.shape))    
    logger.debug('x_test shape: {0}'.format(x_test.shape))
    logger.debug('y_test shape: {0}'.format(y_test.shape))
    logger.debug('Training samples: {0}'.format(x_train.shape[0]))
    logger.debug('Validation samples: {0}'.format(x_val.shape[0]))
    logger.debug('Test samples: {0}'.format(x_test.shape[0]))

    x_train = x_train.astype('float32')
    x_val = x_val.astype('float32')
    x_test = x_test.astype('float32')

    # check if the image is already normalized    
    logger.debug('Maximum value in x_train for the 1st image (to check normalization): {0}'.format(np.amax(x_train[0,:,:,:])))
    logger.debug('Maximum value in x_val for the 1st image (to check normalization): {0}'.format(np.amax(x_val[0,:,:,:])))
    logger.debug('Maximum value in x_test for the 1st image (to check normalization): {0}'.format(np.amax(x_test[0,:,:,:])))

    #x_train /= 255   #normalizing the data
    #x_test /= 255

    # convert class vectors to binary class matrices
    y_train = np_utils.to_categorical(y_train, n_classes)
    y_val = np_utils.to_categorical(y_val, n_classes)
    y_test = np_utils.to_categorical(y_test, n_classes)
    
    logger.info('Loading and formatting of data completed.')
    
    # return the Keras model    
    model = partial_model_architecture(
        n_rows=input_dims[0], 
        n_columns=input_dims[1],
        img_channels=img_channels,
        n_classes=n_classes)

    
    callbacks = []
    csv_logger = CSVLogger(training_log_file_path, separator=',', append=False)
    callbacks.append(csv_logger)

    # if you are running on Notebook        
    if configs["isBeaker"] == "True":
        callbacks.append(TQDMNotebookCallback(leave_inner=True, leave_outer=True))
    else:
        callbacks.append(TQDMCallback(leave_inner=True, leave_outer=True))
    
    if early_stopping:
        early_stopping = EarlyStopping(monitor='val_loss',
                                  min_delta=0.001,
                                  patience=1,
                                  verbose=0, mode='auto')
                                  
        callbacks.append(early_stopping)

    adam = Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
#    adam = Adam(lr=0.0003, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
#    adam = Adam(lr=0.0001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)

    model.compile(loss='categorical_crossentropy',
#                  optimizer=sgd,
                  optimizer=adam,
                  metrics=['accuracy'])


    if not data_augmentation:
        logger.debug('Not using data augmentation.')


        model.fit(x_train, y_train,
                  batch_size=batch_size,
                  nb_epoch=nb_epoch,
                  validation_data=(x_val, y_val),
                  shuffle=True,
                  verbose=0,
                  callbacks=callbacks)
                  
        # TensorBoard works only with Tensorflow backend                  
#                  callbacks=[tb_callback])

    else:
        logger.debug('Using real-time data augmentation.')

        # this will do preprocessing and realtime data augmentation
        datagen = ImageDataGenerator(
            featurewise_center=False,  # set input mean to 0 over the dataset
            samplewise_center=False,  # set each sample mean to 0
            featurewise_std_normalization=False,  # divide inputs by std of the dataset
            samplewise_std_normalization=False,  # divide each input by its std
            zca_whitening=False,  # apply ZCA whitening
            rotation_range=0,  # randomly rotate images in the range (degrees, 0 to 180)
            shear_range = 0.0,  # value in radians, equivalent to 20 deg
            #zoom_range = [1/1, 1],   #same as in NIPS 2015 paper.
            width_shift_range=0.0,  # randomly shift images horizontally (fraction of total width)
            height_shift_range=0.0,  # randomly shift images vertically (fraction of total height)
            horizontal_flip=True,  # randomly flip images
            vertical_flip=True)  # randomly flip images

        # compute quantities required for featurewise normalization
        # (std, mean, and principal components if ZCA whitening is applied)
        # Not required as it is Only required if featurewise_center or featurewise_std_normalization or zca_whitening.
#        datagen.fit(x_train) 

        # fit the model on the batches generated by datagen.flow() and save the loss and acc data history in the hist variable
        #filepath = "/home/310251680/work/scripts/imageCLEF_reprod/saved_models/model_imgCLEF_shallow_ep_10_weights.hdf5"
        #save_model_per_epoch = ModelCheckpoint(filepath, monitor='val_acc', verbose=0, save_best_only=False, save_weights_only=True, mode='auto')
                
        history = model.fit_generator(datagen.flow(x_train, y_train,
                            batch_size=batch_size),
                            samples_per_epoch=x_train.shape[0],
                            nb_epoch=nb_epoch,
                            validation_data=(x_val, y_val))                            
        
        #saving the history of training
        np.save(filename_no_ext + ".npy", history)
        logger.debug("History saved to disk. Filename: {0}.npy".format(filename_no_ext))

                    
    # serialize model to JSON
    model_json = model.to_json()
    with open(filename_no_ext + ".json", "w") as json_file:
        json_file.write(model_json)
    # serialize weights to HDF5
    model.save_weights(filename_no_ext + ".h5")
    logger.info("Model saved to disk.")
    logger.debug("Filename: {0}".format(filename_no_ext))
    
    
def train_3d_cnn_keras(data_set, data_set_test, batch_size, n_classes, nb_epoch, 
        input_dims, img_channels, data_augmentation, 
        checkpoint_dir=None, checkpoint_filename=None):

    filename_no_ext = os.path.abspath(
            os.path.normpath(
                os.path.join(
                    checkpoint_dir,
                    checkpoint_filename))) 
                    
    #function to run the actual test
    # the data, shuffled and split between train and test sets
    
    x_train = data_set.train.images
    y_train = data_set.train.labels
    x_val = data_set.val.images
    y_val = data_set.val.labels
    x_test = data_set_test.images
    y_test = data_set_test.labels    
    
    #reshapping it according to the keras rule
    if len(input_dims) == 3:
        x_train = x_train.reshape(x_train.shape[0], 1, input_dims[0], input_dims[1], input_dims[2]) 
        x_val = x_val.reshape(x_val.shape[0], 1, input_dims[0], input_dims[1], input_dims[2]) 
        x_test = x_test.reshape(x_test.shape[0], 1, input_dims[0], input_dims[1], input_dims[2])
    else:
        raise Exception("Wrong number of dimensions.")
        
    logger.info('Loading datasets.')

    logger.debug('x_train shape: {0}'.format(x_train.shape))
    logger.debug('y_train shape: {0}'.format(y_train.shape))
    logger.debug('x_val shape: {0}'.format(x_val.shape))
    logger.debug('y_val shape: {0}'.format(y_val.shape))    
    logger.debug('x_test shape: {0}'.format(x_test.shape))
    logger.debug('y_test shape: {0}'.format(y_test.shape))
    logger.debug('Training samples: {0}'.format(x_train.shape[0]))
    logger.debug('Validation samples: {0}'.format(x_val.shape[0]))
    logger.debug('Test samples: {0}'.format(x_test.shape[0]))


    x_train = x_train.astype('float32')
    x_val = x_val.astype('float32')
    x_test = x_test.astype('float32')

    # check if the image is already normalized    
    logger.debug('Maximum value in x_train for the 1st image (to check normalization): {0}'.format(np.amax(x_train[0,:,:,:])))
    logger.debug('Maximum value in x_val for the 1st image (to check normalization): {0}'.format(np.amax(x_val[0,:,:,:])))
    logger.debug('Maximum value in x_test for the 1st image (to check normalization): {0}'.format(np.amax(x_test[0,:,:,:])))

    #x_train /= 255   #normalizing the data
    #x_test /= 255

    # convert class vectors to binary class matrices
    y_train = np_utils.to_categorical(y_train, n_classes)
    y_val = np_utils.to_categorical(y_val, n_classes)
    y_test = np_utils.to_categorical(y_test, n_classes)
    
    logger.info('Loading and formatting of data completed.')
    
    #load the model defined in model_architecture function
    model = model_architecture_3d(input_dims[0], input_dims[1], input_dims[2], img_channels, n_classes)

    logger.debug(model.summary())

    # training the model using SGD + momentum
#    sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)

#    adam = Adam(lr=0.005, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
    adam = Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
#    adam = Adam(lr=0.0003, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
#    adam = Adam(lr=0.0001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)

    model.compile(loss='categorical_crossentropy',
#                  optimizer=sgd,
                  optimizer=adam,
                  metrics=['accuracy'])

  

    if not data_augmentation:
        logger.info('Not using data augmentation.')
        model.fit(x_train, y_train,
                  batch_size=batch_size,
                  nb_epoch=nb_epoch,
                  validation_data=(x_val, y_val),
                  shuffle=True)
    else:
        logger.info('Using real-time data augmentation.')

        # this will do preprocessing and realtime data augmentation
        datagen = ImageDataGenerator(
            featurewise_center=False,  # set input mean to 0 over the dataset
            samplewise_center=False,  # set each sample mean to 0
            featurewise_std_normalization=False,  # divide inputs by std of the dataset
            samplewise_std_normalization=False,  # divide each input by its std
            zca_whitening=False,  # apply ZCA whitening
            rotation_range=0,  # randomly rotate images in the range (degrees, 0 to 180)
            shear_range = 0.0,  # value in radians, equivalent to 20 deg
            #zoom_range = [1/1, 1],   #same as in NIPS 2015 paper.
            width_shift_range=0.0,  # randomly shift images horizontally (fraction of total width)
            height_shift_range=0.0,  # randomly shift images vertically (fraction of total height)
            horizontal_flip=True,  # randomly flip images
            vertical_flip=True)  # randomly flip images

        # compute quantities required for featurewise normalization
        # (std, mean, and principal components if ZCA whitening is applied)
        # Not required as it is Only required if featurewise_center or featurewise_std_normalization or zca_whitening.
#        datagen.fit(x_train) 

        # fit the model on the batches generated by datagen.flow() and save the loss and acc data history in the hist variable
        #filepath = "/home/310251680/work/scripts/imageCLEF_reprod/saved_models/model_imgCLEF_shallow_ep_10_weights.hdf5"
        #save_model_per_epoch = ModelCheckpoint(filepath, monitor='val_acc', verbose=0, save_best_only=False, save_weights_only=True, mode='auto')
                
        hist = model.fit_generator(datagen.flow(x_train, y_train,
                            batch_size=batch_size),
                            samples_per_epoch=x_train.shape[0],
                            nb_epoch=nb_epoch,
                            validation_data=(x_val, y_val))                            
        
        #saving the history of training
        #np.save('/home/ziletti/Documents/dev_dec_2016/saved_model/history_imgCLF_shallow_ep_5.npy', hist)
        #print('history saved')

    filename_no_ext = os.path.abspath(
        os.path.normpath(
            os.path.join(
                checkpoint_dir,
                checkpoint_filename)))   
                    
                    
    # serialize model to JSON
    model_json = model.to_json()
    with open(filename_no_ext + ".json", "w") as json_file:
        json_file.write(model_json)
    # serialize weights to HDF5
    model.save_weights(filename_no_ext + ".h5")
    logger.info("Saved model to disk. Name: {0}".format(filename_no_ext))
    
    

def predict_cnn_keras(data_set, n_classes, input_dims, img_channels, 
        checkpoint_dir=None, checkpoint_filename=None, batch_size=1,
        show_model_acc=True, predict_probabilities=True):
    
    filename_no_ext = os.path.abspath(
            os.path.normpath(
                os.path.join(
                    checkpoint_dir,
                    checkpoint_filename))) 
    
    if configs["log_level_general"] == "DEBUG":
        verbose=1
    else:
        verbose=0

    #loading saved model
    model_arch_file = filename_no_ext + ".json"
    model_weights_file = filename_no_ext + ".h5"
    
    json_file = open(model_arch_file, 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)
    logger.debug('Loading model weights.')
    loaded_model.load_weights(model_weights_file)
    logger.info('Model loaded correctly.')
    
    # evaluate loaded model on test data
    adam = Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
#    adam = Adam(lr=0.0003, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
    loaded_model.compile(loss='categorical_crossentropy',
                  optimizer=adam,
                  metrics=['accuracy'])
                  
    # load the data    
    x_test = data_set.images
    y_test = data_set.labels
    
    #reshapping it according to the keras rule
    if len(input_dims) == 2:
        x_test = x_test.reshape(x_test.shape[0], 1, input_dims[0], input_dims[1])
    elif len(input_dims) ==3:
        x_test = x_test.reshape(x_test.shape[0], 1, input_dims[0], input_dims[1], input_dims[2])
    else:
        raise Exception("Wrong number of dimensions.")
        
    logger.info('Loading test dataset for prediction.')

    logger.debug('x_test shape: {0}'.format(x_test.shape))
    logger.debug('Test samples: {0}'.format(x_test.shape[0]))

    x_test = x_test.astype('float32')

    # check if the image is already normalized    
    if len(input_dims) == 2:
        logger.debug('Maximum value in x_test for the 1st image (to check normalization): {0}'.format(np.amax(x_test[0,:,:,:])))
    else:
        logger.debug('Maximum value in x_test for the 1st image (to check normalization): {0}'.format(np.amax(x_test[0,:,:,:,:])))
        
    #x_test /= 255      #normalizing the data

    # convert class vectors to binary class matrices
    #y_test = np_utils.to_categorical(y_test, n_classes)
    logger.info('Loading and formatting of data completed.')

    if configs["log_level_general"] == "DEBUG":
        loaded_model.summary()
    
    logger.info('Predicting...')
        
    #compiling and calculating the score of the model again
    score = loaded_model.evaluate(x_test, y_test, batch_size=batch_size, verbose=verbose)
   
    #print (y_pred)
    if show_model_acc:
        logger.info('Model score: {0} {1}%'.format(loaded_model.metrics_names[1], score[1]*100))
        
    if predict_probabilities:
        prob_predictions = loaded_model.predict(x_test, batch_size=batch_size, verbose=verbose)
    
    #print('argmax y_pred')
    #y_pred = np.argmax(prob_predictions,axis=1)
    #print(y_pred)
    
    #prediting the labels of the test set
    y_pred  = loaded_model.predict_classes(x_test, batch_size=batch_size, verbose=verbose)
        
    #outputs the full classification report for all the classes
    #target_names = range(1,3)
    #print(classification_report(np.argmax(y_test,axis=1), y_pred))

    conf_matrix = confusion_matrix(np.argmax(y_test, axis=1), y_pred)
    np.set_printoptions(precision=2)

#    normalize=False
    
#    if normalize:
#        conf_matrix = conf_matrix.astype('float') / conf_matrix.sum(axis=1)[:, np.newaxis]
#        logger.debug("Normalized confusion matrix")
#    else:
#        logger.debug('Confusion matrix, without normalization')

    logger.debug('Confusion matrix, without normalization')

            
    logger.debug(conf_matrix)

    return prob_predictions, conf_matrix


  
def run_cnn_tensorflow(data_set, n_classes, n_rows, n_columns, batch_size=100, train=True,
        checkpoint_dir=None, n_steps=None, data_set_test=None):
    
    n_classes = int(n_classes)

        
    # neural network structure for this sample:
    #
    # · · · · · · · · · ·      (input data, 1-deep)                 X [batch, n_rows, n_columns, 1]
    # @ @ @ @ @ @ @ @ @ @   -- conv. layer 6x6x1=>6 stride 1        W1 [5, 5, 1, 6]        B1 [6]
    # ∶∶∶∶∶∶∶∶∶∶∶∶∶∶∶∶∶∶∶                                           Y1 [batch, n_rows, n_columns, 6]
    #   @ @ @ @ @ @ @ @     -- conv. layer 5x5x6=>12 stride 2       W2 [5, 5, 6, 12]        B2 [12]
    #   ∶∶∶∶∶∶∶∶∶∶∶∶∶∶∶                                             Y2 [batch, 14, 14, 12]
    #     @ @ @ @ @ @       -- conv. layer 4x4x12=>24 stride 2      W3 [4, 4, 12, 24]       B3 [24]
    #     ∶∶∶∶∶∶∶∶∶∶∶                                               Y3 [batch, 7, 7, 24] => reshaped to YY [batch, 7*7*24]
    #      \x/x\x\x/ ✞      -- fully connected layer (relu+dropout) W4 [7*7*24, 20]       B4 [20]
    #       · · · ·                                                 Y4 [batch, 20]
    #       \x/x\x/         -- fully connected layer (softmax)      W5 [20, n_classes]           B5 [n_classes]
    #        · · ·                                                  Y [batch, 20]
    
    # input X: n_rowsxn_columns grayscale images, the first dimension (None) will index the images in the mini-batch
    X = tf.placeholder(tf.float32, [None, n_rows, n_columns, 1])
    # correct answers will go here
    Y_ = tf.placeholder(tf.float32, [None, n_classes])
    # variable learning rate
    lr = tf.placeholder(tf.float32)
    # Probability of keeping a node during dropout = 1.0 at test time (no dropout) and keep_prob at training time
    keep_prob = tf.placeholder(tf.float32)
    
    # three convolutional layers with their channel counts, and a
    # fully connected layer (the last layer has n_classes softmax neurons)
    K = 6  # first convolutional layer output depth
    L = 6  # second convolutional layer output depth
    M = 6  # third convolutional layer
    N = 50  # fully connected layer
    
    W1 = weight_variable([6, 6, 1, K])  # 6x6 patch, 1 input channel, K output channels
    B1 = bias_variable([K])
    W2 = weight_variable([5, 5, K, L])
    B2 = bias_variable([L])
    W3 = weight_variable([4, 4, L, M])
    B3 = bias_variable([M])
    
    W4 = weight_variable([7 * 7 * M, N])
    B4 = bias_variable([N])
    W5 = weight_variable([N, n_classes])
    B5 = bias_variable([n_classes])    
        
    # The model
    stride = 1  # output is n_rowsxn_columns
    Y1 = tf.nn.relu(tf.nn.conv2d(X, W1, strides=[1, stride, stride, 1], padding='SAME') + B1)
    stride = 2  # output is 14x14
    Y2 = tf.nn.relu(tf.nn.conv2d(Y1, W2, strides=[1, stride, stride, 1], padding='SAME') + B2)
    stride = 2  # output is 7x7
    Y3 = tf.nn.relu(tf.nn.conv2d(Y2, W3, strides=[1, stride, stride, 1], padding='SAME') + B3)
    
    # reshape the output from the third convolution for the fully connected layer
    YY = tf.reshape(Y3, shape=[-1, 7 * 7 * M])
    
    Y4 = tf.nn.relu(tf.matmul(YY, W4) + B4)
    YY4 = tf.nn.dropout(Y4, keep_prob)
    Ylogits = tf.matmul(YY4, W5) + B5
    
    # last layer of softmax
    Y = tf.nn.softmax(Ylogits)
    
    # cross-entropy loss function (= -sum(Y_i * log(Yi)) ), normalised for batches of batch_size  images
    # TensorFlow provides the softmax_cross_entropy_with_logits function to avoid numerical stability
    # problems with log(0) which is NaN
    cross_entropy = tf.nn.softmax_cross_entropy_with_logits(Ylogits, Y_)
    cross_entropy = tf.reduce_mean(cross_entropy)*batch_size
    
    # accuracy of the trained model, between 0 (worst) and 1 (best)
    # tf.argmax gives the index of the highest entry in a tensor along some axis
    correct_prediction = tf.equal(tf.argmax(Y, 1), tf.argmax(Y_, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
    
    # matplotlib visualisation
    allweights = tf.concat(0, [tf.reshape(W1, [-1]), tf.reshape(W2, [-1]), tf.reshape(W3, [-1]), tf.reshape(W4, [-1]), tf.reshape(W5, [-1])])
    allbiases  = tf.concat(0, [tf.reshape(B1, [-1]), tf.reshape(B2, [-1]), tf.reshape(B3, [-1]), tf.reshape(B4, [-1]), tf.reshape(B5, [-1])])
    I = tf_format_mnist_images(X, Y, Y_)
    It = tf_format_mnist_images(X, Y, Y_, batch_size, lines=25)

    # determine total number of parameters
    total_parameters = 0
    for variable in tf.trainable_variables():
        # shape is an array of tf.Dimension
        shape = variable.get_shape()
        #print(shape)
        #print(len(shape))
        variable_parameters = 1
        for dim in shape:
            #print(dim)
            variable_parameters *= dim.value
        #print(variable_parameters)
        total_parameters += variable_parameters
    logger.info('Total number of trainable parameters: {0}'.format(total_parameters))

    
    # training step, the learning rate is a placeholder
    train_step = tf.train.AdamOptimizer(lr).minimize(cross_entropy)
    
    # initialize the variables we created
    init = tf.initialize_all_variables()
    # Create a saver for writing training checkpoints.
    saver = tf.train.Saver()
    sess = tf.Session()
    sess.run(init)
    

    # You can call this function in a loop to train the model, batch_size images at a time
    def training_step(step, update_train_data, update_val_test_data, save_checkpoint):

        train_acc = None
        train_error = None
        val_acc = None
        val_error = None
        test_acc = None
        test_error = None
        epoch = str(step*batch_size//data_set.train.images.shape[0]+1)

        # training on batches of batch_size images with batch_size labels
        batch_X, batch_Y = data_set.train.next_batch(batch_size)
    
        # learning rate decay
        max_learning_rate = 0.0003
        min_learning_rate = 0.0001
        decay_speed = 2000
        learning_rate = min_learning_rate + (max_learning_rate - min_learning_rate) * math.exp(-step/decay_speed)
    
        # compute training values for visualisation
        if update_train_data:
            train_acc, train_error, im_tr, w_tr, b_tr = sess.run([accuracy, cross_entropy, I, allweights, allbiases], {X: batch_X, Y_: batch_Y, keep_prob: 1.0})
            logger.info("{0}: accuracy: {1:.3f}  loss: {2:.2f} (lr: {3:.8f})".format(str(step), train_acc, train_error, learning_rate))

            
        # compute test values for visualisation
        if update_val_test_data:
            val_acc, val_error, im_v = sess.run([accuracy, cross_entropy, It], {X: data_set.val.images, Y_: data_set.val.labels, keep_prob: 1.0})
            logger.info("{0}: ********* epoch {1} *********".format(str(step), str(step*batch_size//data_set.train.images.shape[0]+1)))
            logger.info("validation accuracy: {0:.3f} validation loss: {1:.2f}" .format(val_acc, val_error))

            if data_set_test is not None:
                test_acc, test_error, im_tst = sess.run([accuracy, cross_entropy, It], {X: data_set_test.images, Y_: data_set_test.labels, keep_prob: 1.0})
                logger.info("test accuracy: {0:.3f} test loss: {1:.2f}" .format(test_acc, test_error))

            logger.info("{0}: ******************".format(str(step)))


        # the backpropagation training step
        sess.run(train_step, feed_dict={X: batch_X, Y_: batch_Y, lr: learning_rate, keep_prob: 0.75})


        if save_checkpoint:
            checkpoint_file = os.path.join(checkpoint_dir, 'checkpoint')
            logger.info(str(step) + ": Saving checkpoint.")
            saver.save(sess, checkpoint_file, global_step=step)

        return step, epoch, train_acc, train_error, val_acc, val_error, test_acc, test_error


    def fill_feed_dict(data_set, images_placeholder, labels_placeholder):
      """Fills the feed_dict for training the given step.
      Function taken from https://github.com/tensorflow/tensorflow/blob/master/tensorflow/examples/tutorials/mnist/fully_connected_feed.py
      A feed_dict takes the form of:
      feed_dict = {
          <placeholder>: <tensor of values to be passed for placeholder>,
          ....
      }
      Args:
        data_set: The set of images and labels, from input_data.read_data_sets()
        images_pl: The images placeholder, from placeholder_inputs().
        labels_pl: The labels placeholder, from placeholder_inputs().
      Returns:
        feed_dict: The feed dictionary mapping from placeholders to values.
      """
      # Create the feed_dict for the placeholders filled with the next
      # `batch size` examples.
      images_feed, labels_feed = data_set.train.next_batch(batch_size)
      feed_dict = {
          images_placeholder: images_feed,
          labels_placeholder: labels_feed,
      }
      return feed_dict


    def do_eval(sess, eval_correct, images_placeholder, labels_placeholder, data_set):
      """Runs one evaluation against the full epoch of data.
      NB: later we need to use this because we do not want to make predictions all at once,
      but we want to predict by batch, and append to make a full epoch of predictions
      Function taken from https://github.com/tensorflow/tensorflow/blob/master/tensorflow/examples/tutorials/mnist/fully_connected_feed.py
      
      Args:
        sess: The session in which the model has been trained.
        eval_correct: The Tensor that returns the number of correct predictions.
        images_placeholder: The images placeholder.
        labels_placeholder: The labels placeholder.
        data_set: The set of images and labels to evaluate, from
          input_data.read_data_sets().
      """
      
      # And run one epoch of eval.
      true_count = 0  # Counts the number of correct predictions.
      steps_per_epoch = data_set.num_examples // batch_size
      num_examples = steps_per_epoch * batch_size
      for step in xrange(steps_per_epoch):
        feed_dict = fill_feed_dict(data_set,
                                   images_placeholder,
                                   labels_placeholder)
        true_count += sess.run(eval_correct, feed_dict=feed_dict)
      precision = true_count / num_examples
      logger.info('  Num examples: %d  Num correct: %d  Precision @ 1: %0.04f' % (num_examples, true_count, precision))


    
    if train:
        step_num = []
        epoch = []
        train_acc = []
        train_error = []    
        val_acc = []
        val_error = [] 
        test_acc = []
        test_error = []     
    
        for step in range(n_steps+1): 
            step_num_, epoch_, train_acc_, train_error_, val_acc_, val_error_, test_acc_, test_error_ = training_step(step, step % 100 == 0, step % 500 == 0, step % 1000 == 0)

            if step % 100==0 :
                step_num.append(step_num_)
                epoch.append(epoch_)
                train_acc.append(train_acc_)
                train_error.append(train_error_)
                val_acc.append(val_acc_)
                val_error.append(val_error_)
                test_acc.append(test_acc_)
                test_error.append(test_error_)
            
        # create dataframe with results
        data = {'step': step_num, 'epoch': epoch, 
            'train_acc': train_acc, 'train_error': train_error, 
            'val_acc': val_acc, 'val_error': val_error,
            'test_acc': test_acc, 'test_error': test_error
            }
            
        df_results = pd.DataFrame(data=data)
        
        return df_results

    else:
        # Here's where you're restoring the variables w and b.
        # Note that the graph is exactly as it was when the variables were
        # saved in a prior training run.        
        ckpt = tf.train.get_checkpoint_state(checkpoint_dir)
        
        if ckpt and ckpt.model_checkpoint_path:
            saver.restore(sess, ckpt.model_checkpoint_path)

            # Now you can run the model to get predictions
            #batch_X, batch_Y = data_set.val.next_batch(batch_size)

            logger.debug('Test data set images dimension: {0}'.format(data_set.images.shape))
            logger.debug('Test data set labels dimension: {0}'.format(data_set.labels.shape))

            # evaluate metrics
            acc, cross_entropy_loss, _ = sess.run([accuracy, cross_entropy, It], {X: data_set.images, Y_: data_set.labels, keep_prob: 1.0})
            logger.info("Test accuracy: {0:.8f}".format(acc))
            logger.info("Test cross entropy loss: {0:.8f}".format(cross_entropy_loss))

            # make prediction: probability for each class
            prob_predictions = sess.run(Y, feed_dict={X: data_set.images, keep_prob: 1.0})  

            logger.debug('Prediction vector shape: {0}'.format(prob_predictions.shape))
                        
            #print batch_X.shape
            #print batch_Y.shape
            #prob_predictions = sess.run(Y, feed_dict={X: batch_X, keep_prob: 1.0})  

            #for idx, item in enumerate(predictions):
            #    print('predictions:', predictions[idx], batch_Y[idx])           

            #do_eval(sess, eval_correct, batch_X, batch_Y,
            #    data_set.val.images)

            '''
            #batch_X, batch_Y = data_set.train.next_batch(batch_size)

            predictions = sess.run(Y, feed_dict={X: batch_X, keep_prob: 1.0})  
            
            print predictions.shape
            print('predictions:', zip(predictions, batch_Y))           
            '''

            return prob_predictions
            
        else:
            logger.error('No checkpoint found')

        
