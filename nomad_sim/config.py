# encoding: UTF-8
#!/usr/bin/env python
#from __future__ import print_function

__author__ = "Angelo Ziletti"
__copyright__ = "Copyright 2016, The NOMAD Project"
__maintainer__ = "Angelo Ziletti"
__email__ = "ziletti@fhi-berlin.mpg.de"
__date__ = "21/10/16"


import json
import logging
import os, sys

#config_file = './config_local_emre.json'
config_file = './config_local_angelo.json'
#config_file = './config_beaker.json'

current_dir = os.path.dirname(os.path.realpath(__file__))
config_file_path = os.path.abspath(os.path.normpath(os.path.join(current_dir, config_file)))

with open(config_file_path) as config_data_file:
    configs = json.load(config_data_file)

""" For local use the "config.json" file should be as follows:"""
#"output_file": "/tmp/output.log",
#"ureg_file": "./nomadcore/unit_conversion/units.txt",
#"log_level_general": "DEBUG"


""" For Beaker the "config.json" file should be as follows:"""
#"output_file": "/tmp/output.log",
#"ureg_file": "./nomadcore/unit_conversion/units.txt",
#"log_level_general": "INFO",


""" Useful info: """
#logging.DEBUG > logging.INFO > logging.WARNING > logging.ERROR > logging.CRITICAL
    
