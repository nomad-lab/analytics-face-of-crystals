#! /usr/bin/env python
from __future__ import absolute_import

__author__ = "Daria Tomecka and Angelo Ziletti"
__copyright__ = "Copyright 2017, The NOMAD Project"
__maintainer__ = "Daria M. Tomecka and Angelo Ziletti"
__email__ = "tomeckadm@gmail.com; ziletti@fhi-berlin.mpg.de"
__date__ = "06/04/17"

import soap
import sys
import ase.io
import json
import numpy as np
import pickle
import h5py
import time
import datetime
import os
import logging

from nomad_sim.wrappers import get_json_list
from nomad_sim.wrappers import plot, logger
from nomad_sim.utils_crystals import get_spacegroup
from nomad_sim.utils_crystals import create_supercell
from nomad_sim.wrappers import get_json_list
from nomad_sim.nomad_structures import NOMADStructure
from soap.tools import AseConfig


          
def read_data (json_list):          
    frame_list = None
    frame_list_idcs = [ (0,0) ]
    
    data_file_format='NOMAD'      
  
    def get_set_target(ase_struct, nmd_struct, frame_idx, json_idx, json_file):
        chemical_formula, energy, label = get_spacegroup(nmd_struct)
        #chemical_formula, energy, label = get_lattice_type(nmd_struct)h
        return label

    op_list = np.zeros(len(json_list))

    logger.info("Converting data (%d archives)..." % len(json_list))
    ase_atoms_list = []
    nmd_struct_list = []
    frame_list_idx_list = []
    target_list = []
    label_list = []
    z_count_global = {}
    for json_idx, json_file in enumerate(json_list):
        nmd_struct = NOMADStructure(in_file=json_file, frame_list=frame_list, file_format=data_file_format)
        frame_list_idx_list.append([])
        for idx in frame_list_idcs:
            ase_atoms = nmd_struct.atoms[idx]
            ase_atoms_list.append(ase_atoms)
            frame_list_idx_list[-1].append(idx[1])
            target_list.append(get_set_target(ase_atoms, nmd_struct, idx, json_idx, json_file))
            #for z, n_z in ase_config.z_count.items():
            #    if not z in z_count_global or z_count_global[z] < n_z:
            #        z_count_global[z] = n_z
        nmd_struct_list.append(nmd_struct)    

    # TODO Embed idx/hash. This idx wil be used as an hdf5 tag - 
    # probably better to use a unique hash instead.
    for idx, c in enumerate(ase_atoms_list):
        c.info['idx'] = idx 
        c.info['label'] = json_list[idx] 

    return ase_atoms_list, target_list
