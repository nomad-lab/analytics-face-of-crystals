
#!/usr/bin/python

#from __future__ import absolute_import

__author__ = "Angelo Ziletti"
__copyright__ = "Copyright 2016, The NOMAD Project"
__maintainer__ = "Angelo Ziletti"
__email__ = "ziletti@fhi-berlin.mpg.de"
__date__ = "21/10/16"

"""The load_prdf function was originally written by Maximilian Alber
at TU Berlin"""

import json
import os
import scipy.sparse
import tarfile
import time
import logging
import random


import numpy as np

from nomad_sim.config import configs

try:
    hdlr = logging.FileHandler(configs["output_file"], mode='a')
except:
    hdlr = logging.FileHandler(configs["output_file"], mode='w')

level = logging.getLevelName(configs["log_level_general"])

logger = logging.getLogger(__name__)
logger.setLevel(level)
logging.basicConfig(level=level)
FORMAT = "%(levelname)s: %(message)s"
formatter = logging.Formatter(fmt=FORMAT)
handler = logging.StreamHandler()
handler.setFormatter(formatter)
hdlr.setFormatter(formatter)
logger.addHandler(handler)
logger.addHandler(hdlr)
logger.setLevel(level)
logger.propagate = False

__metainfopath__ = configs["meta_info_file"]



#BIN_COUNT = 100
BIN_COUNT = 50
MAX_DISTANCE = 25


def load_xray_1d(directory, f_count):

    BIN_COUNT = 90
    MAX_ANGLE = 90.0
    
    archive = tarfile.open(os.path.join(directory,
                                        "descriptor.tar.gz"), 'r')

    n_configurations = 0
    n_rdfs = 0
    name_list = []
    
    for member in archive.getmembers()[:f_count]:
        if not member.isfile():
            continue
        if member.name.endswith('_target.json'):
            continue
        if member.name.endswith('.info'):
            continue  
        if member.name.endswith('_aims.in'):
            continue  
        
        data = json.load(archive.extractfile(member))

    
        for c in data['data']:
            name_list.append(c['checksum'])
            two_theta = c['two_theta']
            n_configurations += 1
            pass


    logger.debug("Configuration count: {0}".format(n_configurations))

    shape = (n_configurations, BIN_COUNT)
    size = n_configurations * BIN_COUNT * 8
    logger.debug("Array shape: {0}".format(shape))
    logger.debug("Array dimension on disk {0} GB".format(size / (1024**3)))

    X = scipy.sparse.lil_matrix((n_configurations, BIN_COUNT))
    y = np.zeros((n_configurations, ))
    lookup = []

    bins = np.linspace(0, MAX_ANGLE, num=BIN_COUNT+1)-1


    # strings
    chemical_formula = [None]*n_configurations
    target = [None]*n_configurations

    name_list = [name_+'.json' for name_ in name_list]

    lookup = []
    
    i_configuration = 0

#    cdef size_t i
    t = time.time()
    idx = 0
    for member in archive.getmembers()[:f_count]:
        if not member.isfile():
            continue
        if member.name.endswith('_target.json'):
            continue
        if member.name.endswith('.info'):
            continue  
        if member.name.endswith('_aims.in'):
            continue  
        
        data = json.load(archive.extractfile(member))

        for c_i, c in enumerate(data['data']):
            if i_configuration % 100 == 0:
                logger.debug("Processing configuration {0}/{1}".format(i_configuration+1, n_configurations))

            two_theta = c['two_theta']
            binned, foo = np.histogram(two_theta, bins=bins, density=True)
            
            #chemical_formula[i_configuration] = c['chemical_formula']
            
            #target[i_configuration] = c[str(target_name)]
                
            for i in range(BIN_COUNT):
                X[i_configuration, i] = binned[i]

            
            lookup.append((i_configuration, name_list[i_configuration], c_i,
                target[i_configuration],  
                chemical_formula[i_configuration]))
                
            i_configuration += 1
            pass

    X = X.asformat("csr")
    logger.debug("Actual feature matrix needs {0} bytes".format(X.data.nbytes))
    logger.debug("Feature matrix shape: {0}".format(X.shape))
    
    #print lookup
    #print 'matrix X', X
    
    return X, y, lookup
    


def load_prdf(directory, f_count):

    archive = tarfile.open(os.path.join(directory,
                                        "descriptor.tar.gz"), 'r')

    n_configurations = 0
    n_rdfs = 0
    atom_set = set()

    for member in archive.getmembers()[:f_count]:
        if not member.isfile():
            continue
        data = json.load(archive.extractfile(member))

        for c in data['data']:
            rdfs = c['radial_distribution_function']
                        
            atoms = ([rdfs[rdf]['particle_atom_numer_1'] for rdf in rdfs] +
                     [rdfs[rdf]['particle_atom_numer_2'] for rdf in rdfs])
            atom_set.update(atoms)

            for name, rdf in rdfs.items():
                n_rdf = len(rdf['weights'])
                if n_rdfs < n_rdf:
                    n_rdfs = n_rdf

                if len(rdf['arr']) != n_rdf:
                    raise Exception("Wrong number of distances at c=%i, %s."
                                    % (n_configurations, name))

            n_configurations += 1
            pass

    n_atoms = max(atom_set)+1
    logger.debug("Setting up dictionary for cluster calculation.")

    logger.debug("Configuration count: {0}".format(n_configurations))
    logger.debug("Longest rdf list: {0}".format(n_rdfs))
    logger.debug("Atomic set info:") 
    logger.debug("Number of different atoms in the set: {0}".format(len(atom_set)))
    logger.debug("Highest atomic number in the set: {0}".format(n_atoms))
    logger.debug("Actual atom set: {0}".format(atom_set))


    shape_ = (n_configurations, n_atoms, n_atoms, BIN_COUNT)
    shape = (n_configurations, n_atoms * n_atoms * BIN_COUNT)
    size = n_configurations * n_atoms * n_atoms * BIN_COUNT * 8
    logger.debug("Sparse array shape: {0}, {1}".format(shape_, shape))
    #logger.debug("In dense it would be {0} GB".format(size / (1024**3)))

    X = scipy.sparse.lil_matrix((n_configurations,
                                 n_atoms * n_atoms * BIN_COUNT))
    y = np.zeros((n_configurations, ))
    energy = np.zeros((n_configurations, ))
    
    # strings
    #chemical_formula = [None]*n_configurations
    checksum = [None]*n_configurations
    
    lookup = []

    bins = np.logspace(0, np.log10(MAX_DISTANCE), num=BIN_COUNT+1)-1

    cur_class = 0
    i_configuration = 0

#    cdef size_t i
#    cdef np.ndarray[np.float64_t, ndim=1] arr
#    cdef np.ndarray[np.float64_t, ndim=1] weights
    t = time.time()
    idx = 0
    for member in archive.getmembers()[:f_count]:
        if not member.isfile():
            continue

        data = json.load(archive.extractfile(member))

        for c_i, c in enumerate(data['data']):
            if i_configuration % 100 == 0:
                logger.debug("Processing configuration {0}/{1}".format(i_configuration+1, n_configurations))


            rdfs = c['radial_distribution_function']
            energy = c['energy_total']
            checksum = c['checksum']
            json_file = c['main_json_file_name']


            def xsorted(rdfs):
                l = [x.split("_") for x in list(rdfs)]
                n = sorted([(int(a), int(b)) for a, b in l])
                return ["%i_%i" % (a, b) for a,b in n]

            # want indexs to incrase mono., other lil_matrix has bad performance
            for name in xsorted(rdfs):
                rdf = rdfs[name]
                i_atom = rdf['particle_atom_numer_1']
                j_atom = rdf['particle_atom_numer_2']

                if i_atom < j_atom:
                    i_atom, j_atom = j_atom, i_atom

                arr = np.array(rdf['arr'], dtype=np.float64).reshape((-1,))
                weights = np.array(rdf['weights'], dtype=np.float64).reshape((-1,))

                binned, foo = np.histogram(arr, bins=bins, weights=weights, density=True)

                idx = (i_atom * n_atoms * BIN_COUNT +
                       j_atom * BIN_COUNT)

                for i in range(BIN_COUNT):
                    X[i_configuration, idx] = binned[i]
                    idx += 1
                    pass

            y[i_configuration] = cur_class
            lookup.append((i_configuration, json_file, c_i, energy, checksum))        
            i_configuration += 1
            pass

        cur_class += 1
        pass

    X = X.asformat("csr")
    logger.debug("Actual feature matrix needs {0} bytes".format(X.data.nbytes))
    logger.debug("Feature matrix shape: {0}".format(X.shape))
    
    return X, y, lookup



def load_atomic_features(directory, f_count):
    archive = tarfile.open(os.path.join(directory,
                                        "descriptor.tar.gz"), 'r')
                                        
    n_configurations = 0
    atom_set = set()

    for member in archive.getmembers()[:f_count]:
        if not member.isfile():
            continue
        data = json.load(archive.extractfile(member))

        for c in data['data']:
            atom_feats_dict = c['atomic_features']
            name = c['checksum']
            atoms = c['particle_atom_number']
            atom_set.update(atoms)
            n_configurations += 1            
            pass
    
    n_atom_feats = len(atom_feats_dict)

    logger.debug("Configuration count: {0}".format(n_configurations))
    logger.debug("Primary feature count: {0}".format(n_atom_feats))
    logger.debug("Atomic set info:") 
    logger.debug("Number of different atoms in the set: {0}".format(len(atom_set)))
    logger.debug("Actual atom set: {0}".format(atom_set))


    #shape_ = (n_configurations, n_atom_feats, n_atoms, BIN_COUNT)
    #shape = (n_configurations, n_atoms * n_atoms * BIN_COUNT)
    size = n_configurations * n_atom_feats * 8
    #logger.debug("Sparse array shape: {0}, {1}".format(shape_, shape))
    logger.debug("Feature matrix dimension: {0} GB".format(size / (1024**3)))

    X = np.zeros((n_configurations, n_atom_feats))
    X_columns = [None]*n_configurations
    #label = np.zeros((n_configurations, ))
    target = np.zeros((n_configurations, ))
    chemical_formula = [None]*n_configurations
    energy = np.zeros((n_configurations, ))
    #is_lowest_energy = [None]*n_configurations
    json_file_list = [None]*n_configurations

    lookup = []


    cur_class = 0
    i_configuration = 0

#    cdef size_t i
#    cdef np.ndarray[np.float64_t, ndim=1] atom_feats_values
    t = time.time()
    idx = 0
    
    # loop over the files
    for member in archive.getmembers()[:f_count]:
        if not member.isfile():
            continue

        data = json.load(archive.extractfile(member))

        l=[]
        X_labels=[]

        # loop over multiple configuration within the file
        for c_i, c in enumerate(data['data']):
            if i_configuration % 100 == 0:
                logger.debug("Processing configuration {0}/{1}".format(i_configuration+1, n_configurations))

            energy = c['energy_total']
            atom_feats_dict = c['atomic_features']            
            atom_feats_keys =  c['atomic_features'].keys()

            atom_feats_values = np.array(c['atomic_features'].values(), dtype=np.float64).reshape((-1,))

            X[i_configuration] = atom_feats_values
            X_columns[i_configuration] = atom_feats_keys
#            label[i_configuration] = c['label']
            target[i_configuration] = c['delta_e_target']             
            chemical_formula[i_configuration] = c['chemical_formula']
            #is_lowest_energy[i_configuration] = c['is_lowest_energy']
            json_file_list[i_configuration] = c['main_json_file_name']
                      
            lookup.append((
                i_configuration, 
                json_file_list[i_configuration], 
                c_i,
                target[i_configuration],
                chemical_formula[i_configuration],
                energy))
                #is_lowest_energy[i_configuration]))
                 #label[i_configuration],  
            
            X_labels.append(X_columns[i_configuration])

            i_configuration += 1



    logger.debug("Feature matrix shape: {0}".format(X.shape))
    
    return X, target, lookup, X_labels


def load_xray(target_name=None, desc_folder=None, desc_file=None, filename=None, f_count=200000):
    
    if filename is None:
        filename = os.path.abspath(os.path.normpath(os.path.join(desc_folder, desc_file)))

    archive = tarfile.open(filename, 'r')

    n_configurations = 0
    n_members = 0
    atom_set = set()
    name_list = []
    op_list = []
    json_file_list = []

    for member in archive.getmembers()[:]:
        if not member.isfile():
            continue        
        if member.name.endswith('_xray.png'):
            continue
        if member.name.endswith('_xray_rs.png'):
            continue
        if member.name.endswith('_xray.png'):
            continue
        if member.name.endswith('.npy'):
            continue
        if member.name.endswith('.info'):
            continue  
        if member.name.endswith('_aims.in'):
            continue
        if member.name.endswith('summary.json'):
            continue

        data = json.load(archive.extractfile(member))


        for c in data['data']:
            name_list.append(c['checksum'])
            json_file_list.append(c['main_json_file_name'])
            atoms = c['particle_atom_number']
            atom_set.update(atoms)
            n_configurations += 1  
            try:
                op_list.append(c['operation_number'])
            except:
                pass
            pass
    
    
    logger.debug("Configuration count: {0}".format(n_configurations))
    logger.debug("Atomic set info:") 
    logger.debug("Number of different atoms in the set: {0}".format(len(atom_set)))
    logger.debug("Actual atom set: {0}".format(atom_set))

    # strings
    chemical_formula = [None]*n_configurations
    target = [None]*n_configurations

    name_list = [name_+'.json' for name_ in name_list]


    lookup = []

    cur_class = 0
    i_configuration = 0

#    cdef size_t i
    #cdef np.ndarray[np.float64_t, ndim=1] atom_feats_values
    t = time.time()
    idx = 0
    
    # loop over the files
    for member in archive.getmembers()[:]:
        if not member.isfile():
            continue        
        if member.name.endswith('_xray.png'):
            continue
        if member.name.endswith('_xray_rs.png'):
            continue
        if member.name.endswith('.npy'):
            continue 
        if member.name.endswith('.info'):
            continue  
        if member.name.endswith('_aims.in'):
            continue
        if member.name.endswith('summary.json'):
            continue
        
        data = json.load(archive.extractfile(member))

        l=[]
        X_labels=[]
        
        
        # loop over multiple configuration within the file
        for c_i, c in enumerate(data['data']):
            if i_configuration % 100 == 0:
                logger.debug("Processing configuration {0}/{1}".format(i_configuration+1, n_configurations))

            #cell[i_configuration] = c['cell']            
            #positions[i_configuration] = c['particle_position']            
            chemical_formula[i_configuration] = c['chemical_formula']
            
            target[i_configuration] = c[str(target_name)]
                
            
            '''
            X[i_configuration] = atom_feats_values
            X_columns[i_configuration] = atom_feats_keys
            label[i_configuration] = c['label']
            target[i_configuration] = c['delta_e_rs_zb']             
            chemical_formula[i_configuration] = c['chemical_formula']
            is_lowest_energy[i_configuration] = c['is_lowest_energy']
             
            '''
            lookup.append((i_configuration, json_file_list[i_configuration], c_i,
                #op_list[i_configuration],
                target[i_configuration],  
                chemical_formula[i_configuration]))
            
            #X_labels.append(X_columns[i_configuration])
            
            i_configuration += 1



    #logger.debug("Feature matrix shape: {0}".format(X.shape))
    logger.debug("Target vector shape: {0}".format(len(target)))
    #logger.debug("Feature matrix shape: {0}".format(X.shape))


    X_labels = None
    
    return target, lookup, X_labels




# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

