# -*- coding: utf-8 -*-
"""
Created on Fri Aug 19 13:54:53 2016

@author: ziletti
"""
from __future__ import absolute_import

import os, logging, sys
import numpy as np
import pandas as pd



from nomad_sim.config import configs

try:
    hdlr = logging.FileHandler(configs["output_file"], mode='a')
except:
    hdlr = logging.FileHandler(configs["output_file"], mode='w')

level = logging.getLevelName(configs["log_level_general"])

logger = logging.getLogger(__name__)
logger.setLevel(level)
logging.basicConfig(level=level)
FORMAT = "%(levelname)s: %(message)s"
formatter = logging.Formatter(fmt=FORMAT)
handler = logging.StreamHandler()
handler.setFormatter(formatter)
hdlr.setFormatter(formatter)
logger.addHandler(handler)
logger.addHandler(hdlr)
logger.setLevel(level)
logger.propagate = False

__metainfopath__ = configs["meta_info_file"]


def read_gdb_7k(dataset=None, xyz_file=None, target_file=None):
    """ Read the gdb_7k dataset.
    
    Link to reference: http://iopscience.iop.org/article/10.1088/1367-2630/15/9/095003/meta;jsessionid=BE06A5911A9B58D271F6E2C145D54FDC.c2.iopscience.cld.iop.org
    Link to dataset: http://www.mrupp.info/Data/2013dsgdb7njp.zip
    
    Parameters 
    ----------
    dataset : string
        File containing the dataset. If taken from the link above, it is named "dsgdb7njp.xyz".

    xyz_file : string, optional, default ./dsgdb7njp_ase.xyz
        File containing the configurations in the dataset that can be read by the ASE python library.

    target_file : string, optional, default ./dsgdb7njp.csv
        CSV file containing the target values as panda dataframe. See the code below for details.

    Returns
    -------
    
    dataset : dataframe
         Dataframe containing the data read from the datasets. It is 7211 rows.
    
    """
        
    if xyz_file is None:
        xyz_file = './dsgdb7njp_ase.xyz'
        
    if target_file is None:
        target_file = './dsgdb7njp.csv'

    logger.info('Preprocessing gdb13_7k database')
    
    i=0
    f = open(dataset, 'r')
    out = open(xyz_file, 'w')
    l = [l for l in f.readlines()]
    # to read the 1st line correctly
    id_empty= [-1]
    
    for idx, l_ in enumerate(l):
        if not l_.strip():
            i += 1
            id_empty.append(idx)        
        
    target_values = [l[idx+2] for idx in id_empty]  
        
    data = []
    
    for target_ in target_values:
        data.append(str(target_).split())
    
    labels = []
    # ae_pbe0    kcal/mol   Atomization energy (DFT/PBE0)
    labels.append('ae_pbe0')
    # p_pbe0     Angstrom^3 Polarizability (DFT/PBE0)
    labels.append('p_pbe0')
    # p_scs      Angstrom^3 Polarizability (self-consistent screening)
    labels.append('p_scs')
    # homo_gw    eV         Highest occupied molecular orbital (GW)
    labels.append('homo_gw')
    # homo_pbe0  eV         Highest occupied molecular orbital (DFT/PBE0)
    labels.append('homo_pbe0') 
    # homo_zindo eV         Highest occupied molecular orbital (ZINDO/s)
    labels.append('homo_zindo')
    # lumo_gw    eV         Lowest unoccupied molecular orbital (GW)
    labels.append('lumo_gw')
    # lumo_pbe0  eV         Lowest unoccupied molecular orbital (DFT/PBE0)
    labels.append('lumo_pbe0')
    # lumo_zindo eV         Lowest unoccupied molecular orbital (ZINDO/s)
    labels.append('lumo_zindo')
    # ip_zindo   eV         Ionization potential (ZINDO/s)
    labels.append('ip_zindo')
    # ea_zindo   eV         Electron affinity (ZINDO/s)
    labels.append('ea_zindo')
    # e1_zindo   eV         First excitation energy (ZINDO)
    labels.append('e1_zindo')
    # emax_zindo eV         Maximal absorption intensity (ZINDO)
    labels.append('emax_zindo')
    # imax_zindo arbitrary  Excitation energy at maximal absorption (ZINDO)
    labels.append('imax_zindo')
    
    data = np.asarray(data).astype(float)
    df= pd.DataFrame(data=data, columns=labels)
    
    # add homo-lumo gaps
    df['homo_lumo_gap_gw'] = df['lumo_gw']-df['homo_gw']
    df['homo_lumo_gap_pbe0'] = df['lumo_pbe0']-df['homo_pbe0']
    
    # copy in the xyz file the original dataset without the empty lines
    lines = [line for line in open(dataset) if line[:-1]]
    f.close()
    out.writelines(lines)
    out.close()
    
    if df.shape[0]!=7211:
        logger.warning('The target value array should contain 7211 elements.')
        logger.warning('Your target value contains {0} elements.'.format(df.shape[0]))

    
    df.to_csv(target_file, index=False)
    logger.debug('Printing some statistics on the dataset')
    logger.debug(df.describe())
    
    logger.info('Preprocessing: done.')


    return dataset

#read_write_json_files_nomad(data_folder=data_folder_cif_afm)
                
#json_list = get_json_list(method='folder', drop_duplicates=False, 
#    data_folder=data_folder_cif_afm, tmp_folder=tmp_folder)

#ase_atoms_list = read_data(json_list)
#print len(ase_atoms_list)

def read_data(json_list):          
    """Read json file from the NOMAD Archive format and get a list of ASE structures""" 
    frame_list = None
    frame_list_idcs = [ (0,0) ]
    
    data_file_format='NOMAD'      
  
    op_list = np.zeros(len(json_list))

    logger.info("Converting data (%d archives)..." % len(json_list))
    ase_atoms_list = []
    nmd_struct_list = []
    frame_list_idx_list = []
    target_list = []
    label_list = []
    z_count_global = {}
    for json_idx, json_file in enumerate(json_list):
        nmd_struct = NOMADStructure(in_file=json_file, frame_list=frame_list, file_format=data_file_format)
        frame_list_idx_list.append([])
        for idx in frame_list_idcs:
            ase_atoms = nmd_struct.atoms[idx]
            ase_atoms_list.append(ase_atoms)
            frame_list_idx_list[-1].append(idx[1])
        nmd_struct_list.append(nmd_struct)    

    # TODO Embed idx/hash. This idx wil be used as an hdf5 tag - 
    # probably better to use a unique hash instead.
    for idx, c in enumerate(ase_atoms_list):
        c.info['idx'] = idx 
        c.info['label'] = json_list[idx] 

    return ase_atoms_list

def read_write_json_files_nomad(data_folder, NB_MAX_FOLDERS=1000, NB_MAX_FILES=30000, filename_suffix='_nomad.json'):
    """Read geometry files from the competition, and write json_files in the NOMAD format"""
    ase_atoms_list = [] 
    label_list = []   # the labels are the filenames

    i = 0
    j = 0
    for root, dirs, files in os.walk(data_folder):
        i += 1 
        if i > NB_MAX_FOLDERS: #this is for folders 
            break
        for file_ in files:
            j+= 1
            if j > NB_MAX_FILES:  #this is for files
                i=NB_MAX_FILES+1 
                break
            if file_.endswith(".cif"):
                filepath = os.path.join(root, file_)
                #print filepath
                #ase_atoms = ase.io.read(filepath, index='0', format='aims-output') 
                try:
                    ase_atoms = ase.io.read(filepath, index=None, format='cif') 
                    ase_atoms_list.append(ase_atoms)
                    label_list.append(str(root).split("/")[-1])
                    print "writing file "+filepath
                
                except:
                    #print "passing on this file"
                    pass
            if (j % 100) == 0:
                print "iteration: ", j
                        

    label_list = map(int, label_list)

    # order lists according to label to be compatible with the csv_file
    #label_list, ase_atoms_list = (list(t) for t in zip(*sorted(zip(label_list, ase_atoms_list))))

    for idx in range(len(label_list)):
        print idx, "/", len(label_list)
        filename = os.path.abspath(os.path.normpath(os.path.join(path, 
            '{0}{1}'.format(str(label_list[idx]), filename_suffix))))
        atoms = ase_atoms_list[idx]
        
        outF=file(filename,'w')
        outF.write("""
{
 "sections": {
     "section_run-0": {
         "name" : "section_run",
         "gIndex" : 0,
         "sections": {
               "section_single_configuration_calculation-0": {
                  "name": "section_single_configuration_calculation",
                  "gIndex": 0,
                  "energy_total": 0.0},
             "section_system-0": 
        """
             )           
        cell = atoms.get_cell()*1.0E-10
        res={
                        "name": "section_system",
                        "gIndex" : 0,
                        "label": label_list[idx],
                        "chemical_formula": atoms.get_chemical_formula(mode='hill'),
                        "simulation_cell": cell.tolist(),
                        "atom_labels": atoms.get_chemical_symbols(),
                        # nomad json files are in SU 
                        "atom_positions": map(lambda x: [x.x*1.0E-10,x.y*1.0E-10,x.z*1.0E-10], atoms),
                    }
                        
        json.dump(res, outF, indent=2)
        outF.write("""
            }}}}

            """)
        outF.flush()
