from __future__ import absolute_import

__author__ = "Angelo Ziletti"
__copyright__ = "Copyright 2016, The NOMAD Project"
__maintainer__ = "Angelo Ziletti"
__email__ = "ziletti@fhi-berlin.mpg.de"
__date__ = "21/10/16"

"""The initial stub was written by Maximilian Alber at TU Berlin"""

import os, sys
import numpy as np
import logging
import pyximport
pyximport.install(reload_support=True)

from nomad_sim import cconvert
from nomad_sim.cnn_preprocessing import extract_images
from nomad_sim.model_cnn import load_model
from nomad_sim.model_cnn import get_features 
from nomad_sim.config import configs
from nomad_sim.cnn_preprocessing import read_data_sets

try:
    hdlr = logging.FileHandler(configs["output_file"], mode='a')
except:
    hdlr = logging.FileHandler(configs["output_file"], mode='w')

level = logging.getLevelName(configs["log_level_general"])

logger = logging.getLogger(__name__)
logger.setLevel(level)
logging.basicConfig(level=level)
FORMAT = "%(levelname)s: %(message)s"
formatter = logging.Formatter(fmt=FORMAT)
handler = logging.StreamHandler()
handler.setFormatter(formatter)
hdlr.setFormatter(formatter)
logger.addHandler(handler)
logger.addHandler(hdlr)
logger.setLevel(level)
logger.propagate = False

__metainfopath__ = configs["meta_info_file"]


def build_sim_matrix(
    target_name=None, 
    target_categorical=None,
    desc_folder=None, 
    matrix_file=None, 
    desc_type=None, 
    input_dims=None, 
    desc_file=None, 
    f_count_max=10000,
    use_xray_img=True, 
    nb_nn_layer=None, 
    model_arch_file=None, 
    model_weights_file=None,
    path_to_x_test=None,
    n_bins=None, 
    disc_type=None,
    batch_size=None):

    if desc_type=='prdf':
        X_train, y_train, lookup = cconvert.load_prdf(desc_folder, int(f_count_max))

        # saving the sparse matrix to file
        np.savez_compressed(
            matrix_file,
            **{
                "X_train_data": X_train.data,
                "X_train_indices": X_train.indices,
                "X_train_indptr": X_train.indptr,
                "X_train_shape": X_train.shape,
    
                "y_train": y_train,
    
                "lookup": lookup,
            }
        )

    elif desc_type=='rdf':
        X_train, y_train, lookup = cconvert.load_prdf(desc_folder, int(f_count_max))

        # saving the sparse matrix to file
        np.savez_compressed(
            matrix_file,
            **{
                "X_train_data": X_train.data,
                "X_train_indices": X_train.indices,
                "X_train_indptr": X_train.indptr,
                "X_train_shape": X_train.shape,
    
                "y_train": y_train,
    
                "lookup": lookup,
            }
        )
    
    elif desc_type=='xray_1d':
        X_train, y_train, lookup = cconvert.load_xray_1d(desc_folder, int(f_count_max))

        # saving the sparse matrix to file
        np.savez_compressed(
            matrix_file,
            **{
                "X_train_data": X_train.data,
                "X_train_indices": X_train.indices,
                "X_train_indptr": X_train.indptr,
                "X_train_shape": X_train.shape,

                "y_train": y_train,
                "lookup": lookup,
            }
        )
    

    elif desc_type=='atomic_features':
        X_train, y_train, lookup, X_labels = cconvert.load_atomic_features(desc_folder, 
            int(f_count_max))
        
        # saving the dense matrix to file
        np.savez_compressed(
            matrix_file,
            **{
                "X_train": X_train,
                "y_train": y_train,
    
                "lookup": lookup,
                #column names in X_train
                "X_labels": X_labels,
            }
        )

    elif desc_type=='xray':
        """Works for list of desc_files as expected"""
        
        if isinstance(desc_file, list):
            archive_list = [os.path.abspath(os.path.normpath(os.path.join(desc_folder, desc_file_item))) for desc_file_item in desc_file]

        # alternative, not implemented yet
            # read pickle previously written by run_cnn
#            logger.warning("Reading the data from the pickle wrote previously.")
#            data = load_data_from_pickle(input_dims, path_to_x_test)    

        # read data
        data_set, n_classes, num_labels, class_labels = read_data_sets(
            target_categorical=target_categorical,
            desc_folder=desc_folder, desc_file_list=archive_list,
            one_hot=True, flatten_images=False, 
            n_bins=n_bins, 
            target_name=target_name,
            disc_type=disc_type, 
            input_dims=input_dims,
            split_train_val=False)
                                         
        # load the data    
        X = data_set.images
        y = data_set.labels
    
        # reshape according to the Keras rule
        if len(input_dims) == 2:    
            X = X.reshape(X.shape[0], 1, input_dims[0], input_dims[1])
        elif len(input_dims) == 3:
            X = X.reshape(X.shape[0], 1, input_dims[0], input_dims[1], input_dims[2])
        else:
            raise Exception("Wrong number of dimensions.")
     
        logger.debug("X_test shape: {0}".format(X.shape))           
        logger.debug("y_test shape: {0}".format(y.shape))      
            
                
        if use_xray_img:
            X_out = np.reshape(X, (X.shape[0], -1))
            logger.info("Shape of image matrix: {0}".format(X.shape))
            size = np.prod(X.shape)*8
            logger.info("Image matrix dimension {0} MB".format(size / (1024**2)))
        
        else:
            logger.info("Using the convolutional neural network filters for the embedding.")
            logger.info("Layer number: {0}".format(nb_nn_layer))
            logger.info("Reading model architecture file: {0}".format(model_arch_file))
            logger.info("Reading model weights file: {0}".format(model_weights_file))
            model = load_model(model_arch_file, model_weights_file)
            logger.debug(model.summary())
            if batch_size is None:
                logger.debug("Loading the whole dataset at once.")
                layer_output = get_features(data_set, model, nb_nn_layer, 
                    input_dims=input_dims, batch_size=None)
            else:
                logger.debug("Loading the dataset in batches. Batch_size={0}".format(batch_size))
                layer_output = get_features(data_set, model, nb_nn_layer, 
                    input_dims=input_dims, batch_size=batch_size)
            logger.debug("Layer_output shape: {0}".format(layer_output.shape))
            
            X_out = layer_output


        y = y.tolist()
        # load lookup info
        lookup = []
            
        
        if isinstance(desc_file, list):
            for desc_file_item in desc_file:
                _, lookup_, X_labels_ = cconvert.load_xray(target_name=target_name, 
                    desc_folder=desc_folder, desc_file=desc_file_item)
                    
                lookup.append(lookup_)

        # collapse from list of lists to list
        try:    
            lookup = [item for sublist in lookup for item in sublist]
            y = [item for sublist in y for item in sublist]
        except:
            pass
        
        # saving the dense matrix to file
        np.savez_compressed(
            matrix_file,
            **{
                "X_train": X_out,
                "y_train": y,
                "lookup": lookup,
            }
            
        )




# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

