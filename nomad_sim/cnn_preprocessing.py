# encoding: UTF-8
#!/usr/bin/env python
from __future__ import absolute_import
from __future__ import division
from sklearn.cross_validation import StratifiedShuffleSplit
from sklearn.cross_validation import ShuffleSplit
#from __future__ import print_function

__author__ = "Angelo Ziletti"
__copyright__ = "Copyright 2016, The NOMAD Project"
__maintainer__ = "Angelo Ziletti"
__email__ = "ziletti@fhi-berlin.mpg.de"
__date__ = "21/10/16"

import cPickle as pickle
import sys,logging
import gzip
import os
import copy
import tarfile
from scipy import ndimage
from scipy import misc
import logging
from nomad_sim.utils_data_retrieval import extract_images, extract_target_files, extract_labels

import pyximport
pyximport.install(reload_support=True)

from nomad_sim.cconvert import load_xray
import numpy as np
import pandas as pd

import tensorflow as tf
tf.set_random_seed(0)


from nomad_sim.config import configs

try:
    hdlr = logging.FileHandler(configs["output_file"], mode='a')
except:
    hdlr = logging.FileHandler(configs["output_file"], mode='w')

level = logging.getLevelName(configs["log_level_general"])

logger = logging.getLogger(__name__)
logger.setLevel(level)
logging.basicConfig(level=level)
FORMAT = "%(levelname)s: %(message)s"
formatter = logging.Formatter(fmt=FORMAT)
handler = logging.StreamHandler()
handler.setFormatter(formatter)
hdlr.setFormatter(formatter)
logger.addHandler(handler)
logger.addHandler(hdlr)
logger.setLevel(level)
logger.propagate = False

__metainfopath__ = configs["meta_info_file"]
#------------------------------------------------------------------------
    
def find_nearest(array,value):
    idx = (np.abs(array-value)).argmin()
    
def dense_to_one_hot(labels_dense, label_encoder=None):
    """Convert class labels from scalars to one-hot vectors."""            
    n_classes = len(label_encoder.classes_)
    logger.debug('Unique classes: {0}'.format(n_classes))
    n_labels = labels_dense.shape[0]
    index_offset = np.arange(n_labels) * n_classes
    labels_one_hot = np.zeros((n_labels, n_classes))
    labels_one_hot.flat[index_offset + labels_dense.ravel()] = 1
    return labels_one_hot
    
    
# Adapted from the TensorFlow tutorial at
# https://www.tensorflow.org/versions/master/tutorials/index.html
class DataSet(object):

    def __init__(self, input_dims, images, labels,
                 dtype=tf.float32, flatten_images=True):
        """Construct a DataSet.

        `dtype` can be either
        `uint8` to leave the input as `[0, 255]`, or `float32` to rescale into
        `[0, 1]`.
        """
        self._input_dims = input_dims

        dtype = tf.as_dtype(dtype).base_dtype
        if dtype not in (tf.uint8, tf.float32):
            raise TypeError('Invalid image dtype %r, expected uint8 or float32' %
                            dtype)
        assert images.shape[0] == labels.shape[0], (
            'images.shape: %s labels.shape: %s' % (images.shape,
                                                   labels.shape))
        self._num_examples = images.shape[0]

        if len(self._input_dims) == 2 :
            # Convert shape from [num examples, rows, columns, depth]
            # to [num examples, rows*columns] (assuming depth == 1)
            assert images.shape[3] == 1
            if (flatten_images):
                images = images.reshape(images.shape[0],
                                        images.shape[1] * images.shape[2])
        elif len(self._input_dims) == 3 :
            # Convert shape from [num examples, dim1, dim2, dim3, depth]
            # to [num examples, dim1*dim2*dim3] (assuming depth == 1)
            assert images.shape[4] == 1
            if (flatten_images):
                images = images.reshape(images.shape[0],
                                        images.shape[1] * images.shape[2]* images.shape[3])
        else:
            raise Exception("Wrong number of dimensions.")
        
        
        if dtype == tf.float32:
            # Convert from [0, 255] -> [0.0, 1.0].
            images = images.astype(np.float32)
            images = np.multiply(images, 1.0 / 255.0)
        self._images = images
        self._labels = labels
        self._epochs_completed = 0
        self._index_in_epoch = 0
        
    def __getstate__(self):
        return self.train.images, self.train.labels, self.val.images, self.val.labels

    @property
    def images(self):
        return self._images

    @property
    def labels(self):
        return self._labels

    @property
    def num_examples(self):
        return self._num_examples

    @property
    def epochs_completed(self):
        return self._epochs_completed

        
#    def next_batch(self, batch_size):
#        """Return the next `batch_size` examples from this data set."""
#        start = self._index_in_epoch
#        print start
#        self._index_in_epoch += batch_size
#        if self._index_in_epoch > self._num_examples:
#            # Finished epoch
#            self._epochs_completed += 1
#            # Shuffle the data
#            perm = np.arange(self._num_examples)
#            np.random.shuffle(perm)
#            self._images = self._images[perm]
#            self._labels = self._labels[perm]
#            # Start next epoch
#            start = 0
#            self._index_in_epoch = batch_size
#            assert batch_size <= self._num_examples
#        end = self._index_in_epoch
#        return self._images[start:end], self._labels[start:end]
    
def make_data_sets(input_dims, train_images, train_labels, test_images, test_labels, 
        val_images=None, val_labels=None, flatten_images=False):

    class DataSets(object):
        pass
    
    data_sets = DataSets()

    data_sets.train = DataSet(input_dims, train_images, train_labels, flatten_images=flatten_images)
    data_sets.test = DataSet(input_dims, val_images, val_labels, flatten_images=flatten_images)

    if (val_images is not None) and (val_labels is not None):
        data_sets.val = DataSet(val_images, val_labels, flatten_images=flatten_images)
    else:
        data_sets.val = None

    return data_sets
    
def read_data_sets(desc_folder=None, desc_file_list=None, one_hot=True, 
        dtype=tf.float32, flatten_images=False, n_bins=None, target_name=None, 
        target_categorical=None, disc_type=None, input_dims=None, split_train_val=True,
        tmp_folder=None):
            
    class DataSets(object):
        pass

    data_sets = DataSets()

    
    if isinstance(desc_file_list, list)==False:
        desc_file_list = [desc_file_list]
    
    logger.debug('Descriptor file_list: {0}'.format(desc_file_list))
    desc_file_list = [os.path.abspath(os.path.normpath(os.path.join(desc_folder, desc_file))) for desc_file in desc_file_list]
    

    images_list = []        
    for desc_file in desc_file_list:
        # if the final array shape is not know, it is faster to append to a list and 
        # covert to array than append to numpy array 
        images_ = extract_images(filename=desc_file, 
            input_dims=input_dims, desc_folder=desc_folder, tmp_folder=tmp_folder)
            
        images_list.append(images_)     
    
    images = np.concatenate(images_list)
            
    #print 'shape images', images.shape
    # reshape because the 1st dimension in the images array now is the 
    # number of the element of desc_file_list while we want the total number of images        
    #images = np.reshape(images, (-1, images.shape[1], images.shape[2], images.shape[3]))
    
    # here the loop is "inside" the function because we want to calculate the
    # label_encoder when we have seen all the data        
    label_encoder, labels, class_labels = extract_labels(desc_file_list, n_bins=n_bins, target_name=target_name, 
        target_categorical=target_categorical, disc_type=disc_type,
        desc_folder=desc_folder, tmp_folder=tmp_folder)

        
    # save it because labels will change if you have one-hot encoding but we 
    # want to have the label number in any case
    num_labels = labels
    n_classes = len(label_encoder.classes_)

    logger.debug("Shape of image matrix: {0}".format(images.shape))
    size = np.prod(images.shape)*8.0

    logger.debug("Shape of labels matrix: {0}".format(labels.shape))
    size = np.prod(labels.shape)*8.0
    
    
    class_list, class_pop = np.unique(labels, return_counts=True)
    
    logger.debug("Class populations: \n {0}".format(class_pop))

    if one_hot:
        labels = dense_to_one_hot(labels, label_encoder=label_encoder)        
        logger.debug("Using one-hot encoding. The sample number is {0}*(image matrix samples)".format(n_classes))  
    
    
    if split_train_val:
        logger.debug("Splitting in train/validation set")  

        try:    
            sss = StratifiedShuffleSplit(labels, 1, test_size=0.25, random_state=0)
            logger.debug("Using stratified sampling.")
        except:
            logger.debug("Not using stratified sampling.")
            sss = ShuffleSplit(labels.shape[0], 1, test_size=0.25, random_state=0)
                            
        for train_index, val_index in sss:
            train_images, val_images=images[train_index], images[val_index]
            train_labels, val_labels=labels[train_index], labels[val_index]
    
        logger.debug("Shape of the train images: {0}".format(train_images.shape))  
        size = np.prod(train_images.shape)*8.0
        logger.debug("Matrix dimension: {0:.1f} MB".format(size / (1024**2)))
        
        logger.debug("Shape of the validation images: {0}".format(val_images.shape))    
        size = np.prod(val_images.shape)*8.0
        logger.debug("Matrix dimension: {0:.1f} MB".format(size / (1024**2)))
    
        logger.debug("Shape of the train labels: {0}".format(train_labels.shape))  
        size = np.prod(train_labels.shape)*8.0
        logger.debug("Matrix dimension: {0:.1f} MB".format(size / (1024**2)))
        
        logger.debug("Shape of the validation labels: {0}".format(val_labels.shape))    
        size = np.prod(val_labels.shape)*8.0
        logger.debug("Matrix dimension: {0:.1f} MB".format(size / (1024**2)))
    
    
        data_sets.train = DataSet(input_dims, train_images, train_labels, dtype=dtype, flatten_images=flatten_images)
        data_sets.val = DataSet(input_dims, val_images, val_labels, dtype=dtype, flatten_images=flatten_images)

    else:
        logger.debug("Not splitting in train/validation set")  

        logger.debug("Shape of the images: {0}".format(images.shape))  
        size = np.prod(images.shape)*8.0
        logger.debug("Matrix dimension: {0:.1f} MB".format(size / (1024**2)))
        
        logger.debug("Shape of the labels: {0}".format(labels.shape))  
        size = np.prod(labels.shape)*8.0
        logger.debug("Matrix dimension: {0:.1f} MB".format(size / (1024**2)))
                   
        data_sets = DataSet(input_dims, images, labels, dtype=dtype, flatten_images=flatten_images)

    return data_sets, n_classes, num_labels, class_labels
    

def load_data_from_pickle(path_to_x_train, path_to_x_test,
    path_to_y_train, path_to_y_test, path_to_x_val=None, path_to_y_val=None):    
    
    #function to load the pre-processed  data (already resized to 28x28)
    #data_set, n_classes, num_labels, class_labels = read_data_sets(target_categorical=True,
    #                                     desc_folder=desc_folder, desc_file_list=desc_file_list_train,
    #                                     one_hot=True, flatten_images=False, target_name='spacegroup_symbol',
    #                                     split_train_val=True)
                
    x_val = None
    y_val = None                
                 
    with open(path_to_x_train, 'rb') as input:
        x_train = pickle.load(input)

    with open(path_to_y_train, 'rb') as input:
        y_train = pickle.load(input)
        
    if path_to_x_val is not None:
        with open(path_to_x_val, 'rb') as input:
            x_val = pickle.load(input)
            
    if path_to_y_val is not None:
        with open(path_to_y_val, 'rb') as input:
            y_val = pickle.load(input)
        
    with open(path_to_x_test, 'rb') as input:
        x_test = pickle.load(input)

    with open(path_to_y_test, 'rb') as input:
        y_test = pickle.load(input)
        
    
    logger.debug('shape X_train: {0}'.format(x_train.shape))
    logger.debug('shape Y_train: {0}'.format(y_train.shape))
    
    if path_to_x_val is not None:
        logger.debug('shape X_validation: {0}'.format(x_val.shape))
    else:
        logger.debug('Not loading X_validation set.')
        
    if path_to_y_val is not None:
        logger.debug('shape Y_validation: {0}'.format(y_val.shape))
    else:
        logger.debug('Not loading Y_validation set.')
    
    logger.debug('shape X_test: {0}'.format(x_test.shape))
    logger.debug('shape Y_test: {0}'.format(y_test.shape))
    

    return x_train, y_train, x_val, y_val, x_test, y_test
    
