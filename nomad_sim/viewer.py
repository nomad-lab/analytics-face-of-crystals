#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import absolute_import

__author__ = "Angelo Ziletti"
__copyright__ = "Copyright 2016, The NOMAD Project"
__maintainer__ = "Angelo Ziletti"
__email__ = "ziletti@fhi-berlin.mpg.de"
__date__ = "21/10/16"

import logging, os, sys
import numpy as np
import itertools
import pandas as pd

from jinja2 import Template
from bokeh.embed import components
from sklearn import preprocessing
from bokeh.plotting import figure
from bokeh.models import ColumnDataSource, HoverTool, CustomJS, TapTool, Rect
from six.moves import zip
from bokeh.models import Circle
from nomad_sim.patch_names_rs_zb import rename_material
from random import shuffle
from decimal import Decimal
from nomad_sim.utils_crystals import format_e
from nomad_sim.utils_crystals import convert_energy_substance
from scipy.spatial import ConvexHull

import warnings 
from pint import UnitRegistry
# disable DeprecationWarning for scikit learn
warnings.filterwarnings('ignore', category=DeprecationWarning)
# disable warnings from pint
logging.getLogger("pint").setLevel(logging.ERROR)


import nomadcore.unit_conversion.unit_conversion as uc
import nomadcore.parser_backend as parser

from nomad_sim.config import configs

try:
    hdlr = logging.FileHandler(configs["output_file"], mode='a')
except:
    hdlr = logging.FileHandler(configs["output_file"], mode='w')

ureg = UnitRegistry(os.path.normpath(configs["ureg_file"]))
level = logging.getLevelName(configs["log_level_general"])

css_file_path = configs["css_file_viewer"]

logger = logging.getLogger(__name__)
logger.setLevel(level)
logging.basicConfig(level=level)
FORMAT = "%(levelname)s: %(message)s"
formatter = logging.Formatter(fmt=FORMAT)
handler = logging.StreamHandler()
handler.setFormatter(formatter)
hdlr.setFormatter(formatter)
logger.addHandler(handler)
logger.addHandler(hdlr)
logger.setLevel(level)
logger.propagate = False

__metainfopath__ = configs["meta_info_file"]


class Viewer(object):
    """ Generate the NOMAD viewer from a loookup file. 
    
    By default, the lookup file needs to be called 'lookup.dat' and needs to be in the
    `tmp_folder`. This file is a space separated file where each row correspond to a 
    crystal structure with same parameters for the visualization. \n
    Such file needs to have the following columns: \n
    json | step_number | X | Y | target | label |chemical_formula | \n
    
    where

    json: string
        Absolute path to the json file from the NOMAD Archive that we wish 
        to read structures from. It can contain multiple frames.
    
    step_number: int
        For each json file, the frame number (relative to that 
        json file) of the structure we want to read.
        If only one frame is present, step_number=0
            
    X: float
        x position on the NOMAD viewer clustering plot of the structure of this row.

    Y: float
        y position on the NOMAD viewer clustering plot of the structure of this row.
    
    target: float, optional
        Value of the target property that it is going to be used to create the 
        color palette in the NOMAD viewer.

    label: float, optional
        For classification, specifies the class associated with the sample (row).
        At the moment, it is not used by the NOMAD Viewer.
        
    chemical_formula: string, optional
        Chemical formula of the material corresponding to the sample (row)
        At the moment, it is not used by the NOMAD Viewer
        
        
        
    Attributes
    ----------
    
    title : None or string
        Title of the viewer.
        
    width : float, default 600
        Width in pixels (?) of the webpage containing the NOMAD viewer 
        
    height : float, default 700
        Height in pixels (?) of the webpage containing the NOMAD viewer     
            
    Notes
    -------
    Examples of usage of the Viewer can be found at the NOMAD Analytics Toolkit.
        
    Supervised learning
    
    - https://analytics-toolkit.nomad-coe.eu/beaker/#/open?uri=%2Fhome%2Fbeaker%2Ftutorials%2Fsis_cscl.bkr
    
    - http://analytics-toolkit.nomad-coe.eu/tutorial-LASSO_L0 (pretty much the same as the first, data-wise, while the algorithm is different)
    
    Unsupervised learning
    
    - https://analytics-toolkit.nomad-coe.eu/beaker/#/open?uri=%2Fhome%2Fbeaker%2Ftutorials%2FEmbedding.bkr
    
    - https://analytics-toolkit.nomad-coe.eu/beaker/#/open?uri=%2Fhome%2Fbeaker%2Ftutorials%2FSOAP_similarity.bkr


    """    
        

    def __init__(self, title=None, name=None, width=600, height=700):

        if name is None:
            name = 'viewer'

        self.title = title
        self.width = width
        self.height = height
        self.name = name        


    def plot(self, archive=None, colors=None, frames=None, frame_list=None, clustering_x_list=None, 
             clustering_y_list=None, 
             is_classification=None,
             target_list=None, target_pred_list=None, target_unit=None, 
             target_name=None, target_class_names=None,
             energy_unit=None,
             descriptor=None, xray_img_list=None,
             legend_title=None, 
             x_axis_label=None, y_axis_label=None, plot_title=None,
             clustering_point_size=None, map_point_size=None, html_folder=None, tmp_folder=None,
             plot_convex_hull=None):
                 
        """ Plot the NOMAD Viewer using a given list of structures
        
        Parameters 
        ----------
        archive : string, list of `NOMADStructure` objects 
            List of structures from the class `NOMADStructure`. Each instance of 
            the class `NOMADStructure` can contain multiple frames         
            
        colors : string, list 
            Color palette used for the points in the plot. 
            For regression: hardcoded to 5 values. 
            Each color represents a quintile. 
            We use percentile to have a scale which is robust to outliers.
            For classification: hardcoded to 37 values. 
            Each color is associated to a unique class value.

        frames : string, optional, {'all', 'first', 'last', 'list'}
            Define which frames should be shown.\n
            'all': show all the frames in the `NOMADstructure` \n
            'first': show only 1st frame \n
            'last': show only last frame \n
            'list': show user-specified list of frames. 
            .. todo:: Check compatibility with multiple frames.


        frame_list : int, list (or list of lists)
            Specifies for each `NOMADstructure` the frames to load.
            It is a list if only one frame for each json file needs to be loaded,
            while it is a list of lists otherwise. Negative indeces are supported.

        clustering_x_list : float, list or list of list
            For each frame in a given `NOMADstructure`, it is the x_coord of the
            clustering plot of the NOMAD Viewer.
            If multiple frames need to be loaded by `NOMADstructure`, it is a list of lists.
    
        clustering_y_list : float, list or list of list
            For each frame in a given `NOMADstructure`, it is the y_coord of the
            clustering plot of the NOMAD Viewer.
            If multiple frames need to be loaded by `NOMADstructure`, it is a list of lists.
        
        target_list : float, list (or list of lists), optional        
            Used to define the colors of the points in the plot.
            If not specified, the total energy (`energy_total`) is used as color code.
            
        target_pred_list : float, list (or list of lists), optional        
            Prediction of the underlying model on the target value. 
            To be compared with target_list.
        
        target_class_names : float or string, list, optional
            List of classes for the case of classification.

        x_axis_label : float
            Label of the x-axis.
        
        y_axis_label : 
            Label of the y-axis.
        
        clustering_point_size : float, default 12
            Size of the points (in pt?) in the clustering plot.

        map_point_size : float, default `clustering_point_size`*0.75
            Size of the points (in pt?) in the map plot.       
        
        html_folder : string, default `tmp_folder`
            Path to the folder where the generated html page is saved.
        
        tmp_folder : string, default '/tmp/'
            Path to the temporary folder where the images, the input files, 
            the descriptor files, and the similarity matrix are written.
        
        plot_convex_hull : bool, default False
            calculate and plot convex hull for each color partition (class in case of classificication)
            
            
        Returns
        -------
        
        file_html_link : string
            html string that in the Beaker notebook generates the html link to the 
            viewer (name.html). For example,
            <a target=_blank href='/path/to/file/viewer.html'>Click here to open the Viewer</a>

            .. todo:: Add shaw support.      
        
        file_html_name : string
            Absolute path to where the NOMAD Viewer (html page) is generated.        
        

        """

        energy_list = []
        png_file_list =[]
        geo_file_list =[]
        name_list = [] 
        chemical_formula_list = []
        
        
        if tmp_folder is None:
            tmp_folder = '/tmp'            
            
        if html_folder is None:
            html_folder = tmp_folder

        if target_list is None:
            target_list = energy_list

        if target_unit is not None:
            target_unit_legend = target_unit
        else:
            target_unit = 'arb. units'
            target_unit_legend = ''
            
        if target_name is None:
            target_name = 'Target'
            
        if legend_title is None:
            legend_title = ''
            
        if frames is None:
            frames = 'all'

        if clustering_point_size is None:
            clustering_point_size = 12

        if map_point_size is None:
            map_point_size = clustering_point_size*0.75
            
        if plot_title is None:
            plot_title = 'Structural-similarity plot'
            
        if (x_axis_label is not None) and (x_axis_label is not None):
            show_axis = True
        else:
            show_axis = False

            
        # read output log and substitute html end of line characters
        out_log = os.path.join(tmp_folder, 'output.log')   
        outf = open(out_log, 'r')        
        outf_string = str(outf.read()).replace("\n", "<br>")        
        
        
        # extract the structure from the archive (list of structures)
        for archive_id, archive_item in enumerate(archive):
            structure = archive_item
            # obtain number of frames
            N_runs = structure.gIndexRunMax+1
            N_frames = structure.gIndexDescMax+1
 
            # make sure each element is a list, to have each element iterable for later use
            if clustering_x_list is not None and clustering_y_list is not None:
                if isinstance(clustering_x_list[archive_id], list)==False:
                    clustering_x_list[archive_id] = [clustering_x_list[archive_id]]

                if isinstance(clustering_y_list[archive_id], list)==False:
                    clustering_y_list[archive_id] = [clustering_y_list[archive_id]]

            if target_list is not None:
                if isinstance(target_list[archive_id], list)==False:
                    target_list[archive_id] = [target_list[archive_id]]   
 
            if target_pred_list is not None:
                if isinstance(target_pred_list[archive_id], list)==False:
                    target_pred_list[archive_id] = [target_pred_list[archive_id]]                                   

            # read-in attributes from structure
            # structure[i,j] returns energy, png_file, geo_file, name in position i,j in type Structure         
            # see the method __getitem__ in Class NOMADStructure 
            # in file "nomad_structures.py" from more details
            if frames=='all':
                structure_attributes =  [structure[i,j] for i in range(N_runs) for j in range(N_frames)]
            elif frames=='first':
                structure_attributes =  [structure[i,0] for i in range(N_runs)]
            elif frames=='first-last':
                # if only one structure first and last structures are the same - show only one
                if (N_frames-1) == 0:
                    structure_attributes =  [structure[i,0] for i in range(N_runs)] 
                else:
                    structure_attributes =  [structure[i,0] for i in range(N_runs)] + [structure[i,N_frames-1] for i in range(N_runs)]
            elif frames=='last':
                structure_attributes =  [structure[i, N_frames-1] for i in range(N_runs)]
            elif frames=='list':
                # extract frame_list if present
                if frame_list is not None:
                    # check if frame_list is a list or list of lists
                    if any(isinstance(el, list) for el in frame_list):
                        # frame is a list of lists -> extract the list referring to each archive
                        frame_slice = frame_list[archive_id]
                    else:
                        # frame is a list -> the frame_slice is the list itself
                        frame_slice = frame_list

                    # check for negative values of the list
                    try:
                        frame_slice = [x+structure.gIndexDescMax if x < 0 else x for x in frame_slice]
                    except:
                        if frame_slice < 0:
                            frame_slice = frame_slice+structure.gIndexDescMax 
        
                    # check if index is not out-of-bound
                    if isinstance(frame_slice, list):
                        if all((i<= structure.gIndexDescMax and i>=0) for i in frame_slice):
                            structure_attributes =  [structure[i, frame] for i in range(N_runs) for frame in frame_slice]
                        else:
                            logger.error("Frame_list contains at least one element out-of-bound. " + 
                                "For that sequence, the frame number needs to be between {0} and {1}. Maybe you have switched the order between the JSON files and the frame_list?"
                                .format(-structure.gIndexDescMax, structure.gIndexDescMax))
                    elif isinstance(frame_slice, int):
                        if frame_slice<=structure.gIndexDescMax and frame_slice>=0:
                            structure_attributes =  [structure[i, frame_slice] for i in range(N_runs)]
                        else:
                            logger.error("Frame_list is an integer out-of-bound. For that sequence, \nthe frame number needs to be between {0} and {1}."
                                .format(-structure.gIndexDescMax, structure.gIndexDescMax))
                        
                    else:
                        logger.error("Frame_slice is not valid. Please check the frame_list provided")

                else:
                    logger.error("Please specify a valid list of frames to be shown.")

            else:
                logger.error("Please specify a valid option for frames. Options available: 'all', 'first', 'first-last', 'last', 'list'.")

            # unpack the tuple structure_attributes
            if descriptor is not None:
                if descriptor.name == 'XrayDiffraction':
                    pass
            
            # energy_total is always in J
            energy, png_file, geo_file, chemical_formula, name = zip(*structure_attributes)
                    
            # create a list of list appending all the frames in the structures present in the archive
            energy_list.append(energy)
            png_file_list.append(png_file)
            geo_file_list.append(geo_file)
            chemical_formula_list.append(chemical_formula)
            name_list.append(name)

        # flatten the lists (since they could be list of lists)
        energy = list(itertools.chain(*energy_list))
        geo_file = list(itertools.chain(*geo_file_list))
        png_file = list(itertools.chain(*png_file_list))
        chemical_formula = list(itertools.chain(*chemical_formula_list))
        name = list(itertools.chain(*name_list))

        # convert energy to energy_unit
        energy = convert_energy_substance(unit='J', value=energy,
            ureg=ureg, 
            energy_unit=energy_unit, 
            length_unit=None)
        
        # HARD-CODED for binary dataset
        chemical_formula = rename_material(chemical_formula)

        if clustering_x_list is not None and clustering_y_list is not None:
            x = list(itertools.chain(*clustering_x_list))
            y = list(itertools.chain(*clustering_y_list))
        else:
            x = None
            y = None
                
        if target_list is not None:
            if isinstance(target_list, list)==True:
                target = list(itertools.chain(*target_list))
            else:
                target = target_list

        if target_pred_list is not None:
            if isinstance(target_pred_list, list)==True:
                target_pred = list(itertools.chain(*target_pred_list))
            else:
                target_pred = target_pred_list

        if target_pred_list is not None:
            if target_list is not None:
                if not is_classification:
                    abs_error = [abs(target_i - target_pred_i) for target_i, target_pred_i in zip(target, target_pred)]
                    abs_error_hover = [format_e(item) for item in abs_error] 

        #if xray_img_list is not None:
        #    xray_imgs = list(itertools.chain(*xray_img_list))            
        
        
        # set the total number of frames in the archive
        if len(energy) == len(geo_file) == len(png_file) == len(chemical_formula) == len(name):
            N_arch = len(energy)
        else:
            logger.error("Inconsistent list lengths in atomic structures.")

        if (x is None or y is None):
            logger.warning("Please provide x and y clustering coordinates for the plot.\nGenerating random coordinates.")
            # create random x and y (later they will be read from an actual calculation)
            x = np.random.rand(N_arch)
            y = np.random.rand(N_arch)

        # define a predefinite JSmol window
        whichJSmol = np.zeros(N_arch)
            
        # define energy for hover tools for correct format visualization
        # (truncation to a given number of significative digits)
#        energy_hover = np.around(energy, decimals=4)
        energy_hover = [format_e(item) for item in energy] 
        target_hover = [format_e(item) for item in target] 

        
        if target_pred_list is not None:
            # check if it is a tuple (as given for example by cnn model)  
            try: 
                target_pred_hover = [format_e(item) for item in target_pred]
#                target_pred_hover = [np.around(item, decimals=3) for item in target_pred]
            except:
                target_pred_hover = target_pred
#                target_pred_hover = np.around(target_pred_hover, decimals=3)

        # manipulate the target to use it in the color palette definition
        # reshape for use in the MinMaxScaler (single feature convention)
        target = np.asarray(target)
        target = target.reshape(-1,1) 

        
        # Quantile-based discretization function
        # Discretize variable into equal-sized buckets
        # to have a color scale which is robust to outliers
        df_target = pd.DataFrame(target, columns=['target'])
        df_energy = pd.DataFrame(energy, columns=['energy'])

        if is_classification:
            # works only if target is either 0 or 1 or 2 or 3 
            # (not sure if that is true)
            
            palette = ['#000000', '#0072B2', '#009E73', '#E69F00', '#CC79A7',
                       '#f44336', '#e91e63', '#9c27b0', '#673ab7',
                       '#3f51b5', '#2196f3', '#03a9f4', '#00bcd4',
                       '#009688', '#4caf50', '#8bc34a', '#cddc39',
                       '#ffeb3b', '#ffc107', '#ff9800', '#ff5722',
                       '#795548', '#9e9e9e', '#607d8b',
                       # darker colors
                       '#b71c1c', '#880e4f', '#4a148c', '#311b92',
                       '#1a237e', '#0d47a1', '#01579b', '#006064',
                       '#004d40', '#1b5e20', '#33691e', '#827717',
                       '#f57f17', '#ff6f00', '#e65100', '#bf360c',
                       '#3e2723', '#212121', '#263238']
                                   
            # get number of unique target values
            n_classes = len(list(set(df_target.values.flatten().tolist())))
            colors = [palette[int(target_value)] for target_value in df_target.values.flatten().tolist()]

        else:        
            # one color for each bin     
            # not color blind safe
            #palette = ['#000099','#004d99','#009900','#994d00', '#990000']

            # color-blind safe (http://mkweb.bcgsc.ca/colorblind/)
            palette = ['#000000', '#0072B2', '#009E73', '#E69F00', '#CC79A7']
            # try to divide in 5 quantiles, if it does not work divide in less
            for i in range(5, 0,-1):
                try:
                    target_bin = (pd.qcut(df_target['target'], i, labels=False)).values
                    logger.info('The color in the plot is given by the target value.')
                    bins = list((pd.qcut(df_target['target'], i, labels=False, retbins=True))[1])
#                    bins = np.around(bins, decimals=3)
                    bins = [format_e(item) for item in bins] 
                    colors = [palette[idx] for idx in target_bin ]
                    n_quantiles = i
                    break
                except:
                    pass
            
        # initialize the zoom window to zero
        x_zoom = np.zeros(N_arch)
        y_zoom = np.zeros(N_arch)
        width_zoom = np.zeros(N_arch)
        height_zoom = np.zeros(N_arch)

        # put the necessary info in a ColumnDataSource to use in the plot
        # ColumnDataSource is used by Bokeh
        # (the library for the interactive plot)
        if (target_pred_list is not None) and (xray_img_list is not None):
            source = ColumnDataSource(
                    data=dict(
                        x = x,
                        y = y,
                        jmol_file = [],
                        name = name,
                        chemical_formula = chemical_formula,
                        energy = energy_hover, 
                        target = target_hover,
                        target_pred = target_pred_hover,
                        imgs = png_file, 
                        geo_file = geo_file,
                        xray_imgs = xray_img_list,
                        colors = colors,
                        whichJSmol = whichJSmol 
                        )
                    )
        elif target_pred_list is not None:
            source = ColumnDataSource(
                    data=dict(
                        x = x,
                        y = y,
                        jmol_file = [],
                        name = name,
                        chemical_formula = chemical_formula,
                        energy = energy_hover, 
                        target = target_hover,
                        target_pred = target_pred_hover,
                        abs_error = abs_error_hover,
                        imgs = png_file, 
                        geo_file = geo_file,
                        colors = colors,
                        whichJSmol = whichJSmol 
                        )
                    )
                    
        else:
            source = ColumnDataSource(
                    data=dict(
                        x = x,
                        y = y,
                        jmol_file = [],
                        name = name,
                        chemical_formula = chemical_formula,
                        energy = energy_hover, 
                        target = target_hover,
                        imgs = png_file, 
                        geo_file = geo_file, 
                        colors = colors,
                        whichJSmol = whichJSmol 
                        )
                    )            

        # ColumnDataSource to use in the 'Map' plot
        # (we do not use the same because otherwise the glyph selection 
        # properties are passed automatically
        # and we loose the colors in the Map plot when a point is selected)
        sourceMap = ColumnDataSource(
                    data=dict(
                        x=x,
                        y=y,
                        colors=colors,
                        x_zoom = x_zoom,
                        y_zoom = y_zoom,
                        width_zoom = width_zoom,
                        height_zoom = height_zoom
                        )
                    )

        # personalized Hover tool
        # different according to the task (supervised learning, unsupervised,
        # showing decriptor image, etc...)
        if (target_pred_list is not None) and (xray_img_list is not None):
            hover = HoverTool(
                    tooltips="""
                    <div>
                        <div>
                            <img
                                src="@imgs" height="150" alt="@imgs" width="150"
                                style="float: center; margin: 15px 15px 15px 15px;"
                                border="1"
                            ></img>
                            <img
                                src="@xray_imgs" height="150" alt="@xray_imgs" width="150"
                                style="float: center; margin: 15px 15px 15px 15px;"
                                border="1"
                            ></img>
                        </div>
                        <div>
                            <span style="font-size: 15px; font-weight: bold;">@chemical_formula</span>
                            <span style="font-size: 15px; color: #666;">[$index]</span>
                        </div>
                        <div>
                            <span style="font-size: 15px; color: black;">@name </span>
                        </div>  
                        
                       <div>
                            <span style="font-size: 15px; color: black;">(x, y) = </span>
                            <span style="font-size: 15px; color: black;">(@x, @y) </span>
                        </div>  

                        <div>
                            <span style="font-size: 15px; color: black;"> Ref. """+str(target_name)+""" = </span>
                            <span style="font-size: 15px; color: black;">@target """+ str(target_unit)+""" </span>
                        </div>
                        <div>
                            <span style="font-size: 15px; color: black;"> Pred. """+str(target_name)+""" = </span>
                            <span style="font-size: 15px; color: black;">@target_pred """+ str(target_unit)+""" </span>
                        </div>                        
                    </div>
                    """
                    )


        elif target_pred_list is not None:
            hover = HoverTool(
                    tooltips="""
                <table class="nomad-tooltip">
                            <tr>
                                <th class="nomad-header-tooltip">System description 
                                <span style="font-size: 10px; color: #cccccc;">[$index]</span>
                                </th>
                            </tr>
                        <tr>
                            <td>
                                <span class="small-text-tooltip"">Chemical formula: </span>
                                <span class="small-text-tooltip"">@chemical_formula</span>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <p class="small-text-tooltip">Atomic structure preview </p> 

                            <img
                                src="@imgs" height="150" alt="@imgs" width="150"
                                style="float: center; margin: 15px 15px 15px 15px;"
                                border="1"
                            ></img>
                            </td>
                        </tr>
                            <tr>
                            <td align="center">
                                <p class="small-text-tooltip"> (click to load an interactive 3D view below)</p> 
                            </td>
                            </tr>



                        
                            <tr>
                                <th class="nomad-header-tooltip">Predictions on this system </th>
                            </tr>
                    
                        <tr>
                            <td>
                                <span class="small-text-tooltip">Ref. """+str(target_name)+""" = </span>
                                <span class="small-text-tooltip">@target """+ str(target_unit)+""" </span>
                            </td>
                        </tr>    
                        <tr>
                            <td>
                                <span class="small-text-tooltip"> Pred. """+str(target_name)+""" = </span>
                                <span class="small-text-tooltip">@target_pred """+ str(target_unit)+""" </span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="small-text-tooltip"> Abs. error = </span>
                                <span class="small-text-tooltip-error">@abs_error """+ str(target_unit)+""" </span>
                            </td>
                        </tr>
                        <tr>
                                <th class="nomad-header-tooltip"> More info </th>
                            </tr>
                        <tr>
                            <td>
                                <span class="small-text-tooltip">(x, y) = </span>
                                <span class="small-text-tooltip">(@x, @y) </span>
                            </td>
                        </tr>

                </table>
                
                    """
                    )
                
        else:
            hover = HoverTool(
                    tooltips="""
                <table class="nomad-tooltip">
                            <tr>
                                <th class="nomad-header-tooltip">System description 
                                <span style="font-size: 10px; color: #cccccc;">[$index]</span>
                                </th>
                            </tr>
                        <tr>
                            <td>
                                <span class="small-text-tooltip"">Chemical formula: </span>
                                <span class="small-text-tooltip"">@chemical_formula</span>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <p class="small-text-tooltip">Atomic structure preview </p> 

                            <img
                                src="@imgs" height="150" alt="@imgs" width="150"
                                style="float: center; margin: 15px 15px 15px 15px;"
                                border="1"
                            ></img>
                            </td>
                        </tr>
                            <tr>
                            <td align="center">
                                <p class="small-text-tooltip"> (click to load an interactive 3D view below)</p> 
                            </td>
                            </tr>

                        
                        <tr>
                                <th class="nomad-header-tooltip"> More info </th>
                            </tr>
                        <tr>
                            <td>
                                <span class="small-text-tooltip">(x, y) = </span>
                                <span class="small-text-tooltip">(@x, @y) </span>
                            </td>
                        </tr>

                </table>
                
                    """
                    )         

        # Create a set of tools to use in the Bokeh plot
        tools_1=hover,"wheel_zoom,box_zoom,pan,reset,tap,previewsave,resize"

        # create main clustering plot
        p1 = figure(title=plot_title, plot_width=600, plot_height=600, tools=tools_1, background_fill_color = '#f2f2f2',
            outline_line_width = 0.01, toolbar_location="left")
            
        p1.title_text_font ="Trebuchet MS";  p1.title_text_font_size = '30pt'; p1.title_text_color = '#20335d'
        p1.title_text_font_style = 'bold'; p1.title_text_baseline = 'bottom'
        # hide logo
        #p1.logo = None        
        if not show_axis:
            p1.axis.visible = None; p1.xgrid.grid_line_color = None; p1.ygrid.grid_line_color = None            
        else:
            p1.axis.visible = True
            p1.xaxis.axis_label  = x_axis_label
            p1.yaxis.axis_label  = y_axis_label
            p1.xaxis.axis_label_text_font_size  = "14pt"
            p1.yaxis.axis_label_text_font_size  = "14pt"



        # JS code to reset the plot area according to the selection of the user
        js_zoom="""
                var data = source.get('data');
                
                //read from cb_obj the start and end of the selection
                var start = cb_obj.get('start');
                var end = cb_obj.get('end');
                
                // save the values in the data source 
                data['%s'] = [start + (end - start) / 2];
                data['%s'] = [end - start];
                source.trigger('change');
            """

        p1.x_range.callback = CustomJS(
                args=dict(source=source), code=js_zoom % ('x_zoom', 'width_zoom'))
        p1.y_range.callback = CustomJS(
                args=dict(source=source), code=js_zoom % ('y_zoom', 'height_zoom'))

        # define the renderer and actually plot the point in figure p1 (Clustering plot)
        r1 = p1.circle('x', 'y', size=clustering_point_size, fill_color=colors, fill_alpha=1.0, source=source, line_color=None,

                               # set visual properties for selected glyphs

                               # set visual properties for non-selected glyphs
                               nonselection_fill_alpha=0.1,
                               nonselection_fill_color="blue",
                               nonselection_line_color=None,
                               nonselection_line_alpha=0.0)
        


        tools_2="pan,box_zoom,wheel_zoom,resize,reset"

        # create small figure with the Map of the clustering plot       
        p2 = figure(title='Map', plot_width=350, plot_height=300, tools=tools_2, background_fill_color= "#ffffff",
            outline_line_width=0.01, toolbar_location="right")

        p2.title_text_font ="Trebuchet MS";  p2.title_text_font_size = '18pt'; p2.title_text_color = '#20335d'
        p2.title_text_font_style = 'bold'; p2.title_text_baseline = 'bottom'
        p2.axis.visible = None; p2.xgrid.grid_line_color = None; p2.ygrid.grid_line_color = None
        p2.logo = None
        
        #p2.toolbar_location = None

        # define the renderer and actually plot the point in figure p2 (Map plot)
        r2 = p2.circle('x', 'y', size=map_point_size, fill_color=colors, fill_alpha=1.0, source=sourceMap, line_color=None)

        #r2.selection_glyph = Circle(fill_color='blue', line_color=None)
        r2.nonselection_glyph = Circle(fill_color='blue', fill_alpha=1.0, line_color=None)
        rect = Rect(x='x_zoom', y='y_zoom', width='width_zoom', height='height_zoom', fill_alpha=0.6,
                    line_color=None, fill_color='blue')
        # pass source (not sourceMap) otherwise the Box will not be shown on the Map plot
        p2.add_glyph(source, rect)
        
        if plot_convex_hull:
            for color_of_class in list(set(colors)):
                matching_indices = [i for i, cc in enumerate(colors) if cc==color_of_class]
                x_y_array = np.array([ (clustering_x_list[i][0],clustering_y_list[i][0]) for i in matching_indices ])
                len_match_ind = len(matching_indices)
                if len_match_ind>1:
                    if len_match_ind>2:
                        hull = ConvexHull(x_y_array)
                        x_for_hull, y_for_hull = x_y_array[hull.vertices].transpose()
                    else:
                        x_for_hull, y_for_hull = x_y_array.transpose()
                    p1.patch(x_for_hull, y_for_hull, color=color_of_class, alpha=0.5 )
                    p2.patch(x_for_hull, y_for_hull, color=color_of_class, alpha=0.5 )



        # JS code to be used in the callback to load the corresponding structure in JSmol
        # when user clicks on a point of the Clustering plot 
        js_loadJmol_1 ="""
                // get data source from Callback args
                var data = source.get('data');
 
                // obtain the index of the point that was clicked
                // cb_obj contains information on the tool used
                var inds = cb_obj.get('selected')['1d'].indices;
                """
        if target_pred_list:
            js_loadJmol_2 = """
                    //pick from the data source the corresponding file
                    var geo_file = data['geo_file'][inds];
                    var chemical_formula = data['chemical_formula'][inds];
                    var energy = data['energy'][inds];
                    var target = data['target'][inds];
                    var target_pred = data['target_pred'][inds];
                    """
        else:
            js_loadJmol_2 = """
                    //pick from the data source the corresponding file
                    var geo_file = data['geo_file'][inds];
                    var chemical_formula = data['chemical_formula'][inds];
                    var energy = data['energy'][inds];
                    var target = data['target'][inds];
                    """            
        js_loadJmol_3 ="""    
                // load in which JSmol applet the structure should be loaded
                // it is an array because it is included in the ColumnDataSource which needs to be iterable 
                var whichJSmol = data['whichJSmol'];

                
                // decide in which JSmol applet the structure should be loaded
                // swap the value between 0 and 1 to alternate the JSmol applet in which we should plot
                // only one value of the array is read (for convenience). It does not matter because the elements are all the same (either 0 or 1)
                // open the file in jsmol

                if (whichJSmol[inds] == 0) {
                var file= \"javascript:Jmol.script(jmolApplet0," + "'load "+ geo_file + " {1 1 1}; rotate x 0; rotate y 0; rotate z 0; set bondTolerance 0.45; ')" ; 
                //var file= \"javascript:Jmol.script(jmolApplet0," + "'load "+ geo_file + " {3 3 3}; rotate x 0; rotate y 0; rotate z 0; set bondTolerance 0.45; ')" ; 
                //var file= \"javascript:Jmol.script(jmolApplet0," + "'load "+ geo_file + " {3 3 3}; rotate x 10; rotate y 12; rotate z 6; set bondTolerance 0.45; ')" ; 
                location.href = file;
                // change all the values of the array
                for (var i = 0; i < whichJSmol.length; i++){         
                       whichJSmol[i] = 1;
                }
                writeInfoApplet0(chemical_formula, energy, geo_file);
                } 
                else if (whichJSmol[inds] == 1) {
                //var file= \"javascript:Jmol.script(jmolApplet1," + "'load "+ geo_file + " {3 3 3}; rotate x 10; rotate y 12; rotate z 6; set bondTolerance 0.45; ')" ; 
                //var file= \"javascript:Jmol.script(jmolApplet1," + "'load "+ geo_file + " {3 3 3}; rotate x 0; rotate y 0; rotate z 0; set bondTolerance 0.45; ')" ; 
                var file= \"javascript:Jmol.script(jmolApplet1," + "'load "+ geo_file + " {1 1 1}; rotate x 0; rotate y 0; rotate z 0; set bondTolerance 0.45; ')" ; 
                location.href = file;
                for (var i = 0; i < whichJSmol.length; i++){
                    whichJSmol[i] = 0;
                } 
                writeInfoApplet1(chemical_formula, energy, geo_file);
                }
                """
        
        if target_pred_list:
            js_loadJmol_4 = """writeSummary(chemical_formula, target, target_pred);"""
        else:
            js_loadJmol_4 = """writeSummary(chemical_formula, energy, target);"""

        js_loadJmol_5 = """
                // save the modification in the ColumnDataSource to keep the information for the next user click
                data['whichJSmol'] = whichJSmol;
                source.trigger('change');

        """

        js_loadJmol = js_loadJmol_1 + js_loadJmol_2 + js_loadJmol_3 + js_loadJmol_4 + js_loadJmol_5

        # returns the TapTool objects of p1 (Clustering plot) 
        taptool = p1.select(type=TapTool)


        # load the corrsponding crystal structure when a point on the Clustering plot is clicked 
        # load in either 1st or 2nd JSmol applet
        taptool.callback = CustomJS(args=dict(source=source), code=js_loadJmol)

        # plots can be a single Bokeh model, a list/tuple, or even a dictionary
        plots = {'Clustering': p1, 'Map': p2}

        script, div = components(plots)

        # template for the HTML page to be generated        
        html_template_viewer_1_a = '''<!DOCTYPE html>
        <html lang="en">
            <head>
                <meta charset="utf-8">
            <script>
            document.title = "NOMAD viewer";
            </script>  

                 <link rel="stylesheet" href="./jsmol/bokeh-0.11.1.min.css" type="text/css" />
                 <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.css">'''

                 
        html_template_viewer_1_b = '''<link rel="stylesheet" type="text/css" href="''' + str(css_file_path) + '''">''' 
            
        html_template_viewer_1_c = '''
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script> 
               <script type="text/javascript" src="https://cdn.pydata.org/bokeh/release/bokeh-0.11.1.min.js"></script>
               <script>window.Bokeh || document.write('<script src="./jsmol/bokeh-0.11.1.min.js"></script>
               <script src="https://code.jquery.com/jquery-1.11.2.js"></script>
                    {{ js_resources }}
                    {{ css_resources }}
                    {{ script }}

            <script>
                             
            function writeInfoApplet0(chemical_formula_, energy_, geo_file_) {
            document.getElementById("chemical_formula0").innerHTML = String(chemical_formula_);
            document.getElementById("energy0").innerHTML = String(energy_);
            document.getElementById("geo_link0").innerHTML = "View";
            document.getElementById("geo_link0").href = String(geo_file_);
            document.getElementById("geo_link0").target = "_blank";
            };
            
            function writeInfoApplet1(chemical_formula_, energy_, geo_file_) {
            document.getElementById("chemical_formula1").innerHTML = String(chemical_formula_);
            document.getElementById("energy1").innerHTML = String(energy_);
            document.getElementById("geo_link1").innerHTML = "View";
            document.getElementById("geo_link1").href = String(geo_file_);
            document.getElementById("geo_link1").target = "_blank";
            };'''

        if target_pred_list:
            write_summary_function ='''
               function writeSummary(chemical_formula_, target_, target_pred_){
                //check if the user actually clicked on one point on the plot
                if (chemical_formula_ != null && target_pred_ != null){
                    $("#clustering_info tbody").append(
                    "<tr class='clickable-row' data-href='url://www.google.com'>"+
                    "<td>" + String(chemical_formula_) + "</td>"+
                    "<td>" + String(target_) + "</td>"+
                    "<td>" + String(target_pred_) + "</td>"+
                    "</tr>");
                }
                }; 
            '''
        else:
            write_summary_function ='''            
                function writeSummary(chemical_formula_, energy_, target_){
                //check if the user actually clicked on one point on the plot
                if (chemical_formula_ != null && energy_ != null){
                    $("#clustering_info tbody").append(
                    "<tr class='clickable-row' data-href='url://www.google.com'>"+
                    "<td>" + String(chemical_formula_) + "</td>"+
                    "<td>" + String(energy_) + "</td>"+
                    "<td>" + String(target_) + "</td>"+
                    "</tr>");
                }
                }; 
            '''
        
        html_template_viewer_2 = '''function deleteRow(tableID) {
                var table = document.getElementById(tableID);
                var rowCount = table.rows.length;
 
                for(var i=2; i<rowCount; i++) {
                    var row = table.rows[i];
                    table.deleteRow(i);
                    rowCount--;
                    i--;
                    }
                };
            
            </script>  
<style>



.legend { list-style: none; }
.legend li { float: left; margin-right: 10px; }
.legend span { border: 1px solid #ccc; float: left; width: 12px; height: 12px; margin: 2px; 
}
/* your colors */

                       
.legend .label_0 { background-color: #000000; }
.legend .label_1 { background-color: #0072B2; }
.legend .label_2 { background-color: #009E73; }
.legend .label_3 { background-color: #E69F00; }
.legend .label_4 { background-color: #CC79A7; }


.legend .label_5 { background-color: #2196f3; }
.legend .label_6 { background-color: #03a9f4; }
.legend .label_7 { background-color: #00bcd4; }
.legend .label_8 { background-color: #009688; }
.legend .label_9 { background-color: #4caf50; }
.legend .label_10 { background-color: #8bc34a; }
.legend .label_11 { background-color: #cddc39; }
.legend .label_12 { background-color: #ffeb3b; }
.legend .label_13 { background-color: #ffc107; }
.legend .label_14 { background-color: #ff9800; }
.legend .label_15 { background-color: #ff5722; }
.legend .label_16 { background-color: #795548; }
.legend .label_17 { background-color: #9e9e9e; }
.legend .label_18 { background-color: #607d8b; }
.legend .label_19 { background-color: #b71c1c; }
.legend .label_20 { background-color: #880e4f; }
.legend .label_21 { background-color: #4a148c; }
.legend .label_22 { background-color: #311b92; }
.legend .label_23 { background-color: #1a237e; }
.legend .label_24 { background-color: #0d47a1; }
.legend .label_25 { background-color: #01579b; }
.legend .label_26 { background-color: #006064; }
.legend .label_27 { background-color: #004d40; }
.legend .label_28 { background-color: #1b5e20; }
.legend .label_29 { background-color: #33691e; }
.legend .label_30 { background-color: #827717; }
.legend .label_31 { background-color: #f57f17; }
.legend .label_32 { background-color: #ff6f00; }
.legend .label_33 { background-color: #e65100; }
.legend .label_34 { background-color: #bf360c; }
.legend .label_35 { background-color: #3e2723; }
.legend .label_36 { background-color: #212121; }
.legend .label_37 { background-color: #263238; }

                       

.legend .quintile_1 { background-color: #000000; }
.legend .quintile_2 { background-color: #0072B2; }
.legend .quintile_3 { background-color: #009E73; }
.legend .quintile_4 { background-color: #E69F00; }
.legend .quintile_5 { background-color: #CC79A7; }


 </style>
 
   </head><body id='fullwidth' class='fullwidth page-1'>
    <table style="width: 100%, border: 4">

        <tr>
            <table class="headerNOMAD">
                <tr>
                    <td class="label">
                        <img id="nomad" src="https://nomad-coe.eu/uploads/nomad/images/NOMAD_Logo2.png" width="229" height="100" alt="NOMAD Logo" />
                    </td>
                    <td class="input">
                       <span class="header-large-text">Viewer<br></span>
                        <span class="header-small-text">The&nbsp;NOMAD&nbsp;Laboratory <br></span>
                        <span>&nbsp;</span>
                    </td>
                </tr>
            </table>
        </tr>
        <tr>

            <table align="center" style="background-color: #F5F5F5">
                <tr align="center">
                    <td style="vertical-align: top;">
                        {{ div['Clustering'] }}'''
                    
            
        if is_classification:            
            # works only with 2 or 3 classes (?? please check)

            legend_1 = '''
            <span class="results-small-text"> <p align="center"> '''+ str(legend_title) + '''</p></span>
            '''
            
            legend_2_list = []
            
            for i in xrange(len(target_class_names)):                
                legend_2_list.append('''<li><span class="label_''' + str(i) + 
                    '''"></span><div class="legend-small-text">''' 
                    + str(target_class_names[i]) + 
                    ''' (Target=''' + str(i) + ''')</div> </li>''')
                    
            legend_2_ = ''.join(legend_2_list)
            
            legend_2 = '''<p align="center">
                <ul class="legend">''' + legend_2_ +'''</ul> </p>'''
                
    
            legend = legend_1 + legend_2

            
        else:
            legend_1 = '''
            <span class="results-small-text"> <p align="center"> '''+ str(legend_title) + '''</p></span>
            <p align="center">                
            <ul class="legend">
            '''
            # NOTE: this is ugly and it should be changed but it is not trivial 
            # to automatize (also the colors should be changed accordingly)
            if n_quantiles == 5:
                legend_2 = '''
                <li><span class="quintile_1"></span><div class="legend-small-text">''' +'['+str(bins[0])+str(target_unit_legend)+', '+str(bins[1])+str(target_unit_legend)+')' + '''</div> </li>
                <li><span class="quintile_2"></span><div class="legend-small-text">''' +'['+str(bins[1])+str(target_unit_legend)+', '+str(bins[2])+str(target_unit_legend)+')' + '''</div> </li>
                <li><span class="quintile_3"></span><div class="legend-small-text">''' +'['+str(bins[2])+str(target_unit_legend)+', '+str(bins[3])+str(target_unit_legend)+')' + '''</div> </li>
                <li><span class="quintile_4"></span><div class="legend-small-text">''' +'['+str(bins[3])+str(target_unit_legend)+', '+str(bins[4])+str(target_unit_legend)+')' + '''</div> </li>
                <li><span class="quintile_5"></span><div class="legend-small-text">''' +'['+str(bins[4])+str(target_unit_legend)+', '+str(bins[5])+str(target_unit_legend)+')' + '''</div> </li>
                </ul>
                </p>'''        
            elif n_quantiles == 4:
                legend_2 = '''
                <li><span class="quintile_1"></span><div class="legend-small-text">''' +'['+str(bins[0])+str(target_unit_legend)+', '+str(bins[1])+str(target_unit_legend)+')' + '''</div> </li>
                <li><span class="quintile_2"></span><div class="legend-small-text">''' +'['+str(bins[1])+str(target_unit_legend)+', '+str(bins[2])+str(target_unit_legend)+')' + '''</div> </li>
                <li><span class="quintile_3"></span><div class="legend-small-text">''' +'['+str(bins[2])+str(target_unit_legend)+', '+str(bins[3])+str(target_unit_legend)+')' + '''</div> </li>
                <li><span class="quintile_4"></span><div class="legend-small-text">''' +'['+str(bins[3])+str(target_unit_legend)+', '+str(bins[4])+str(target_unit_legend)+')' + '''</div> </li>
                </ul>
                </p>'''  
            elif n_quantiles == 3:
                legend_2 = '''
                <li><span class="quintile_1"></span><div class="legend-small-text">''' +'['+str(bins[0])+str(target_unit_legend)+', '+str(bins[1])+str(target_unit_legend)+')' + '''</div> </li>
                <li><span class="quintile_2"></span><div class="legend-small-text">''' +'['+str(bins[1])+str(target_unit_legend)+', '+str(bins[2])+str(target_unit_legend)+')' + '''</div> </li>
                <li><span class="quintile_3"></span><div class="legend-small-text">''' +'['+str(bins[2])+str(target_unit_legend)+', '+str(bins[3])+str(target_unit_legend)+')' + '''</div> </li>
                </ul>
                </p>'''              
            elif n_quantiles == 2:
                legend_2 = '''
                <li><span class="quintile_1"></span><div class="legend-small-text">''' +'['+str(bins[0])+str(target_unit_legend)+', '+str(bins[1])+str(target_unit_legend)+')' + '''</div> </li>
                <li><span class="quintile_2"></span><div class="legend-small-text">''' +'['+str(bins[1])+str(target_unit_legend)+', '+str(bins[2])+str(target_unit_legend)+')' + '''</div> </li>
                </ul>
                </p>'''                   
            elif n_quantiles == 1:
                legend_2 = '''
                <li><span class="quintile_1"></span><div class="legend-small-text">''' +'['+str(bins[0])+str(target_unit_legend)+', '+str(bins[1])+str(target_unit_legend)+')' + '''</div> </li>
                </ul>
                </p>'''  
                
            
            legend = legend_1 + legend_2
    
        html_template_viewer_2_1 = '''
        </td> 
        
            <td style="vertical-align: top;">
            
            <table class="instructions-table">
                <tr>
                     <td class="instructions-title-text">Instructions </td>
                </tr>
                <tr>
                    <td colspan=2 class="instructions-text">
                    
                    On the left, we provide an <b><i>interactive</i></b> plot of the data-analytics results. <br>
                    A menu to turn on/off interactive functions is located on the left side of the plot (just below the pinwheel logo).
                    <br><br>
                             
                    <span class="instructions-h1-text"> Basic features </span>
                    
                    <ul>
                        <li> By <i>hovering</i> over a point in the plot, information regarding that system is displayed. </li>
                        <li> By <i>clicking</i> over a point, an interactive 3D visualization of the structure appears 
                        in one of the bottom panels (alternating left and right panel at each click, 
                        for comparing the last two selections). </li> 

                    </ul>

                    <span class="instructions-h1-text"> Advanced features </span>
                    
                    <ul>
                        <li> You can <i>zoom-in</i> on a selected area activating the <i>box zoom</i> function (2nd button from the top). 
                        The full plot is still shown in the map on the right-side of this webpage, and a shaded area indicates where the selected area is in the plot. </li>
                        <li> You can modify the <i>aspect-ratio</i> activating the <i>resize</i> function (3rd button from the top),
                        and dragging the bottom-right corner of the plot.</li>
                    </ul>
                    
                    </td>
                </tr>
            </table>

            </td>
    
                        <td style="height:100%">
                            <table style="height:100%">
                                <tr>                            
                                    <td align="center" style="width: 100%; height:320px; vertical-align: top">
                                        {{ div['Map'] }} 
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" style=" vertical-align: top">
                                        <table id="clustering_info" align="center">
                                                <tr class='clickablea-row' data-href='url://www.google.com'>
                                                    <th colspan=3 class="selection"> Selection </th>
                                                </tr>
                                                
                                                '''

        if target_pred_list:
            html_template_viewer_3 = '''
                                                <tr>
                                                    <th> Name </th>
                                                    <th> Reference '''+str(target_name)+''' ['''+str(target_unit)+'''] </th>
                                                    <th> Predicted '''+str(target_name)+''' ['''+str(target_unit)+'''] </th>
                                                </tr>
                                      '''
        else:
            html_template_viewer_3 = '''
                                                <tr>
                                                    <th> Name </th>
                                                    <th> Energy [''' +str(target_unit)+ ''']</th>
                                                    <th> '''+str(target_name)+''' ['''+str(target_unit)+'''] </th> 
                                                </tr>
                                      '''            
                                  
        html_template_viewer_4 = '''      
                                            
                                    </table>
                                  <INPUT type="button" value="Clear Selection" onclick="deleteRow('clustering_info')" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <table id="jsmol_table">
                        <tr>
                            <th>Name</th>
                            <th>Energy ['''+str(target_unit)+''']</th>
                            <th>Geometry File</th>
                        </tr>
                        <tr>
                            <td> <div id="chemical_formula0"> &nbsp; </div> </td>    
                            <td> <div id="energy0"> &nbsp; </div> </td>    
                            <td> &nbsp; <a id="geo_link0"></a>  </td>  
                        </tr>
                        <tr>
                            <td colspan=3 class="none"> 
                                <div id="appdiv0"></div>
                            </td>  
                        </tr>
                        </table>
                    </td>


                    <td align="center">
                        <table id="jsmol_table">
                        <tr>
                            <th>Name</th>
                            <th>Energy ['''+str(target_unit)+''']</th>
                            <th>Geometry File</th>
                        </tr>
                        <tr>
                            <td> <div id="chemical_formula1"> &nbsp; </div> </td>    
                            <td> <div id="energy1"> &nbsp; </div> </td>    
                            <td> &nbsp; <a id="geo_link1"></a>  </td>  
                        </tr>
                        <tr>
                            <td colspan=3 class="none"> 
                                <div id="appdiv1"></div>
                            </td>  
                        </tr>
                        </table>
                    </td>
                </tr>
        <tr>
            <td colspan=2>
                <table>
                    <tr>
                        <td style="width:10%"> &nbsp;  </td>    
                        <td>
                            <span class="results-small-text"><br>'''+ outf_string + '''</span>
                        </td>    
                        <td> &nbsp; </td>  
                    </tr>
                </table>           
            </td>
        </tr>
            </table>

        </tr>
        <tr>
            <td> &nbsp; </td>
        </tr>

        </table>
    </body>
</html>
'''


        template = Template(html_template_viewer_1_a + html_template_viewer_1_b + html_template_viewer_1_c + write_summary_function +
            html_template_viewer_2 + legend + html_template_viewer_2_1 + html_template_viewer_3 + html_template_viewer_4)

        # javascript script to be included in the HTML page to load JSmol
        js_jsmol=""" 
        <script type="text/javascript" src="./jsmol/JSmol.min.js"></script>
        <script type="text/javascript">
        Jmol._isAsync = false;
        Jmol.getProfile() // records repeat calls to overridden or overloaded Java methods
        var jmolApplet0; // set up in HTML table, below
        var jmolApplet1; // set up in HTML table, below
        var chemical_formula;
        // use ?_USE=JAVA or _USE=SIGNED or _USE=HTML5
        jmol_isReady = function(applet) {
            Jmol._getElement(applet, "appletdiv").style.border="0px solid black"
         }      

        var  Info = {
            width: 400,
            height: 300,
            debug: false,
            color: "#FFFFFF",
            //color: "#F0F0F0",
            zIndexBase: 20000,
            z:{monitorZIndex:100},
            serverURL: "./php/jsmol.php",
            use: "HTML5",
            jarPath: "./jsmol/java",    // this needs to point to where the j2s directory is.
            j2sPath: "./jsmol/j2s",     // this needs to point to where the java directory is.
            jarFile: "./jsmol/JmolApplet.jar",
            isSigned: false,
            disableJ2SLoadMonitor: true,
            disableInitialConsole: true,
            readyFunction: jmol_isReady,
            allowjavascript: true,
            //script: "set antialiasDisplay;load async /home/ziletti/nomad-lab-base/analysis-tools/structural-similarity/tutorials/tmp/P2Uyj-OseIfJb-idQy8HkE-q5VYUp_0_0_aims.in; spin on"
        }


              $(document).ready(function() {
             
              $("#appdiv0").html(Jmol.getAppletHtml("jmolApplet0", Info));
              $("#appdiv1").html(Jmol.getAppletHtml("jmolApplet1", Info));
              }
              ); 


              var lastPrompt=0;

              </script>

            """



        # output static HTML file
        # isBeaker is to allow the viewer to work locally and on Beaker
        # with Beaker only certain files are accessible by the browsers
        # in particular, only webpage in "'/home/beaker/.beaker/v1/web/"
        # and subfolders can be accessed
        # Here, isBeaker is defined as global variable in the Tutorial notebooks
        # this is not clean, and it should be changed
        if configs["isBeaker"] == "True":
            #Beaker
            file_html_name = '/tmp/'+self.name+'.html'
            file_html_link = "<a target=_blank href='/user/tmp/"+self.name+".html'>Click here to open the Viewer</a>"
        else:
            # Local
            file_html_name = os.path.abspath(os.path.normpath(os.path.join(html_folder, 
                '%s.html' % (self.name))))        
            file_html_link = "<a target=_blank href='"+file_html_name+"'>Click here to open the Viewer</a>"


        # build the page HTML
        html = template.render( js_resources=js_jsmol,
                                script=script,
                                div=div)

        with open(file_html_name, 'w') as f:
            f.write(html)
            f.flush()
            f.close()

        logger.info("Click on the button 'View interactive 2D scatter plot' to see the plot.")
                
        
        return file_html_link, file_html_name

# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
