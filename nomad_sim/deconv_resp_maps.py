#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import absolute_import

__author__ = "Angelo Ziletti and Devinder Kumar"
__copyright__ = "Copyright 2017, The NOMAD Project"
__maintainer__ = "Angelo Ziletti"
__email__ = "ziletti@fhi-berlin.mpg.de"
__date__ = "10/12/16"

import os
#os.environ["THEANO_FLAGS"] = "mode=FAST_RUN,device=gpu2,floatX=float32,lib.cnmem=0.90" 
import sys
import os.path
import logging    
import nomad_sim_visualization
import numpy as np
from keras.optimizers import SGD, Adam
from keras.models import model_from_json
import cPickle as pickle
import matplotlib.pyplot as plt
from matplotlib import gridspec
from numpy import amin, amax
from numpy.random import rand

from nomad_sim.utils_plotting import make_multiple_image_plot
from nomad_sim.config import configs

try:
    hdlr = logging.FileHandler(configs["output_file"], mode='a')
except:
    hdlr = logging.FileHandler(configs["output_file"], mode='w')

level = logging.getLevelName(configs["log_level_general"])

logger = logging.getLogger(__name__)
logger.setLevel(level)
logging.basicConfig(level=level)
FORMAT = "%(levelname)s: %(message)s"
formatter = logging.Formatter(fmt=FORMAT)
handler = logging.StreamHandler()
handler.setFormatter(formatter)
hdlr.setFormatter(formatter)
logger.addHandler(handler)
logger.addHandler(hdlr)
logger.setLevel(level)
logger.propagate = False

__metainfopath__ = configs["meta_info_file"]
#-----------------------------------------------------------------------------------------------------------------------


def load_model(model_arch_file, model_weights_file):
    """ Load Keras model from .json and .h5 files"""
    
    json_file = open(model_arch_file, 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    model = model_from_json(loaded_model_json)
    logger.info("Loading model weights.")
    model.load_weights(model_weights_file)
    adam = Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
    model.compile(loss='categorical_crossentropy',
                  optimizer=adam,
                  metrics=['accuracy'])
    logger.debug("Model summary below.")
    
    return model


def load_data():
    """Load data from Pickle. DEPRECATED"""
    base_dir = "/home/ziletti/Documents/calc_xray/faces_crystals_tutorial/saved_models/"

    test_data = base_dir+'data_set_xtest.pkl'
    logger.info('loading test data')
    with open(test_data,'r') as f:
        X_test = pickle.load(f)
    logger.info('loading test data complete...')
    X_test = X_test.reshape(X_test.shape[0], 1, 64, 64)
    logger.info(X_test.shape[0], 'test samples')
    X_test = X_test.astype('float32')
    logger.info("data loaded : ", X_test.shape)

    return X_test


def plot_att_response_maps(data, model_arch_file, model_weights_file, 
    figure_dir,
    nb_conv_layers,
    layer_nb='all', 
    nb_top_feat_maps=4, 
    filename_maps="attentive_response_maps",
    filename_filter_sums="filters_sum",
    plot_all_filters=False,    
    plot_filter_sum=True,
    plot_summary=True):
    """ Plot attentive response maps given a Keras trained model and 
    input images.
    
    Parameters 
    ----------
    data : ndarray, shape (n_images, dim1, dim2) 
        Array of input images that will be used to calculate the 
        attentive response maps.
        
    model_arch_file : string
        Full path to the model architecture file (.json format) written
        by Keras after the neural network training.
        This is used by the load_model function to load the neural network
        architecture.

    model_weights_file : string
        Full path to the model weights file (.h5 format) written by Keras
        after the neural network training .
        This is used by the load_model function to load the neural network
        architecture.
            
    figure_dir : string
        Full path of the directory where the images resulting from the 
        deconvolution procedure will be saved.

    nb_conv_layers : int
        Numbers of Convolution2D layers in the neural network architecture. 

    layer_nb : list of int, or 'all'
        List with the layer number which will be deconvolved starting from 0.
        E.g. layer_nb=[0, 1, 4] will deconvolve the 1st, 2nd, and 5th 
        convolution2d layer. Only up to 6 conv_2d layers are supported.
        If 'all' is selected, all conv_2d layers will be deconvolved, 
        up to nb_conv_layers.
        
    nb_top_feat_maps : int
        Number of the top attentive response maps to be calculated and 
        plotted. It must be <= to the minimum number of filters used in the 
        neural network layers. This is not checked by the code, and 
        respecting this criterion is up to the user.
        
    filename_maps : str
        Base filename (without extension and path) of the files where the 
        attentive response maps will be saved. 
        
    filename_filter_sums : str
        Base filename (without extension and path) of the files where the 
        sum of the attentive response maps for each layer will be saved.

    plot_all_filters : bool   
        If True, plot and save the nb_top_feat_maps for each layer.
        The files will be saved in different folders according to the layer:
        - "convolution2d_1" for the 1st layer
        - "convolution2d_2" for the 2nd layer
        etc.
        
    plot_filter_sum : bool
        If True, plot and save the sum of all the filters for a given layer.

    plot_summary : bool
        If True, plot and save a summary figure containing:
        (left) input image
        (center) nb_top_feat_maps filters for each deconvolved layer
        (right) sum of the all filters of the last layer
        If set to True, also plot_filter_sum must be set to True. 

    """    
    model = load_model(model_arch_file,model_weights_file)
    
    if not os.path.exists(figure_dir):
        os.makedirs(figure_dir)

    nb_input_imgs = data.shape[0]
    img_size = (data.shape[1], data.shape[2])

    all_target_layers = []
    for idx_conv_layer in range(nb_conv_layers):
        all_target_layers.append('convolution2d_' + str(idx_conv_layer+1))
    
    
    # plot the attentive response maps for all images in the dataset            
    for idx_img in range(nb_input_imgs):
        plt.clf()
        logger.info("Calculating attentive response map for figure {0}/{1}".format(idx_img+1, nb_input_imgs))
        # plot input picture
        input_picture= data[idx_img]
        input_picture = input_picture.reshape(img_size)
        plt.axis('off')
        plt.imshow(input_picture, cmap='gray')
        
        # save input picture
        filename_input =  str(idx_img) + "_img_input.png"
        filename_input_full = os.path.abspath(os.path.normpath(os.path.join(figure_dir, filename_input))) 
        # to avoid whitespaces when saving
        plt.gca().xaxis.set_major_locator(plt.NullLocator())
        plt.gca().yaxis.set_major_locator(plt.NullLocator())
        plt.savefig(filename_input_full, dpi=100, bbox_inches='tight', pad_inches=0.0, format='png')
        
        # get one image at a time to deconvolve        
        input_data = data[idx_img]
        input_data = input_data.reshape(1, 1, img_size[0], img_size[1])

        if layer_nb=='all':
            target_layers = all_target_layers
        else:
            target_layers = [all_target_layers[i] for i in list(layer_nb)]
            
        output_layers = []
        for target_layer in target_layers:
            # ouput.shape = #img, #filters, #channels, #px, #py
            logger.info("Processing layer: {0}".format(target_layer))
            output, feat_maps = nomad_sim_visualization.deconv_visualize(model, target_layer, 
                input_data, nb_top_feat_maps, save=False)
            output_layers.append(output)

        output_layers = np.asarray(output_layers)
        # output_for_maps.shape = #img x #filters x #channels, #px, #py
        output_for_maps = np.reshape(output_layers, (-1, img_size[0], img_size[1]))                         


        filename_maps_i = filename_maps + "_img_" + str(idx_img) + ".png" 
        filename_maps_i_full = os.path.abspath(os.path.normpath(os.path.join(figure_dir, filename_maps_i)))         
        # plot images
        make_multiple_image_plot(output_for_maps, title="Attentive response maps", 
            n_rows=nb_top_feat_maps, n_cols=len(target_layers), 
            vmin=None, vmax=None, 
            save=True, filename=filename_maps_i_full)
        
        # processing one image at a time
        # output_for_filters.shape = #layers #filters #px, #py
        output_for_filters = np.reshape(output_layers, (len(target_layers), nb_top_feat_maps, img_size[0], img_size[1]))                         
                
        if plot_all_filters:
            for idx_layer, target_layer in enumerate(target_layers):    
                # make a dir for each target layer
                layer_dir = os.path.abspath(os.path.normpath(os.path.join(figure_dir, target_layer)))         
                if not os.path.exists(layer_dir):
                    os.makedirs(layer_dir)
                    
                for idx_filter in range(nb_top_feat_maps):
                    plt.clf()
        
                    data_filter = output_for_filters[idx_layer, idx_filter, :, :]
                    
                    #show only positive filters
                    vmin = max(0.0, amin(data_filter))
                    vmax = amax(data_filter)
                    plt.axis('off')
                    plt.imshow(data_filter, cmap='hot', vmin=vmin, vmax=vmax*1.0)
                
                    # to avoid whitespaces when saving
                    plt.gca().xaxis.set_major_locator(plt.NullLocator())
                    plt.gca().yaxis.set_major_locator(plt.NullLocator())

                    filter_name =  str(target_layer) + "_" + "filter_nb" + str(idx_filter) + "_image_"  + str(idx_img) + ".png"
                    filename = os.path.abspath(os.path.normpath(os.path.join(layer_dir, filter_name))) 
                    plt.savefig(filename, dpi=100, bbox_inches='tight', pad_inches=0.0, format='png')
                    

        if plot_filter_sum:
            for idx_layer, target_layer in enumerate(target_layers):    
                plt.clf()
                # make a dir for each target layer
                layer_dir = os.path.abspath(os.path.normpath(os.path.join(figure_dir, target_layer)))         
                if not os.path.exists(layer_dir):
                    os.makedirs(layer_dir)

                data_filter = output_for_filters[idx_layer, : , :, :]

                # sum up contributions from all filters to have the whole range of
                # possibilities
                combined_filters = np.amax(data_filter, axis=0)
                combined_filters = combined_filters.reshape(img_size)
                plt.axis('off')
                plt.imshow(input_picture, cmap='gray', vmin=0, vmax=0.05)
                plt.imshow(combined_filters, alpha=0.9, cmap='hot')
                # show only positive filter
                vmin = max(0.0, amin(combined_filters))
                vmax = amax(combined_filters)
                
                plt.imshow(combined_filters, cmap='hot', vmin=vmin, vmax=vmax*1.0)
                
                # save filter sum
                filter_sum_name =  str(target_layer) + "_image_" + str(idx_img) + "_sum.png"
                filename_filter_sum = os.path.abspath(os.path.normpath(os.path.join(layer_dir, filter_sum_name))) 
                # to avoid whitespaces when saving
                plt.gca().xaxis.set_major_locator(plt.NullLocator())
                plt.gca().yaxis.set_major_locator(plt.NullLocator())
                plt.savefig(filename_filter_sum, dpi=100, bbox_inches='tight', pad_inches=0.0, format='png')
        
        if plot_summary:
            if plot_filter_sum:
                filename_summary =  str(idx_img) + "_summary_plot.png"
                filename_summary_full = os.path.abspath(os.path.normpath(os.path.join(figure_dir, filename_summary))) 
                
                plt.clf()
                fig = plt.figure(figsize=(8, 6)) 
                plt.style.use('fivethirtyeight')
                input_img = plt.imread(filename_input_full)
                att_resp_maps = plt.imread(filename_maps_i_full)
                # plot sum of filters from last layer
                last_layer_filter_sum = plt.imread(filename_filter_sum)
    
                gs = gridspec.GridSpec(1, 3, width_ratios=[1, 4, 1]) 
                gs.update(left=0.05, right=0.90, wspace=0.05, hspace=0.0)

                ax0 = plt.subplot(gs[0])
                ax0.imshow(input_img)
                ax0.axis('off')
    
                ax1 = plt.subplot(gs[1])
                ax1.imshow(att_resp_maps)
                ax1.axis('off')
    
                ax2 = plt.subplot(gs[2])
                ax2.imshow(last_layer_filter_sum)
                ax2.axis('off')
        
                plt.savefig(filename_summary_full, dpi=100, bbox_inches='tight', format='png')
            else:
                raise Exception("Cannot produce plot summary without filter sum."
                    "Set plot_filter_sum=True.")
