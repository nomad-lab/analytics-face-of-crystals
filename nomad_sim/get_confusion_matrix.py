#!/usr/bin/env python3
# function to get the classificaton report and confusion matrix for test set from a saved keras CNN model.
import os
os.environ["THEANO_FLAGS"] = "mode=FAST_RUN,device=gpu1,floatX=float32,lib.cnmem=0.80,base_compiledir=/tmp/310251680/ " 
import numpy as np 
import matplotlib.pyplot as plt

from sklearn.metrics import classification_report, confusion_matrix

from keras.models import model_from_json
from keras.optimizers import SGD
from keras.utils import np_utils



nb_classes = 24

def load_data():
	base_dir = '/home/2015-0096_dl_data/imgCLEF/data/vgg/'
	#train_data = base_dir+'images_train.npy'
	test_data = base_dir+'images_test.npy'
	print('loading test data')
	with open(test_data,'rb') as f:
	    X_test = np.load(f)
	print('loading test data complete...')
	#Y_train = np.load('/home/2015-0096_dl_data/imgCLEF/data/labels_train.npy')
	Y_test = np.load('/home/2015-0096_dl_data/imgCLEF/data/labels_test.npy')
	print(X_test.shape)
	test_pic = X_test[0]
	print(test_pic.shape)
	plt.imshow(test_pic)
	return X_test,Y_test


#loading saved model
model_arch_file = "/home/310251680/work/scripts/imageCLEF_reprod/saved_models/model_data_vgg_aug_ep_30.json"
model_weights_file="/home/310251680/work/scripts/imageCLEF_reprod/saved_models/model_data_vgg_aug_ep_30.h5"

json_file = open(model_arch_file, 'r')
loaded_model_json = json_file.read()
json_file.close()
model = model_from_json(loaded_model_json)
print('loading model weights..')
model.load_weights(model_weights_file)

print(model)

sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
model.compile(loss='categorical_crossentropy',
                  optimizer=sgd,
                  metrics=['accuracy'])


#loading data and reshaping it according to keras model
X_test, y_test = load_data()
X_test = X_test.reshape(X_test.shape[0],1,224,224)
print(X_test.shape[0], 'test samples')
X_test = X_test.astype('float32')
X_test /= 255
#y_test = np_utils.to_categorical(y_test, nb_classes)

#compiling and caluclating the score of the model again
score = model.evaluate(X_test,y_test)
print(score)

#y_pred = model.predict(X_test)
#print(y_pred)
#print('argmax y_pred')
#y_pred = np.argmax(y_pred,axis=1)
#print(y_pred)

#prediting the lables of the test set
y_pred  = model.predict_classes(X_test)
print (y_pred)

#outputs the full classification report for all the classes
target_names = range(1,25)
print(classification_report(np.argmax(y_test,axis=1),y_pred))


def plot_confusion_matrix(cm, title='Confusion matrix', cmap=plt.cm.autumn):
	#plots the confusion matrix
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    #tick_marks = np.arange(len(iris.target_names))
    #plt.xticks(tick_marks, iris.target_names, rotation=45)
    #plt.yticks(tick_marks, iris.target_names)
    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')

cm = confusion_matrix(np.argmax(y_test,axis=1), y_pred)
np.set_printoptions(precision=2)
print('Confusion matrix, without normalization')
print(cm)
#plt.figure()
#plot_confusion_matrix(cm)
#plt.show()
