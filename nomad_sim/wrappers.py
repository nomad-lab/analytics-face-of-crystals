#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import absolute_import

__author__ = "Angelo Ziletti"
__copyright__ = "Copyright 2016, The NOMAD Project"
__maintainer__ = "Angelo Ziletti"
__email__ = "ziletti@fhi-berlin.mpg.de"
__date__ = "21/10/16"

import cPickle as pickle
import os
import logging
import sys
import stat
import json
import tarfile
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import copy
import multiprocessing

from scipy import constants

from sklearn.metrics.pairwise import cosine_similarity
from nomad_sim.nomad_structures import NOMADStructure
from nomad_sim.viewer import Viewer
from nomad_sim.convert import build_sim_matrix
from nomad_sim.gen_similarity_matrix import sim_matrix_to_embedding, load_sim_matrix
from nomad_sim.l1_l0 import choose_atomic_features, write_atomic_features, classify_rs_zb
from nomad_sim.l1_l0 import get_energy_diff, combine_features, l1_l0_minimization
from nomad_sim.sis import SIS
from bokeh.util.browser import view
from nomad_sim.cnn_preprocessing import read_data_sets
from nomad_sim.cnn_preprocessing import make_data_sets
from nomad_sim.cnn_preprocessing import load_data_from_pickle
from nomad_sim.tf_autoencoder import run_autoencoder
from nomad_sim.cnn_minimal import run_cnn_tensorflow
from nomad_sim.cnn_minimal import train_cnn_keras
from nomad_sim.cnn_minimal import predict_cnn_keras
from nomad_sim.keras_autoencoder import run_keras_autoencoder
from nomad_sim.utils_plotting import plot_save_cnn_results
from nomad_sim.utils_crystals import create_vacancies
from nomad_sim.descriptors import XrayDiffraction
from nomad_sim.descriptors import XrayDiffraction1D
from nomad_sim.descriptors import Descriptor
from nomad_sim.descriptors import AtomicFeatures
from nomad_sim.utils_crystals import get_spacegroup
from nomad_sim.utils_crystals import modify_crystal
from nomad_sim.utils_crystals import create_supercell
from nomad_sim.utils_binaries import get_binaries_dict_delta_e
from nomad_sim.utils_crystals import convert_energy_substance
from nomad_sim.utils_plotting import insert_newlines
from nomad_sim.utils_data_retrieval import write_summary_file

#from nomad_sim.cnn_preprocessing import DataSets
import nomadcore.unit_conversion.unit_conversion as uc
import nomadcore.parser_backend as parser
from nomadcore.local_meta_info import loadJsonFile, InfoKindEl

import warnings 
from pint import UnitRegistry
# disable DeprecationWarning for scikit learn
warnings.filterwarnings('ignore', category=DeprecationWarning)
warnings.filterwarnings('ignore', category=UserWarning)
logging.getLogger("pint").setLevel(logging.ERROR)


from nomad_sim.config import configs

try:
    hdlr = logging.FileHandler(configs["output_file"], mode='a')
except:
    hdlr = logging.FileHandler(configs["output_file"], mode='w')

ureg = UnitRegistry(os.path.normpath(configs["ureg_file"]))
level = logging.getLevelName(configs["log_level_general"])

logger = logging.getLogger(__name__)
logger.setLevel(level)
logging.basicConfig(level=level)
FORMAT = "%(levelname)s: %(message)s"
formatter = logging.Formatter(fmt=FORMAT)
handler = logging.StreamHandler()
handler.setFormatter(formatter)
hdlr.setFormatter(formatter)
logger.addHandler(handler)
logger.addHandler(hdlr)
logger.setLevel(level)
logger.propagate = False

__metainfopath__ = configs["meta_info_file"]


def get_json_list(
        method=None,
        data_folder=None,
        path_to_file=None,
        tmp_folder=None,
        show_preview=False,
        drop_duplicates=False,
        displace_duplicates=False,
        predicted_value=False,
        get_unique_list=True):
    """ Get the json file list.

    .. todo:: Split `method = 'file' and `method` = 'folder' in two different functions,
        and change the name `get_json_list` to `read_lookup_file` for `method` = 'file'

    Parameters
    ----------
    method : string, {'folder', 'file'}
        Specify how to obtain the json files where the structures are included. \n
        'folder': read from the specified folder all the NOMAD (json) files. \n
        'file': read summary file containing: JSON file, frame number, clustering x_coord, clustering y_coord

        .. todo:: Add cluster number.

        In addition to the file, also the path to the folder where the json files
        (`data_folder`) are stored needs to be given (for now)

        .. todo:: Remove this constraint.

    data_folder : string
        Folder where the json files are. Needs to be specified even if `method` = 'file'.

        .. todo:: Remove this constraint.

    path_to_file : string, optional, default `tmp_folder`/lookup.dat
        Absolute path where the file to be read is located. Only used if `method` = 'file'.
        If not specified, by default the code looks for a file called 'lookup.dat'
        in `tmp_folder`.

    tmp_folder : string
        Temporary folder. Used only if `method` = 'file', and `tmp_folder` is not specified.


    Returns
    -------

    json_list : list
        List with the absolute paths to the json files.

    frame_list : list or list of lists, optional (used only if `method` = 'file')
        For each (unique) json file, returns the frame number (relative to that
        json file) that appears in the lookup file.
        If multiple json files are present, it is a list of lists.

    x_list : list or list of lists, optional (used only if `method` = 'file')
        For each (unique) json file, returns the x_coord (relative to that
        json file) that appears in the lookup file.
        If multiple json files are present, it is a list of lists.

    y_list : list or list of lists, optional (used only if `method` = 'file')
        For each (unique) json file, returns the y_coord (relative to that
        json file) that appears in the lookup file.
        If multiple json files are present, it is a list of lists.

    target_list : list or list of lists, optional (used only if `method` = 'file')
        For each (unique) json file, returns the target (relative to that
        json file) that appears in the lookup file.
        If multiple json files are present, it is a list of lists.
        In the rocksalt/zincblend example, the target is (E_rs - E_zb)

    predicted_list : list or list of lists, optional (used only if `method` = 'file')

    """

    if method is None:
        method = 'folder'

    if method == 'file':
        if path_to_file is None:
            path_to_file = os.path.abspath(
                os.path.normpath(
                    os.path.join(
                        tmp_folder,
                        'lookup.dat')))

    if method == 'folder':
        if data_folder is not None:
            json_list = []
            
            if isinstance(data_folder, list)==False:
                data_folder = [data_folder]
            
            for folder in data_folder:            
                for root, dirs, files in os.walk(folder, topdown=True):
                    for file_ in files:
                        if file_.endswith(".json"):
                            json_list.append(os.path.join(root, file_))

            if show_preview is True:
                json_file = open(json_list[0], 'r')

                # This ensures that even if an exception is raised,
                # the json_file will still be closed properly.
                try:
                    data = json.load(json_file)
                    logger.debug(json.dumps(data, indent=4, sort_keys=True))

                finally:
                    json_file.close()

        else:
            logger.error(
                "Please specify a valid path to the folder where the data are stored.")
            sys.exit(1)


        return json_list

    elif method == 'file':
        if path_to_file is not None:
            if data_folder is not None:
                if predicted_value:
                    columns = [
                        'JSON',
                        'Step number',
                        'X',
                        'Y',
                        'target',
                        'label',
                        'chemical_formula',
                        'y_pred']
                else:
                    columns = ['JSON', 'Step number', 'X', 'Y',
                               'target', 'label', 'chemical_formula']

                # read from file
                df = pd.read_csv(path_to_file, delimiter=' ',
                                 names=columns)

                # drop duplicates to avoid to have points exactly superimposing
                # because it does not allow to load any structure when clicked
                # target and label are not included because with atomic features because
                # rocksalt/zincblend are exactly on top of each other
                if drop_duplicates:
                    # df['X_round']=df['X'].apply(lambda x:  np.around(x, decimals=3))
                    # df['Y_round']=df['Y'].apply(lambda x:  np.around(x, decimals=3))

                    # logger.debug("Sample with the same 'Step number', 'X' and 'Y'\n (up tp 3rd decimal point) and chemical_formula will be dropped.")
                    logger.debug(
                        "Samples with the same 'Step number' and chemical_formula will be dropped.")

                    # df.drop_duplicates(subset=['Step number', 'X_round', 'Y_round',
                    # 'chemical_formula'], keep='first', inplace=True)

                    df.drop_duplicates(
                        subset=[
                            'Step number',
                            'chemical_formula'],
                        keep='first',
                        inplace=True)

                    # df.drop('X_round', axis=1, inplace=True)
                    # df.drop('Y_round', axis=1, inplace=True)

                # artificially displace duplicates to allow action on click
                # generate new X (and Y) according to a normal distribution centered
                # around X (or Y) with variance X/250 (or Y/250)
                if displace_duplicates:
                    df['is_X_Y_duplicate'] = df.duplicated(
                        subset=['X', 'Y'], keep=False)

                    if any(item for item in df['is_X_Y_duplicate']):
                        logger.info(
                            "Same points in the plot have exactly the same X and Y coordinates.")
                        df['X'] = df.apply(
                            lambda row: np.random.normal(
                                loc=row.X,
                                scale=max(
                                    1e-3,
                                    row.X / 1e3),
                                size=None) if row.is_X_Y_duplicate is True else row.X,
                            axis=1)
                        df['Y'] = df.apply(
                            lambda row: np.random.normal(
                                loc=row.Y,
                                scale=max(
                                    1e-3,
                                    row.Y / 1e3),
                                size=None) if row.is_X_Y_duplicate is True else row.Y,
                            axis=1)
                        logger.info(
                            "The degenerate points in the plot are randomly displaced \n (by a very small amount) to allow actions on click.")

                # extract only the JSON filename (with extension)
                #json_filename = map(lambda x: os.path.basename(x), df['JSON'])
                json_filename = map(lambda x: os.path.abspath(os.path.normpath((x))), df['JSON'])
                df.insert(0, 'JSON filename', json_filename)
                df.drop('JSON', axis=1, inplace=True)

                #print json_filename
                
                # join the folder and the JSON filename to obtain the absolute
                # path to the JSON files
                #json_path = map(
                #    lambda x: os.path.abspath(
                #        os.path.normpath(
                #            os.path.join(
                #                data_folder,
                #                '%s' %
                #                (x)))),
                #    json_filename)
                
                json_path = json_filename
                df.insert(0, 'JSON path', json_path)

                # get unique JSON file
                if get_unique_list:
                    json_list = df['JSON path'].unique().tolist()
                else:
                    json_list = df['JSON path'].tolist()
                # set the dataframe index to JSON path for a faster search
                df = df.set_index(['JSON path'])

                frame_list = []
                x_list = []
                y_list = []
                target_list = []

                if predicted_value:
                    y_pred_list = []

                label_list = []
                chemical_formula_list = []

                for json_file in json_list:
                    frame_list_json = df['Step number'].loc[json_file].tolist()
                    # if only one frame, make a list of one element
                    # needed to be compatible with the viewer class
                    # (method="folder")
                    if not isinstance(frame_list_json, list):
                        frame_list_json = [frame_list_json]

                    x_list_json = df['X'].loc[json_file].tolist()
                    y_list_json = df['Y'].loc[json_file].tolist()

                    # put tolist() only when it is not text, it depends if None or not
                    # TO DO: make this cleaner

                    try:
                        target_list_json = df['target'].loc[json_file].tolist()
                    except:
                        target_list_json = df['target'].loc[json_file]

                    try:
                        label_list_json = df['label'].loc[json_file].tolist()
                    except:
                        label_list_json = df['label'].loc[json_file]

                    if predicted_value:
                        try:
                            y_pred_list_json = df['y_pred'].loc[
                                json_file].tolist()
                        except:
                            y_pred_list_json = df['y_pred'].loc[json_file]

                    chemical_formula_list_json = df[
                        'chemical_formula'].loc[json_file]

                    frame_list.append(frame_list_json)
                    x_list.append(x_list_json)
                    y_list.append(y_list_json)
                    target_list.append(target_list_json)
                    label_list.append(label_list_json)

                    if predicted_value:
                        y_pred_list.append(y_pred_list_json)

                    chemical_formula_list.append(chemical_formula_list_json)

                logger.debug(
                    "Information from the lookup file read correctly.")

                if predicted_value:
                    return json_list, frame_list, x_list, y_list, target_list, y_pred_list
                else:
                    return json_list, frame_list, x_list, y_list, target_list

            else:
                logger.error(
                    "Please specify a valid path to the folder where the data are stored.")
        else:
            logger.error(
                "Please specify a valid path to the file where the JSON files to read are listed.")
    else:
        logger.error(
            "Please specify a valid method. Available choices: 'folder', 'file'.")
        
# split a list into evenly sized chunks
def chunks(l, n):
    return [l[i:i+n] for i in range(0, len(l), n)]
    
def calc_descriptor_mp(data, job_number):
    total = len(data)
    chunk_size = total / job_number
    slice = chunks(data, chunk_size)
    jobs = []

    for i, s in enumerate(slice):
        j = multiprocessing.Process(target=calc_descriptor, args=(i, s))
        jobs.append(j)
    for j in jobs:
        j.start()

def calc_descriptor(
        desc_type=None,
        json_list=None,
        frame_list=None,
        file_format=None,
        desc_file=None,
        desc_folder=None,
        desc_info_file=None,
        write_desc=None,
        tmp_folder=None,
        take_first=True,
        is_rs_zb=True,
        dict_delta_e=None,
        selected_feature_list=None,
        derived_features=None,
        target_list=None,
        grayscale=None,
        cell_type=None,
        operations_on_structure=None,
        spacegroup_tuples=None,
        atoms_scaling=None,
        **kwargs):
    """ Calculates the descriptor.

    Starting from a list of json files, calculates for each file the descriptor
    specified by `desc_type`, and stores the results in the compressed archive
    desc_file in the directory `desc_folder`.

    Parameters
    ----------
    desc_type : str, optional, {'prdf', 'rdf', 'xray', 'atomic_features'}
        Specify with type of descriptor to calculate.\n
        'prdf': calculate the partial radial distribution function.\n
        'rdf': calculate the radial distribution function. This is like the 'prdf'
        'xray': calculates the x-ray diffraction pattern.\n
        'atomic_features': include primary features from file. 
    json_list : list
        List with the absolute paths to the json files.
    frame_list : list or list of list, optional
        For each json file, specifies with frames to consider in the descriptor
        calculation. If multiple json files are present, it is a list of lists.
    file_format : string, optional, {'NOMAD', 'rdf'}
        Specify what is the format of the file to read.
        Possible values: 'NOMAD' and 'rdf' (rdf is deprecated).
    desc_file : string, optional, default 'descriptor.tar.gz'
        Name of the compressed archive where the descriptors are written.
    desc_folder : string, optional, default `tmp_folder`
        Folder where the desc_file is written.
    write_desc : bool, optional, default `True`
        If `True`, write the descriptor to disk.
    tmp_folder : string
        Temporary folder.

    Examples
    --------

    Minimal example::

        from nomad_sim.wrappers import calc_descriptor


        tmp_folder = './your_temp_folder/'
        json_list = ['./data/graphene.json', './data/phosphorene.json', './data/phosphorene_oxides.json']

        calc_descriptor(desc_type='prdf',
        json_list=json_list, tmp_folder=tmp_folder)


    """

    if file_format is None:
        file_format = 'NOMAD'

    if write_desc is None:
        write_desc = True

    if desc_type is None:
        desc_type = 'prdf'

    if desc_folder is None:
        desc_folder = tmp_folder

    if desc_file is None:
        desc_file = 'descriptor.tar.gz'

    if desc_info_file is None:
        # if it ends with json the code is not working
        desc_info_file = os.path.abspath(
            os.path.normpath(
                os.path.join(
                    desc_folder,
                    'desc_info.json.info'))) 

    if selected_feature_list is not None:
        if len(selected_feature_list) < 2:
            logger.error('Please select at least two primary features.')
            sys.exit(1)

    # if json_list is only one element, make it a list
    if not isinstance(json_list, list):
        json_list = [json_list]

    desc_file = os.path.abspath(
        os.path.normpath(
            os.path.join(
                desc_folder,
                desc_file)))

    # make the log file empty (do not erase it because otherwise it does not
    # work)
    file_path = os.path.join(tmp_folder, 'output.log')
    open(file_path, 'w').close()
    # os.chmod(file_path, stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO)

    # remove control file from a previous run
    filelist = [f for f in os.listdir(
        tmp_folder) if f.endswith('control.json')]
    for file_ in filelist:
        file_path = os.path.join(desc_folder, file_)
        try:
            if os.path.isfile(file_path):
                os.remove(file_path)
        except Exception as e:
            logger.error(e)

    tar = tarfile.open(desc_file, 'w:gz')

    # load the tables with the data for the extended LASSO procedure
    # define this as None regardless of desc_type for now
    df_atomic_desc_selected = None

    if desc_type == 'atomic_features':
        
        metadata_info_path = os.path.normpath(os.path.join(os.path.dirname(os.path.abspath(__file__)), __metainfopath__)) 
        metadata_info, warns = loadJsonFile(filePath=metadata_info_path, dependencyLoader=None, 
            extraArgsHandling=InfoKindEl.ADD_EXTRA_ARGS,uri=None)    
            
        descriptor = AtomicFeatures(metadata_info=metadata_info, materials_calc_descriptorclass='binaries', **kwargs)

        chemical_formula_list = []
        energy_list = []
        label_list = []
                
        # make list of structure to avoid to read the files twice
        # the atomic features case is different wrt the others because 
        # we need to see all the structures before calculating the energy differences
        # for given spacegroups
        structure_list = []
        
        logger.debug("Reading atomic collection from {0}".format(descriptor.path_to_collection))
        logger.debug("Ordering atomic features by {0} of the elements".format(descriptor.feature_order_by))

    if desc_type == 'xray':
        filename_img_list = []
        filename_npy_list = []
        filename_rs_list = []
        filename_geo_list = []
        filename_target_file_list = []
        filename_thumbnail_file_list = []

        descriptor = XrayDiffraction(use_autoencoder=False, **kwargs)


    if desc_type == 'xray_1d':
        filename_target_file_list = []


    logger.info("Calculating descriptor: {0}".format(desc_type))
    logger.debug("Using {0} cell".format(cell_type))
    


    #----- start big loop over json files -----#
    for idx, json_file in enumerate(json_list):
        # read structure
        if (file_format == 'NOMAD') or (file_format == 'xyz'):
            structure = NOMADStructure(
                in_file=json_file,
                frame_list=None,
                file_format=file_format,
                take_first=take_first,
                cell_type=cell_type)
            

            if (idx % (int(len(json_list) / 10) + 1) == 0):
#                logger.debug(
#                    "Calculating descriptor: file {0}/{1}".format(idx + 1, len(json_list)))
                logger.info(
                    "Calculating descriptor: file {0}/{1}".format(idx + 1, len(json_list)))

            if desc_type == 'prdf':
                if write_desc:
                    # write prdf and add it to the tar file
                    tar.add(
                        structure.write_rdf(
                            path=desc_folder,
                            maxR=20,
                            is_prdf=True))
                            
            elif desc_type == 'rdf':
                if write_desc:
                    # write rdf and add it to the tar file
                    tar.add(
                        structure.write_rdf(
                            path=desc_folder,
                            maxR=20,
                            is_prdf=False))
                            
            elif desc_type == 'atomic_features':
                
                # append structure to list for later use
                structure_list.append(structure)

                # get descriptor for structure of binary
                df_desc_structure = descriptor.calculate(structure, selected_feature_list)

                # NOTE: this is probably inefficient, should be changed at some point
                if df_atomic_desc_selected is None:
                    # the 1st time only
                    df_atomic_desc_selected = df_desc_structure.copy(deep=True)
                else:
                    df_atomic_desc_selected = df_atomic_desc_selected.append(df_desc_structure, 
                        ignore_index=True) 
                
                # get chemical_formula, energy, classification (space group) for binaires
                chemical_formula, energy, label = get_spacegroup(structure, materials_class='binaries')
                
                chemical_formula_list.append(chemical_formula)
                energy_list.append(energy)
                label_list.append(label)

                # the last iteration calculate the energy differences between
                # spacegroup with all the json_file data
                if idx == len(json_list) - 1:    
                    if dict_delta_e is None:

                            # Dictionary with delta energy between spacegroups
                            dict_delta_e = get_binaries_dict_delta_e(chemical_formula_list, energy_list, label_list, spacegroup_tuples)

                    
                    # now that we have all the energy differencies, write the descriptor to file
                    logger.info("Writing descriptor to file.")
                    for structure_ in structure_list:
                        if write_desc:
                            tar.add(
                                descriptor.write(
                                    structure_,
                                    selected_feature_list,
                                    df=df_atomic_desc_selected,
                                    dict_delta_e=dict_delta_e,
                                    path=desc_folder,
                                    json_file=json_file))
                


            elif desc_type == 'xray':
                # write xray image and add it to the tar file
            
                # NOTE: this should be moved outside the json file loop
                # but only when all the descriptor are in the new 
                # form (as Descriptor class)
                #logger.debug("Descriptor: {0}".format(descriptor))
                #print descriptor
                #print descriptor.params(descriptor)
                structure_op_list = _apply_operations(structure, operations_on_structure)
                                
                for op_id, structure in enumerate(structure_op_list):
                    filename_img, filename_npy, filename_rs, filename_geo = descriptor.write(
                        structure, path=desc_folder, replicas=(1, 1, 1), 
                        grayscale=grayscale,
                        operation_number=op_id,
                        atoms_scaling=atoms_scaling)
                                            
                    xray_files = filename_img + filename_npy + filename_rs + filename_geo

                    # same target value for the modified structures
                    target_file = structure.write_target_values(
                        path=desc_folder, target=target_list[idx],
                        operation_number=op_id)
                    
                    thumbnail_file = structure.write_png(path=desc_folder, 
                        replicas=(1, 1, 1), rotation=True, 
#                        replicas=(1, 1, 1), rotation=True, 
                        operation_number=op_id)
                    
                    if configs["isBeaker"] == "True":
                        # do not add thumbnail_files to the Archive
                        archive_files = xray_files + target_file
                    else:
                        archive_files = xray_files + target_file + thumbnail_file

                    
                    for name in archive_files:
                        tar.add(name)

                    # append to the list
                    filename_target_file_list.append(target_file)
                    filename_thumbnail_file_list.append(thumbnail_file)
                    filename_img_list.append(filename_img)
                    filename_npy_list.append(filename_npy)
                    filename_rs_list.append(filename_rs)
                    filename_geo_list.append(filename_geo)
                    
                    

                # in the last step, if use_autoencoder=True apply the
                # autoencoder to the tarfile that contains all the images
                if idx == (len(json_list) - 1):
                    
                    if descriptor.use_autoencoder:
                        logger.info(
                            'Compressing descriptor with an autoencoder.')
                        tar.close()

                        # flatten the list of lists in a list
                        filename_img_list = [
                            item for sublist in filename_img_list for item in sublist]
                        filename_npy_list = [
                            item for sublist in filename_npy_list for item in sublist]
                        filename_rs_list = [
                            item for sublist in filename_rs_list for item in sublist]
                        filename_geo_list = [
                            item for sublist in filename_geo_list for item in sublist]
                        filename_target_file_list = [
                            item for sublist in filename_target_file_list for item in sublist]
                        filename_thumbnail_file_list = [
                            item for sublist in filename_thumbnail_file_list for item in sublist]

                        descriptor.compress_and_write(
                            filename_img_list,
                            filename_npy_list,
                            filename_rs_list,
                            desc_folder=desc_folder,
                            desc_file=desc_file,
                            target_name='target',
                            target_categorical=True,
                            disc_type='uniform')

                        tar = tarfile.open(desc_file, 'w:gz')

                        for name in filename_img_list + filename_npy_list + filename_target_file_list + filename_thumbnail_file_list:
                            tar.add(name)

                    
            elif desc_type == 'xray_1d':

                descriptor = XrayDiffraction1D(**kwargs)
                #logger.debug("Descriptor: {0}".format(descriptor))                

                #xrd_data = descriptor.calculate(structure)

                xray_files = descriptor.write(structure, path=desc_folder)
                
                target_file = structure.write_target_values(
                    path=desc_folder, target=target_list[idx])
                
                
                if isinstance(xray_files, list)==False:
                    xray_files = [xray_files]
                    
                archive_files = xray_files + target_file
                
                for name in archive_files:
                    tar.add(name)
                    

            else:
                logger.error(
                    "Please specify a valid file descriptor type (desc_type). Allowed desc_type values: 'prdf', 'rdf', 'atomic_features'.")
                sys.exit(1)
        else:
            logger.error(
                "Please specify a valid file format. The only possible file format is 'NOMAD'.")
            sys.exit(1)

    #----- end big loop over json files -----#


    # remove the temporary JSON files created while calculating the descriptor
    # NOTE: the order is not the same as the JSON files!
    # do not use for the desc_info_file
    f_json = [f for f in os.listdir(desc_folder) if f.endswith('.json')]
    f_info = [f for f in os.listdir(desc_folder) if f.endswith('.info')]
    f_xray = ([f for f in os.listdir(desc_folder) if f.endswith('_xray.png')])
    f_xray_ph = ([f for f in os.listdir(desc_folder)
                  if f.endswith('_xray_ph.png')])
    f_xray_rs = ([f for f in os.listdir(desc_folder)
                  if f.endswith('_xray_rs.png')])
    f_npy = ([f for f in os.listdir(desc_folder) if f.endswith('.npy')])
    f_geo = ([f for f in os.listdir(desc_folder) if f.endswith('_aims.in')])
    f_thumbnail = ([f for f in os.listdir(desc_folder) if f.endswith('_geometry.png')])

            
    logger.debug("Writing descriptor information to file.")
    if desc_type == 'xray':
        descriptor.write_desc_info(desc_info_file, json_list, 
            operations_on_structure=operations_on_structure, filename_img_list=filename_img_list)
    else:
        descriptor.write_desc_info(desc_info_file, json_list, filename_img_list=None)
        
    # add desc_info_file to tar archive
    # only to xray part to insure compatibility with tutorials
    if desc_type == 'xray':
        tar.add(desc_info_file)

    tar.close()

    # open the Archive and write summary file
    # here we substitute the full path with the basename to be put in the tar archive
    # TO DO: do it before, when the files are added to the tar
    write_summary_file(desc_file, tmp_folder, clean_tmp=True)


    logger.info("Descriptor calculation: done.")

    # do not use this if you want to show the x_ray images in the hover
    filelist = f_npy + f_info + f_geo + f_xray + f_xray_rs + f_xray_ph + f_json + f_thumbnail
#    filelist = f_json + f_npy + f_xray_rs + f_xray_ph

    #filelist = []
    
    for file_ in filelist:
        file_path = os.path.join(desc_folder, file_)
        try:
            if os.path.isfile(file_path):
                os.remove(file_path)
        except Exception as e:
            logger.error(e)
    
    # HARDCODED
    if desc_type == 'prdf' and is_rs_zb is True:
        try:
            labels = [el.values() for el in label_list]
            # flatten the label list
            labels = [item for sublist in labels for item in sublist]
            return labels
        except:
            pass
    elif descriptor is not None:
        return descriptor

            
    return descriptor


def calc_model(
        method=None,
        desc_folder=None,
        tmp_folder=None,
        matrix_file=None,
        desc_file_list_train=None,
        desc_file_list_test=None,
        desc_type=None,
        descriptor=None,
        lookup_file=None,
        energy_unit=None,
        length_unit=None,
        allowed_operations=None,
        derived_features=None,
        max_dim=None,
        sis_control=None,
        control_file=None,
        results_file=None,
        target_name=None,
        target_categorical=None,
        disc_type=None,
        n_rows=None,
        n_columns=None,
        n_bins=None,
        checkpoint_dir=None,
        checkpoint_filename=None,
        n_steps=None,
        train=True,
        plot_filters=True,
        nb_epoch=None,
        data_augmentation=False,
        split_train_val=True,
        read_from_file=False,
        path_to_x_train=None,
        path_to_y_train=None,
        path_to_x_val=None,
        path_to_y_val=None,
        path_to_x_test=None,
        path_to_y_test=None):
            
    """ Calculates model.

    .. todo:: Add documentation.

    """

    if tmp_folder is None:
        tmp_folder = '/tmp/'

    if desc_folder is None:
        desc_folder = tmp_folder

    if matrix_file is None:
        matrix_file = os.path.abspath(
            os.path.normpath(
                os.path.join(
                    desc_folder,
                    'data.npz')))

    if results_file is None:
        results_file = os.path.abspath(
            os.path.normpath(
                os.path.join(
                    tmp_folder,
                    'results.csv')))        
        
    if lookup_file is None:
        lookup_file = os.path.abspath(
            os.path.normpath(
                os.path.join(
                    tmp_folder,
                    'lookup.dat')))   
        
    if control_file is None:
        control_file = os.path.abspath(
            os.path.normpath(
                os.path.join(
                    tmp_folder,
                    'control.json'))) 

    if checkpoint_dir is None:
        checkpoint_dir = tmp_folder

    if checkpoint_filename is None:
       checkpoint_filename = 'model_cnn'

    if method == 'ext_lasso' or method == 'SIS':

        if max_dim is None:
            max_dim = 3

        # build the similarity matrix from the JSON files and save it to a file
        build_sim_matrix(
            desc_folder=desc_folder,
            matrix_file=matrix_file,
            f_count_max=1000,
            desc_type=desc_type)

        
        # load similarity matrix
        X, X_labels, target, lookup = load_sim_matrix(
            matrix_file=matrix_file, desc_type=desc_type)


        logger.debug("Only lowest energy structures are kept in the feature"
            " space generation.")

        # target is a list of tuples 
        # converter works for either float or lists
        # convert target (always in Joule if energy) in energy_unit
        target = [convert_energy_substance('J', list(item), 
            ureg=ureg, energy_unit=energy_unit, 
            length_unit=length_unit) for item in target]            


        # build dataframe with data to combine features
        json_file_path = lookup[:, 1]
        frame_number = lookup[:, 2]
        chemical_formula = lookup[:, 4]
        energy = lookup[:, 5]

        json_file_path = np.asarray(json_file_path).reshape(-1, 1)
        frame_number = np.asarray(frame_number).reshape(-1, 1)        
        chemical_formula = np.asarray(chemical_formula).reshape(-1, 1)
        energy = np.asarray(energy).reshape(-1, 1)
        data = np.concatenate((X, json_file_path, frame_number, chemical_formula, energy, target), axis=1)
        X_labels.append('json_file_path')
        X_labels.append('frame_number')
        X_labels.append('chemical_formula')
        X_labels.append('energy')
        X_labels.append('target')
                        
        df = pd.DataFrame(data=data, columns=X_labels)
        df['energy'] = df['energy'].apply(pd.to_numeric)
        # find rows that correspond to lowest energy structures
        df = df.sort_values(by='energy').groupby(['chemical_formula'], as_index=False).first()




        # copy dataframe with features only to give to l1-l0 minimization
        df_features = df.copy(deep=True)

        #for item in df_features['json_file_path'].tolist():
        #    print item
        
        target = np.asarray(df['target'].values.astype(float))
            
        # drop columns that are not features
        df_col_list = df_features.columns.tolist()
        
        if 'json_file_path' in df_col_list:
            df_features.drop('json_file_path', axis=1, inplace=True)
        if 'frame_number' in df_col_list:
            df_features.drop('frame_number', axis=1, inplace=True)
        if 'energy' in df_col_list:
            df_features.drop('energy', axis=1, inplace=True)
        if 'chemical_formula' in df_col_list:
            df_features.drop('chemical_formula', axis=1, inplace=True)
        if 'target' in df_col_list:
            df_features.drop('target', axis=1, inplace=True)
        if 'index' in df_col_list:
            df_features.drop('index', axis=1, inplace=True)
            
            
        # load the file containing the atomic metadata
        metadata_info_path = os.path.normpath(os.path.join(os.path.dirname(os.path.abspath(__file__)), __metainfopath__)) 
        metadata_info, warns = loadJsonFile(filePath=metadata_info_path, dependencyLoader=None, 
            extraArgsHandling=InfoKindEl.ADD_EXTRA_ARGS,uri=None)          


        '''
        print df[['Eh','C']]

        print df.isnull().values.any()

        # if there are nan, drop entire row
        if df.isnull().values.any():
            #df.dropna(axis=0, how='any', inplace=True)
            #df.reset_index(inplace=True)
            logger.info('Dropping samples for which the selected features are not available.')
        '''

        # convert numerical columns in float
        for col in df_features.columns.tolist():
            df_features[str(col)] = df_features[str(col)].astype(float)

        # make dict with metadata name: shorname
        features = df_features.columns.tolist()
        features = [feature.split('(', 1)[0] for feature in features]
        
        shortname = []
        # in foor loop to allow exception
        for feature in features:
            try:
                shortname.append(metadata_info[str(feature)]['shortname'])
            except:
                shortname.append(feature)

        features_shortnames = dict(zip(features, shortname))
       
        if method == 'ext_lasso': 
            # combine features
            df_combined = combine_features(
                df=df_features, 
                energy_unit=energy_unit,
                length_unit=length_unit,
                metadata_info=metadata_info,
                allowed_operations=allowed_operations,
                derived_features=derived_features)


            # if np.any(np.isnan(df_combined.as_matrix()))==True or np.all(np.isfinite(df_combined.as_matrix())):
            #    logger.error("The selected primary features/allowed operation pair led to over/underflow. Please select a different set of features and/or operations.")        

            feature_list = df_combined.columns.tolist()
            
            # replace metadata info name with shortname
            # using the {'metadata_name': 'metadata_shorname'}
            for fullname, shortname in features_shortnames.items():
                feature_list = [item.replace(fullname.lower(), shortname) for item in feature_list]
                    
            # feature selection using l1-l0
            # it is a list of panda dataframe:
            # 1st el: 1D, 2nd: 2d, 3rd 3D
            try:
                df_desc, y_pred = l1_l0_minimization(target, df_combined.values, feature_list,
                    energy_unit=energy_unit, max_dim=max_dim, print_lasso=True, lassonumber=25,
                    lambda_grid_points=100)
            
            except ValueError:
                logger.error(
                    "The selected primary features/allowed operation pair led to over/underflow in the LASSO procedure. ")
                logger.error("Please select a different set of features and/or operations. ")
                logger.error("Hint: try to remove Z_val, r_sigma, r_pi and/or the x+y / |x+y| operation.")
                logger.error("and/or use [energy]=eV and [length]=angstrom.")

                sys.exit(1)

        if method=='SIS':

            feature_matrix = df_features.values
            feature_list = df_features.columns.tolist()
             # replace metadata info name with shortname
             # using the {'metadata_name': 'metadata_shorname'}
            for fullname, shortname in features_shortnames.items():
                feature_list = [item.replace(fullname.lower(), shortname) for item in feature_list]
            # Dictionaries with parameters for initialization and local paths, remote paths, and SIS parameters
            sis = SIS(target, feature_matrix, feature_list, 
                target_unit=energy_unit,
                control=sis_control, output_log_file = '/tmp/output.log', rm_existing_files=True)

            # start
            sis.start()

            #results
            results = sis.get_results()
            df_desc = [ dic['D'] for dic in results ]
            y_pred  = [ dic['P_pred'] for dic in results ]


        logger.info("Selecting optimal descriptors: done.")

        x_coord = df_desc[1].iloc[:, 0].values
        y_coord = df_desc[1].iloc[:, 1].values

        x_axis_label = df_desc[1].columns.values.tolist()[0]
        y_axis_label = df_desc[1].columns.values.tolist()[1]


        # write a lookup file for the viewer
        lookup_out = []
        for i in range(df.shape[0]):
            lookup_out.append((
                df['json_file_path'][i],
                df['frame_number'][i],
                # x_coord
                str(x_coord[i]),
                # y_coord
                str(y_coord[i]),
                # target value
                df['target'][i],
                # chemical_formuladiff_
                'NaN',
                df['chemical_formula'][i],
                # predicted value (2D descriptor)
                str(y_pred[1][i])
            ))

        # write log file with general info about the model
        with open(lookup_file, "w") as f:
            f.write("\n".join([" ".join(x) for x in lookup_out]))

        with open(control_file, "w") as f:
            f.write("""
    {
          "model_info":[""")

            writeColon = False
            model_info = {
                "x_axis_label": x_axis_label,
                "y_axis_label": y_axis_label,
            }
            if (writeColon):
                f.write(", ")
            writeColon = True
            json.dump(model_info, f, indent=2)
            f.write("""
    ] }""")
            f.flush()



def calc_embedding(
        tmp_folder=None,
        desc_folder=None,
        desc_file=None,
        matrix_file=None,
        lookup_file=None,
        target_name=None,
        class_count=None,
        embed_method=None,
        embed_params=None,
        desc_type=None,
        input_dims=None,
        standardize=False,
        energy_unit=None,
        length_unit=None,
        derived_features=None,
        use_xray_img=True,
        model_arch_file=None,
        model_weights_file=None,
        nb_nn_layer=None,
        path_to_x_test=None,
        n_bins=None, 
        disc_type=None,
        target_categorical=None,
        batch_size=None):
            
    """ Embed high-dimensional data in a two-dimensional space.

    Starting from the tar.gz file that contains the descriptor, calculates the 2d-embedding.
    Results (which are the input for the Viewer) are written in `tmp_folder`/lookup.dat'.

    Parameters
    ----------

    tmp_folder : string, default '/tmp/'
        Temporary folder.

    desc_folder : string, default `tmp_folder`
        Folder where the descriptor were saved. By default it coincides with `tmp_folder`.

    matrix_file : string, default `desc_folder`/data.npz
        File where the similarity matrix is saved.

    lookup_file : string
        File where the results of the clustering are written.

    class_count : int, optional, default: ?
        Maximum number of classes to be written in the `lookup_file`.

    embed_method : string, {'tsne', 'spectral_embedding', 'mds', 'isomap', 
        'hessian', 'pca', 'ipca', 'kernel_pca', 'truncated_svd'}
        two-dimensional embedding method.
        Please refer to 'gen_similarity_matrix.py' for more details on the methods.

    embed_params : dict, optional
        Dictionary containing parameters to feed in the `embed_method`.
        Overwrites the default parameters specified in 'gen_similarity_matrix.py'.
        E.g.:  `embed_params` = {'verbose': 1} will overwrite the `verbose` default value
        given in 'gen_similarity_matrix.py'.

    """

    if tmp_folder is None:
        tmp_folder = '/tmp/'

    if desc_folder is None:
        desc_folder = tmp_folder

    if matrix_file is None:
        matrix_file = os.path.abspath(
            os.path.normpath(
                os.path.join(
                    desc_folder,
                    'data.npz')))

    if embed_params is None:
        embed_params = {}

#    if isinstance(desc_file, list)==False:
#        desc_file = [desc_file]
#    
#    desc_file_list = desc_file
#    
#    logger.debug('Descriptor file_list: {0}'.format(desc_file_list))
#    
#        
#    desc_file_list = [os.path.abspath(os.path.normpath(os.path.join(desc_folder, desc_file))) for desc_file in desc_file_list]
#    
#
#    images_list = []        
#
#    for desc_file in desc_file_list:
#        # if the final array shape is not know, it is faster to append to a list and 
#        # covert to array than append to numpy array 
#        images_ = extract_images(desc_file, desc_folder=desc_folder)   
#        images_list.append(images_)     
        
    # build the similarity matrix from the JSON files
    build_sim_matrix(
        target_name=target_name,
        target_categorical=target_categorical,
        desc_folder=desc_folder,
        desc_file=desc_file,
        matrix_file=matrix_file,
        f_count_max=5000,
        desc_type=desc_type,
        input_dims=input_dims,
        use_xray_img=use_xray_img,
        model_arch_file=model_arch_file,
        model_weights_file=model_weights_file,
        nb_nn_layer=nb_nn_layer,
        path_to_x_test=path_to_x_test,
        n_bins=n_bins, 
        disc_type=disc_type,
        batch_size=batch_size)

        
    # load similarity matrix
    X, X_labels, target, lookup = load_sim_matrix(
        matrix_file=matrix_file, desc_type=desc_type)
    
    # target is a list of tuples 
    # converter works for either float or lists
    # convert target (always in Joule if energy) in energy_unit
    # TO BE CHANGED
    try:
        target = [convert_energy_substance('J', list(item), 
            ureg=ureg, energy_unit=energy_unit, 
            length_unit=length_unit) for item in target]   
    except:
        pass
    
    
    # calculate the embedding
    sim_matrix_to_embedding(
        matrix_file=matrix_file,
        class_count=class_count,
        method=embed_method,
        embed_params=embed_params,
        desc_type=desc_type,
        lookup_file=lookup_file,
        standardize=standardize,
        target=target)

    logger.info("Two-dimensional embedding: done.")


def _apply_operations(structure, operations_on_structure=None):
    """Apply operations to a NOMADStructure class instance."""
    
    structure_op_list = []
    

    if operations_on_structure is not None:
        if isinstance(operations_on_structure, list)==False:
            operations_on_structure = [operations_on_structure]
  
        for idx, operation in enumerate(operations_on_structure):   
            # use deep coy to create a new instance otherwise we always get 
            # only the original structure in the list
            structure_to_modify = copy.deepcopy(structure)
            structure_op_list.append(copy.deepcopy(modify_crystal(structure_to_modify, operation[0], **operation[1])))

    else:
        structure_op_list = [structure]

    return structure_op_list

def _get_structures(
        json_list=None,
        frame_list=None,
        file_format=None,
        png_path=None,
        geo_path=None,
        desc_path=None,
        tmp_folder=None,
        cell_type=None,
        operations_on_structure=None,
        op_list=None,
        descriptor=None,
        atoms_scaling=None):
    """ Obtain the NOMADstructure from each json file in the json_list;
    write geometry files and figures to disk that will be later used to
    plot the results in the Viewer.

    Parameters
    ----------

    json_list : list
        List with the absolute paths to the json files

    frame_list : list or list of list, optional
        For each json file, specifies with frames to consider in the descriptor
        calculation. If multiple json files are present, it is a list of lists.

    file_format : text, optional, default 'NOMAD'
        Specify what is the format of the file to read.
        Possible values: 'NOMAD' and 'rdf' (rdf is deprecated).

    png_path : text, optional, default tmp_folder
        Path to the folder where the png file for each structure are written.

    geo_path : text, optional, default tmp_folder
        Path to the folder where the png file for each structure are written.

    tmp_folder : text
        Temporary folder.

    descriptor: object Descriptor


    Returns
    ----------

    structure_list: list, shape (len(json_list), )
        List of NOMADstuctures. Each element in the list is an instance of the
        class NOMADstructure; such instance can contain multiple frames
        (see nomad_structures.py for more detail on the class NOMADstructure).

    """

    structure_list = []

    if tmp_folder is None:
        tmp_folder = '/tmp'

    if geo_path is None:
        geo_path = tmp_folder

    if png_path is None:
        png_path = tmp_folder
        
    if desc_path is None:
        desc_path = tmp_folder

    logger.info("Generating figures and geometry files.")

    # get unique list of json files 
    # this is important when operations are applied to the json files
    # this does not conserve the order
    #json_list = list(set(json_list))
    # NOTE: we should do something more robust
    new_json_list = []
    for item in json_list:
        if item not in new_json_list:
            new_json_list.append(item)
            
    json_list = new_json_list
    
    for idx, json_file in enumerate(json_list):
        # read structure
        if file_format == 'rdf' or file_format == 'NOMAD':
            try:
                # frame_list is a list of lists -> extract the list referring
                # to each JSON file
                frame_list_json = frame_list[idx]
            except:
                # frame_list is a list -> the frame_list_json is the list
                # itself
                frame_list_json = frame_list

            # make the first "pristine" structure
            structure = NOMADStructure(
                in_file=json_file,
                frame_list=frame_list_json,
                file_format=file_format,
                cell_type=cell_type,
                descriptor=descriptor)
                
            structure_op = _apply_operations(structure, operations_on_structure)
                                   
            for op_id, structure in enumerate(structure_op):
                # write coordinates of structure
                structure.write_geometry(path=geo_path, operation_number=op_list[op_id])
                #structure.write_png(path=png_path, replicas=(3, 3, 3), 
                structure.write_png(path=png_path, replicas=(1, 1, 1), 
                    rotation=True, operation_number=op_list[op_id])
                
                # calculate descriptor
                if descriptor is not None:
                    descriptor.write(
                        structure, path=desc_path, replicas=(1, 1, 1), 
                        grayscale=True,
                        operation_number=op_id,
                        atoms_scaling=atoms_scaling)
            
            
        else:
            raise Exception("Please specify a valid file format. Possible file formats are 'NOMAD' or 'rdf'.")



        structure_list.append(structure_op)

        if idx % (int(len(json_list) / 10) + 1) == 0:
            logger.info(
                "Generating figures and geometries: file {0}/{1}".format(idx + 1, len(json_list)))

    logger.info("Generating figures and geometry files: done.")

    return structure_list


def plot(
        json_list=None,
        frames=None,
        frame_list=None,
        file_format=None,
        png_path=None,
        geo_path=None,
        desc_folder=None,
        tmp_folder=None,
        control_file=None,
        clustering_x_list=None,
        clustering_y_list=None,
        target_list=None,
        target_pred_list=None,
        target_class_names=None,
        target_unit=None,
        energy_unit=None,
        xray_img_list=None,
        is_classification=None,
        write_png=True,
        write_geometry=True,
        legend_title=None,
        plot_title=None,
        target_name=None,
        clustering_point_size=None,
        name=None,
        html_folder=None,
        cell_type=None,
        operations_on_structure=None,
        op_list=None,
        descriptor=None,
        atoms_scaling=None):
    """ Generate the Viewer.

    Parameters
    ----------

    json_list : list
        List with the absolute paths to the json files

    frames : string, optional, {'all', 'first', 'last', 'list'}
        Define which frames should be shown from the NOMADstructure.\n
        'all': show all the frames in the NOMADstucture \n
        'first': show only 1st frame \n
        'last': show only last frame \n
        'list': show user-specified list of frames. The list of frames
        needs to be specified in `frame_list`.

    frame_list : list or list of lists, optional
        For each json file, specifies with frames to consider in the descriptor
        calculation. If multiple json files are present, it is a list of lists.
        Supports negative indeces.

    file_format : text, optional, {'NOMAD', 'rdf'}
        Specify what is the format of the file to read.
        ('rdf' is DEPRECATED).

    png_path : string, default `tmp_folder`
        Path to the folder where the png file for each structure are written.

    geo_path: string, default `tmp_folder`
        Path to the folder where the png file for each structure are written.

    desc_folder : string, default `tmp_folder`
        Folder where the `desc_file` is written.

    tmp_folder: string
        Temporary folder.

    clustering_x_list : list or list of lists of floats
        x coordinate for the 2d-plot in the Viewer. Usually read from a 'lookup.dat'
        file written by the `calc_embedding` function (see the `calc_embedding` function
        for more details).
        If multiple json files are present, it is a list of lists.

    clustering_y_list : list or list of lists of floats
        y coordinate for the 2d-plot in the Viewer. Usually read from a 'lookup.dat'
        file written by the `calc_embedding` function (see the `calc_embedding` function
        for more details).
        If multiple json files are present, it is a list of lists.

    target_list : list or list of list of floats
        Used for the colors of the poins in the 2d-plot of the Viewer.
        Usually read from a 'lookup.dat' file written by the calc_embedding function
        (see the `calc_embedding` function for more details).
        If multiple json files are present, it is a list of lists.

        .. todo:: check if it actually supports multiple files and multiple frames

    clustering_point_size : float, optional, default 12
        Size of the points in the 2d-embedding plot.

    name : string, optional, default 'viewer'
        Name of the html page (without extension) generated.
        Such html page (name.html) is the NOMAD Viewer.

    html_folder : string, optional, default `tmp_folder`
        Folder where the html page name.html is saved.

    Returns
    ----------

    file_html_link : string
        html string that in the Beaker notebook generates the html link to the
        viewer (name.html). For example,
        <a target=_blank href='/path/to/file/viewer.html'>Click here to open the Viewer</a>

        .. todo:: Add Sha support.


    """


    # get structures, generate png, geometry files and optionally descriptor
    # files
    structure_list = _get_structures(
        json_list=json_list,
        frame_list=frame_list,
        file_format=file_format,
        png_path=png_path,
        geo_path=geo_path,
        tmp_folder=tmp_folder,
        cell_type=cell_type,
        operations_on_structure=operations_on_structure,
        op_list=op_list,
        descriptor=descriptor,
        atoms_scaling=atoms_scaling)
    
    # flatten the list
    # it is a list of lists when operations_on_structure is not None
    if operations_on_structure is not None:
        structure_list = [item for sublist in structure_list for item in sublist]
        #take only the 1st frame of frame list and make a list of single item
        # NOTE: this works only for single frames but it is okay for us
        #frame_list = [[item[0]] for item in frame_list]

    logger.debug("Plotting {0} crystal structures.".format(len(structure_list)))
    
    
    # create an instance of class Viewer
    viewer = Viewer(name=name)

    # check if there is a control file
    try:
        with open(control_file) as data_file:
            data = json.load(data_file)

            for c in data['model_info']:
                x_axis_label = c["x_axis_label"]
                y_axis_label = c["y_axis_label"]
    except:
        x_axis_label = None
        y_axis_label = None

    # make the plot
    file_html_link, file_html_name = viewer.plot(archive=structure_list, frames=frames,
                                                 frame_list=frame_list, clustering_x_list=clustering_x_list, clustering_y_list=clustering_y_list,
                                                 target_list=target_list, target_pred_list=target_pred_list,
                                                 target_class_names=target_class_names,
                                                 target_unit=target_unit, target_name=target_name,
                                                 energy_unit=energy_unit,
                                                 descriptor=descriptor, xray_img_list=xray_img_list,
                                                 legend_title=legend_title, is_classification=is_classification,
                                                 x_axis_label=x_axis_label, y_axis_label=y_axis_label, plot_title=plot_title,
                                                 clustering_point_size=clustering_point_size, html_folder=html_folder, tmp_folder=tmp_folder)

    # open the browser and show the plot
    view(file_html_name)

    return file_html_link



'''
def module_level_function(param1, param2=None, *args, **kwargs):
    """This is an example of a module level function.

    Function parameters should be documented in the ``Parameters`` section.
    The name of each parameter is required. The type and description of each
    parameter is optional, but should be included if not obvious.

    If \*args or \*\*kwargs are accepted,
    they should be listed as ``*args`` and ``**kwargs``.

    The format for a parameter is::

        name : type
            description

            The description may span multiple lines. Following lines
            should be indented to match the first line of the description.
            The ": type" is optional.

            Multiple paragraphs are supported in parameter
            descriptions.

    Parameters
    ----------
    param1 : int
        The first parameter.
    param2 : :obj:`str`, optional
        The second parameter.
    *args
        Variable length argument list.
    **kwargs
        Arbitrary keyword arguments.

    Returns
    -------
    bool
        True if successful, False otherwise.

        The return type is not optional. The ``Returns`` section may span
        multiple lines and paragraphs. Following lines should be indented to
        match the first line of the description.

        The ``Returns`` section supports any reStructuredText formatting,
        including literal blocks::

            {
                'param1': param1,
                'param2': param2
            }

    Raises
    ------
    AttributeError
        The ``Raises`` section is a list of all exceptions
        that are relevant to the interface.
    ValueError
        If `param2` is equal to `param1`.

    """
    if param1 == param2:
        raise ValueError('param1 may not be equal to param2')
    return True
'''

# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
