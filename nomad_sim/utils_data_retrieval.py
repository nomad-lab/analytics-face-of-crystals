#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import absolute_import

__author__ = "Angelo Ziletti"
__copyright__ = "Copyright 2016, The NOMAD Project"
__maintainer__ = "Angelo Ziletti"
__email__ = "ziletti@fhi-berlin.mpg.de"
__date__ = "09/08/17"


import logging, os, sys
import numpy as np
import tarfile
import pandas as pd
from collections import defaultdict
import json
from PIL import Image  
import PIL.ImageOps    
import math  
from sklearn import preprocessing 
from scipy import misc

from nomad_sim.config import configs

try:
    hdlr = logging.FileHandler(configs["output_file"], mode='a')
except:
    hdlr = logging.FileHandler(configs["output_file"], mode='w')

level = logging.getLevelName(configs["log_level_general"])

logger = logging.getLogger(__name__)
logger.setLevel(level)
logging.basicConfig(level=level)
FORMAT = "%(levelname)s: %(message)s"
formatter = logging.Formatter(fmt=FORMAT)
handler = logging.StreamHandler()
handler.setFormatter(formatter)
hdlr.setFormatter(formatter)
logger.addHandler(handler)
logger.addHandler(hdlr)
logger.setLevel(level)
logger.propagate = False

__metainfopath__ = configs["meta_info_file"]

#-------------------------------------------------------------------------

def generate_facets_input(desc_folder=None, input_dims=None, desc_file_list=None, 
    tmp_folder=None, main_folder=None, df_filename="df_facets.csv"):
    
    if isinstance(desc_file_list, list)==False:
        desc_file_list = [desc_file_list]
    
    logger.debug('Descriptor file_list: {0}'.format(desc_file_list))
    
    desc_file_list = [os.path.abspath(os.path.normpath(os.path.join(desc_folder, desc_file))) for desc_file in desc_file_list]
    
    target_list = []        
    images_list = []

    
    for desc_file in desc_file_list:
        # if the final array shape is not know, it is faster to append to a list and 
        # covert to array than append to numpy array 
        target = extract_target_files(desc_file, desc_folder=desc_folder, tmp_folder=tmp_folder)   
        images = extract_images(filename=desc_file, 
            input_dims=input_dims, desc_folder=desc_folder, tmp_folder=tmp_folder)
        target_list.append(target)     
        images_list.append(images)
    
    # flatten the list
    target_list = [item for sublist in target_list for item in sublist]
    images_filelist = [item for sublist in images_list for item in sublist]
    
    images = np.asarray(images_filelist)
    images = np.reshape(images, (-1, input_dims[0], input_dims[1]))

    create_sprite_atlas(images=images, main_folder=main_folder, sprite_atlas_filename="sprite_atlas")
    
    target_list_dict = []
    for idx, item in enumerate(target_list):
#        print item["data"][0]["main_json_file_name"], images_filelist[idx].split("/")[-1]
        # the 0 refers to the first frame
        target_list_dict.append(item["data"][0])

    df = pd.DataFrame(target_list_dict)

    # filter columns
#    features = [u'Bravais_lattice_cs', u'Bravais_lattice_lt', u'chemical_formula', 
#        u'crystal_system', u'energy_total__eV', u'hall_symbol', u'lattice_centering', 
#        u'lattice_type', u'spacegroup_symbol', u'spacegroup_number'] 

    features = [u'Bravais_lattice_lt', u'chemical_formula', u'spacegroup_symbol', 'main_json_file_name']  
        
    df = df[features]
    
    df_filepath = os.path.abspath(os.path.normpath(os.path.join(main_folder, df_filename)))    
    df.to_csv(df_filepath, index=False)
   
    return df_filepath

def extract_images(filename, filetype='descriptor_files', input_dims=None, desc_folder=None, tmp_folder=None):
    """ Read tar.gz file and extract npy array with the images from 
    the descriptor_files specified in summary.json. """
    
    logger.debug('Loading images from descriptor file {0}'.format(filename))

    archive = tarfile.open(filename, 'r')
    archive.extractall(tmp_folder)

    files_by_category = read_archive_desc(filename, summary_filename="summary.json")
    img_files = files_by_category[str(filetype)]      
    img_files = [os.path.abspath(os.path.normpath(os.path.join(tmp_folder, img_file))) for img_file in img_files]

    logger.info("Number of images to be read: {0}".format(len(img_files)))    
    img_list = []
    for idx, img_file in enumerate(img_files):
        if idx % (int(len(img_files) / 10) + 1) == 0:
            logger.debug("Reading image: file {0}/{1}".format(idx + 1, len(img_files)))
            img_filename, img_file_extension = os.path.splitext(img_file)

        if img_file_extension == '.npy':
            # read numpy array    
            img = np.load(img_file)
        elif img_file_extension == '.png':
            # read png file
            img_large = misc.imread(img_file, mode='L')
            img = misc.imresize(img_large, (input_dims[0], input_dims[1]))
        else:
            raise TypeError("Extension {0} is not supported".format(img_file_extension))

        img_list.append(img)    

    # convert to numpy array
    # if the final array shape is not know, it is faster to append to a list and 
    # covert to array than append to numpy array 
    data = np.asarray(img_list)
    

    if len(input_dims) == 2:
        data = data.reshape(len(img_list), input_dims[0], input_dims[1], 1)
    elif len(input_dims) == 3:
        data = data.reshape(len(img_list), input_dims[0], input_dims[1], input_dims[2], 1)
    else:
        raise Exception("Wrong number of dimensions.")            
    
    archive.close()

    # clean tmp folder
    clean_folder(tmp_folder)
        
    return data
    
def extract_labels(filename_list, target_name, target_categorical, disc_type='uniform', n_bins=100,
    desc_folder=None, tmp_folder=None):
    """Extract the labels into a 1D uint8 np array [index].
    Assumes that all the classes are contained in the training set.
    The test set can have less classes."""

    target_list = []

    for filename in filename_list:
        logger.debug('Loading labels from file {0}'.format(filename))
        # if the final array shape is not know, it is faster to append to a list and 
        # covert to array than append to numpy array 
        target = extract_target_files(filename=filename, desc_folder=desc_folder, tmp_folder=tmp_folder, clean_tmp=False)   
        target_list.append(target)     
    
    # flatten the list
    target_list = [item for sublist in target_list for item in sublist]
    
    target_list_dict = []
    for idx, item in enumerate(target_list):
#        print item["data"][0]["main_json_file_name"], images_filelist[idx].split("/")[-1]
        # the 0 refers to the first frame
        target_list_dict.append(item["data"][0])

    df = pd.DataFrame(target_list_dict)
    
    # from the panda dataframe extract the cols corresponding to the target
    target = df[target_name]
    
    label_encoder = None
    
    if target_categorical:
        class_labels = np.asarray(target)
    
    if not target_categorical:        
        logger.info('Converting numerical target to categorical.')
        max_value = np.amax(target)
        min_value = np.amin(target)
        logger.info("Target range: [{0}, {1}]".format(min_value, max_value))
        logger.debug("Number of bins: {0}".format(n_bins))
        logger.debug("Discretization type: {0}".format(disc_type))

        if disc_type=='uniform':
            bins = np.linspace(min_value, max_value, n_bins)
            dx = (abs(min_value - max_value))/n_bins
            logger.debug("Discretization bin size: {0}".format(dx))


        if disc_type=='quantiles':
            bins = list((pd.qcut(target, n_bins, labels=False, retbins=True))[1])
            # Reproduce `histogram` binning by manually shifting the 
            # rightmost bin edge by an epsilon value:
            # https://github.com/numpy/numpy/issues/4217
            # not needed for uniform because there the bins are 10, not 11 like here
            bins[-1] += 10E-8
        
        class_labels = np.digitize(target, bins, right=False)
                
        # calculate just to show (note: bins are re-defined here)
        freq, bins = np.histogram(target, bins=bins)

        logger.debug("Class distribution:")
        
        for idx in range(len(bins)-1):
            logger.debug("{0}<= x<{1}  count:{2}".format(bins[idx], bins[idx+1], freq[idx]))
     
    # from class_labels to class number
    label_encoder = preprocessing.LabelEncoder()
    label_encoder.fit(class_labels)
    labels = label_encoder.transform(class_labels)

    logger.debug("Number of unique classes in the dataset: {0}".format(len(label_encoder.classes_)))
    logger.debug("Actual class labels: \n {0}".format(list(label_encoder.classes_)))        

    return label_encoder, labels, class_labels
    
def extract_img_list(filename, desc_folder=None, tmp_folder=None):
    """ Return the list of images from ."""
    
    logger.debug('Loading file {0}'.format(filename))

    files_by_category = read_archive_desc(filename, summary_filename="summary.json")
    desc_folder = desc_folder + tmp_folder
    
    img_list = []
    for file_ in files_by_category['2d_diffraction_images_ks']:
        img = os.path.abspath(os.path.normpath(os.path.join(desc_folder, file_)))    
        img_list.append(img)
    
    return img_list


def extract_target_files(filename, desc_folder=None, tmp_folder=None, clean_tmp=False):
    """ Read target files from descriptor folder.
    Change to put the extractall inside."""
    
    logger.debug('Loading file {0}'.format(filename))

    if clean_tmp:
        clean_folder(tmp_folder)

    archive = tarfile.open(filename, 'r')
    archive.extractall(tmp_folder)
    
    data_list = []
    files_by_category = read_archive_desc(filename, summary_filename="summary.json")
    
    for item in files_by_category['target_files']:  
        in_file = os.path.abspath(os.path.normpath(os.path.join(tmp_folder, item))) 
        with open(in_file) as json_file:
            try:
                data = json.load(json_file)
                data_list.append(data)   
            except Exception as e:
                logger.error("Could not read content from JSON file {0}".format(in_file))
                logger.error("Error: {0}".format(e))
            finally:
                json_file.close()
            
    archive.close()    

    return data_list
    
    
def create_sprite_atlas(images, main_folder, sprite_atlas_filename="sprite_atlas", max_imgs_row=None):
    """Create a sprite atlas for numpy array of images(nb_images, px, py)
    Based on: 
    https://minzkraut.com/2016/11/23/making-a-simple-spritesheet-generator-in-python/"""
    sprite_atlas_filename_path = os.path.abspath(os.path.normpath(os.path.join(main_folder, sprite_atlas_filename)))    
    
    if max_imgs_row is None:
        max_imgs_row = int(math.sqrt(images.shape[0])) 

    nb_imgs, tile_width, tile_height = images.shape  

    if nb_imgs > max_imgs_row :  
        spritesheet_width = tile_width * max_imgs_row
        required_rows = math.ceil(nb_imgs/max_imgs_row)
        imgs_last_row = nb_imgs % max_imgs_row 
        if imgs_last_row != 0:
            required_rows = required_rows + 1
        spritesheet_height = tile_height * required_rows
    else:  
        spritesheet_width = tile_width*nb_imgs
        spritesheet_height = tile_height
    
    spritesheet = Image.new("RGBA",(int(spritesheet_width), int(spritesheet_height)))
    
    img_list = []
    for idx in range(nb_imgs):
        img = images[idx, :, :]
        img_list.append(img)

    for idx, current_img in enumerate(img_list):  
        top = tile_height * math.floor(idx/max_imgs_row)
        left = tile_width * (idx % max_imgs_row)
        bottom = top + tile_height
        right = left + tile_width
    
        box = (left,top,right,bottom)
        box = [int(i) for i in box]
        
        current_frame_img = PIL.Image.fromarray(current_img)      
        spritesheet.paste(current_frame_img, box)
    
    sprite_atlas_filename_path = sprite_atlas_filename_path + ".png"
    spritesheet.save(sprite_atlas_filename_path, "PNG")  
    logger.info("Image creation completed.")
    logger.debug("Saving at {0}.".format(sprite_atlas_filename_path))
    
    return sprite_atlas_filename_path

def clean_folder(tmp_folder, allowed_endings=[".log", ".tar.gz", ".html"]):
    """ Clean tmp folder from files that do not end in allowed_endings"""
    
    flt_files = filter(lambda x: not x.endswith(tuple(allowed_endings)), os.listdir(tmp_folder))
    
    for flt_file in flt_files:
        file_path = os.path.join(tmp_folder, flt_file)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
        except Exception as e:
            logger.warning(e)
            
def read_archive_desc(filename, summary_filename="summary.json"):    
    """ Read descriptor file archive and return a dictionary with
    (key=filetype, value=list_of_files_for_that_type.
    See write_summary_file for allowed_endings and filetypes."""
    
    logger.debug('Loading file {0}'.format(filename))
    archive = tarfile.open(filename, 'r')

    # extract the Archive in tmp folder
    for member in archive.getmembers()[:]:  
        member.name = os.path.basename(member.name)
        if os.path.basename(member.name) == summary_filename:                    
            files_by_category = json.load(archive.extractfile(member))
            
    return files_by_category['data'][0]
    

def write_summary_file(filename, tmp_folder, allowed_endings=None, 
    summary_filename="summary.json", clean_tmp=False):    
    """Write a summary of the files present in the descriptor file
    and save with basename only (no full path)"""

    allowed_endings = {'descriptor_files' : '.npy',
        '2d_diffraction_images_rs' : '_xray_rs.png',
        '2d_diffraction_images_ks' : '_xray.png',
        'target_files' : '_target.json',
        'info_files' : '.info',
        'geometry_files' : '_aims.in',
        'geometry_thumbnails' : '_geometry_thumbnail.png'}

    logger.debug('Loading file {0}:'.format(filename))
    if clean_tmp:
        clean_folder(tmp_folder)

    archive = tarfile.open(filename, 'r')

    members = []    
    for member in archive.getmembers()[:]:  
        member.name = os.path.basename(member.name)
        archive.extract(member, tmp_folder)
        members.append(member.name)
            
    filelist_dict = {}    
    for member in archive.getmembers()[:]:  
        if not member.isfile():
            continue
        # make a dictionary {"filename", "filetype"}        
        for (key, value) in allowed_endings.iteritems():        
            if member.name.endswith(value):
                filelist_dict[member.name] = key

    files_by_cat = defaultdict(list)
    for key, value in sorted(filelist_dict.iteritems()):
        files_by_cat[value].append(key)    
    archive.close()    

    # re-open the Archive to add the summary.json file
    logger.debug('Loading file {0}:'.format(filename))
    new_archive = tarfile.open(filename, 'w:gz')

    member_filename_paths = []   
    for member in members:  
        member_filename_path = os.path.abspath(os.path.normpath(os.path.join(tmp_folder, member)))
        member_filename_paths.append(member_filename_path)
            
    summary_filename_path = os.path.abspath(os.path.normpath(os.path.join(tmp_folder, summary_filename)))
    member_filename_paths.append(summary_filename_path)

    # get unique list on member_filename_paths to avoid to add multiple times
    # the same file (e.g. sumamry.json will not be added twice if already present)
    member_filename_paths = sorted(list(set(member_filename_paths)))

    with open(summary_filename_path, "w") as f:
            f.write("""
    {
          "data":[""")
    
            writeColon = False

            if (writeColon):
                f.write(", ")
            writeColon = True
            json.dump(files_by_cat, f, indent=2)
            f.write("""
    ] }""")
            f.flush()

    for member_filename_path in member_filename_paths:
        # add arcname to avoid to include the whole folder path in the member.name
        new_archive.add(member_filename_path, arcname=os.path.basename(member_filename_path))

    logger.debug('Summary file written in {0}:'.format(filename))


    new_archive.close()
    

# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
