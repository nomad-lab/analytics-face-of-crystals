# -*- coding: utf-8 -*-
#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import absolute_import

__author__ = "Angelo Ziletti"
__copyright__ = "Copyright 2016, The NOMAD Project"
__maintainer__ = "Angelo Ziletti"
__email__ = "ziletti@fhi-berlin.mpg.de"
__date__ = "21/10/16"


import os, logging, sys, stat
import json
import tarfile
import pandas as pd
import numpy as np
import condor
import random
import matplotlib.pyplot as pypl
import math
from pyquaternion import Quaternion
from scipy import constants

base_dir = os.path.dirname(os.path.abspath(__file__))
common_dir = os.path.normpath(os.path.join(base_dir,"../../../../python-common/common/python"))
nomad_sim_dir = os.path.normpath(os.path.join(base_dir,"../../python-modules/"))
atomic_data_dir = os.path.normpath(os.path.join(base_dir, '../../../atomic-data')) 

if not common_dir in sys.path:
    sys.path.insert(0, common_dir) 
    sys.path.insert(0, nomad_sim_dir) 
    sys.path.insert(0, atomic_data_dir) 
    
import atomic_data
from atomic_data.common import *
from atomic_data.collections import *

import string
from sklearn.metrics.pairwise import cosine_similarity
from pymatgen.io.ase import AseAtomsAdaptor
from nomad_sim.nomad_structures import NOMADStructure
from nomad_sim.viewer import Viewer
from nomad_sim.gen_similarity_matrix import sim_matrix_to_embedding, load_sim_matrix
from nomad_sim.l1_l0 import choose_atomic_features, write_atomic_features, classify_rs_zb
from nomad_sim.l1_l0 import get_energy_diff, combine_features, l1_l0_minimization
from bokeh.util.browser import view
from nomad_sim.cnn_preprocessing import read_data_sets
from nomad_sim.keras_autoencoder import run_keras_autoencoder
from PIL import Image, ImageOps
from nomad_sim.utils_crystals import get_xrd_diffraction
from nomad_sim.utils_crystals import get_min_distance
from nomad_sim.utils_crystals import get_avg_nn_distance
from nomad_sim.utils_crystals import scale_structure
from pymatgen.analysis.diffraction.xrd import XRDCalculator
from mendeleev import element
from nomad_sim.utils_crystals import convert_energy_substance
from nomad_sim.utils_binaries import get_chemical_formula_binaries


    

import warnings 
from pint import UnitRegistry
# disable DeprecationWarning for scikit learn
warnings.filterwarnings('ignore', category=DeprecationWarning)
# disable warnings from pint
logging.getLogger("pint").setLevel(logging.ERROR)

import nomadcore.unit_conversion.unit_conversion as uc
import nomadcore.parser_backend as parser



from nomad_sim.config import configs

try:
    hdlr = logging.FileHandler(configs["output_file"], mode='a')
except:
    hdlr = logging.FileHandler(configs["output_file"], mode='w')

ureg = UnitRegistry(os.path.normpath(configs["ureg_file"]))
level = logging.getLevelName(configs["log_level_general"])

logger = logging.getLogger(__name__)
logger.setLevel(level)
logging.basicConfig(level=level)
FORMAT = "%(levelname)s: %(message)s"
formatter = logging.Formatter(fmt=FORMAT)
handler = logging.StreamHandler()
handler.setFormatter(formatter)
hdlr.setFormatter(formatter)
logger.addHandler(handler)
logger.addHandler(hdlr)
logger.setLevel(level)
logger.propagate = False

__metainfopath__ = configs["meta_info_file"]



class Descriptor(object):
    def __init__(self, **params):
        #super().__init__(**kwargs)
        self.name = self.__class__.__name__
        pass

    def __str__(self):
        return self.name + '(' + str(self.__dict__) + ')'

    def calculate(self, structure):
        return {"name": self.name}
    
    def write(self, file_out):
        pass
    
    def read(self, file_in):
        pass
    
    def write_desc_info(self, desc_info_file, json_list, 
        operations_on_structure=None, filename_img_list=None):
        with open(desc_info_file, "w") as f:
            f.write("""
    {
          "descriptor_info":[""")
    
            writeColon = False
    
            desc_info = {
                "descriptor": str(self),
                "json_list": json_list,
            }
    
            # add descriptor-specific information
            if self.name=='AtomicFeatures':
                desc_info.update({
                "selected_feature_list" : self.selected_feature_list
                })
            
            if self.name=='XrayDiffraction':
                desc_info.update({
                "xray_img_list": filename_img_list,
                "operations_on_structure": str(operations_on_structure)
                })
             
             
            if (writeColon):
                f.write(", ")
            writeColon = True
            json.dump(desc_info, f, indent=2)
            f.write("""
    ] }""")
            f.flush()
            
    
    def params(self):
        return self.__dict__
    params = staticmethod(params)
    

class XrayDiffraction(Descriptor):
    def __init__(self, ndim=None, desc_space=None, param_source=None, param_detector=None, 
        user_param_source=None, user_param_detector=None, rotation=False, 
        angles_d=None, 
        use_autoencoder=False,
        scale_atoms=True,
        **params):
        super(XrayDiffraction, self).__init__()

        params = Descriptor.params(self)
        
        if param_source is None:
            param_source = {  
                'wavelength': 1.0E-10, 
                'pulse_energy': 1E-3, 
                'focus_diameter': 1E-6 
            }
            
        if param_detector is None:
            param_detector = {  
                'distance': 0.10,
                'pixel_size': 2.7E-4,
                'nx': 52,
                'ny': 52
            }
        
        if user_param_source is not None:
            param_source.update(user_param_source)

        if user_param_detector is not None:
            param_detector.update(user_param_detector)

        if not rotation:
            angles_d = [0.0]
            
        self.param_source = param_source
        self.param_detector = param_detector
        self.rotation = rotation
        self.angles_d = angles_d
        self.use_autoencoder = use_autoencoder
        self.ndim = ndim
        self.desc_space = desc_space

        params.update({
            "param_source" : param_source,
            "param_detector" : param_detector,
            "rotation": rotation,
            "angles_d": angles_d
        })

        if self.ndim == 2 or self.ndim == 3:
            pass
        else:
            raise ValueError("The specified descriptor dimension (ndim= {0}) is not valid.".format(self.ndim))

        if self.desc_space == 'k-space' or self.desc_space == 'r-space':
            pass
        else:
            raise ValueError("The specified descriptor space ({0}) is not valid.".format(self.desc_space))
                        
    
    def read(self):
        pass
        
    def write(self, structure, path=None, filename_suffix='_xray.png', 
        replicas=None, 
        grayscale=True,
        atoms_scaling=None,
        use_mask=False,
        operation_number=0):
        """Write xray images for the structures read from the JSON files. 
        Unless otherwise specified, it builds a 4x4x4 supercell
        if the structure is periodic.

        Parameters
        ----------
 
        structure : class, NOMADstructure
            Instance of the class NOMADstructure.

        path : string, optional, default `xray_path`
            Path to the folder where the xray files are written 
            

        filename_suffix : string, default '_xray.png'
            Suffix added after filename

        replicas: list of 3 integers (DEPRECATED)
            Number of replicas in each direction. Used only if `isPeriodic` is `True`.

        Returns
        -------
        
        filename_list : string or list
            Xray file (or list of xray files) generated from the file. It is a list if multiple 
            frames are present.
        
        """
        
        filename_list_img = []
        filename_list_npy = []
        filename_list_rs = []
        filename_list_geo = []
        
        # define the default path as xray_path
        if path is None:
            path = structure.xray_path

        if (structure.file_format=='NOMAD') or (structure.file_format=='xyz'):
            for (gIndexRun, gIndexDesc), atoms in structure.atoms.items():
                if atoms is not None:
                    
                    #if it is periodic, replicate according to the vector replicas
                    if structure.isPeriodic == True:
                        # set the default to 4 replicas in each direction
                        if replicas == None:
                            replicas = (1,1,1)
                    else:
                        replicas = (1,1,1) 
                    
                    atoms = atoms * replicas                                          

                    if atoms_scaling is not None:
                        if atoms_scaling == 'min_distance_nn':
                            scale_factor = get_min_distance(atoms)
                            #*np.ones(3)
                            #print 'scale_factor', scale_factor
                            #scale_factor = get_min_distance(atoms)*np.ones(3)
                            #print 'scale_factor_2', scale_factor                       
                            #atoms.set_cell(np.dot(atoms.get_cell(), scale_factor))
                            #atoms.set_cell()
                        elif atoms_scaling == 'avg_distance_nn':
                            scale_factor = get_avg_nn_distance(atoms)
                            #print 'scale_factor', scale_factor
                            #atoms = scale_structure(atoms, scale_factor=(1/scale_factor))
                            pass

                        atoms = scale_structure(atoms, scale_factor=(1/scale_factor))
                    
                    # Source
                    src = condor.Source(**self.param_source)

                    # Detector
                    if self.ndim == 2:
                        det = condor.Detector(**self.param_detector)
                    else:
                        # solid_angle_correction are meaningless for 3d diffraction
                        det = condor.Detector(solid_angle_correction=False, **self.param_detector)                        
                    
                    # Atoms                    
                    atomic_numbers = map(lambda x:x.number, atoms)
                    # add 2 (arbitrary number) to the atomic number such that
                    # H because visible in x-ray diffraction pattern
                    atomic_numbers = [atomic_number + 2 for atomic_number in atomic_numbers]

                    # convert Angstrom to m (CONDOR uses meters)
                    atomic_positions = map(lambda x: [x.x*1E-10,x.y*1E-10,x.z*1E-10], atoms)                        

                    for angle_d in self.angles_d:
                        # filename is the normalized absolute path 
                        filename_xray = os.path.abspath(os.path.normpath(os.path.join(path, '%s_%d_%d_%d_op%d%s' % (structure.name, gIndexRun, gIndexDesc, angle_d, operation_number, filename_suffix))))
                        filename_rs = os.path.abspath(os.path.normpath(os.path.join(path, '%s_%d_%d_%d_op%d%s' % (structure.name, gIndexRun, gIndexDesc, angle_d, operation_number, '_xray_rs.png'))))
                        filename_ph = os.path.abspath(os.path.normpath(os.path.join(path, '%s_%d_%d_%d_op%d%s' % (structure.name, gIndexRun, gIndexDesc, angle_d, operation_number, '_xray_ph.png'))))
                        filename_npy = os.path.abspath(os.path.normpath(os.path.join(path, '%s_%d_%d_%d_op%d%s' % (structure.name, gIndexRun, gIndexDesc, angle_d, operation_number, '.npy'))))
                        angle = np.radians(angle_d)

                        # other possible rotations
#                        angle = angle_d/360.*2*np.pi
#                        rotation_axis = np.array([0., 1., 0.])/np.sqrt(1.)
#                        rotation_axis = np.array([1.,1.,1.])/np.sqrt(3.)
#                        rotation_formalism = 'rotation_matrix'
#                        rotation_axis = np.array([1.,1.,0.])/np.sqrt(2.)
                        
                        #  around x axis                        
                        rot_matrix = np.asarray([[1, 0, 0], [0, math.cos(angle), -math.sin(angle)], [0, math.sin(angle), math.cos(angle)]])

                        # around y axis                                                
#                        rot_matrix = np.asarray([[math.cos(angle), 0, math.sin(angle)], [0, 1, 0], [-math.sin(angle), 0, math.cos(angle)]])
                            
                        # around z axis
#                        rot_matrix = np.asarray([[math.cos(angle), -math.sin(angle), 0], [math.sin(angle), math.cos(angle), 0], [0, 0, 1]])

                        quaternion = condor.utils.rotation.quat_from_rotmx(rot_matrix)
                        rotation_values = np.array([quaternion])
                        rotation_formalism = "quaternion"
                        rotation_mode = "extrinsic"


#                        # make random rotation
#                        rnd_quaternion = Quaternion.random()
#                        rot_matrix = rnd_quaternion.rotation_matrix
#                        logger.info("Random rotation matrix: {0}".format(rot_matrix))
#                        quaternion = condor.utils.rotation.quat_from_rotmx(rot_matrix)
#                        logger.info("Random quaternion from Condor: {0}".format(quaternion))

        
        
                        par = condor.ParticleAtoms(atomic_numbers=atomic_numbers, 
                            atomic_positions=atomic_positions,
                            rotation_values=rotation_values, rotation_formalism=rotation_formalism, 
                            rotation_mode=rotation_mode)
        
                           
                        s = "particle_atoms"
                        E = condor.Experiment(src, {s : par}, det)
                        
                        if self.ndim == 2:
                            res = E.propagate()
                        else:
                            res = E.propagate3d()
  
                        
                        real_space = np.fft.fftshift(np.fft.ifftn(np.fft.fftshift(res["entry_1"]["data_1"]["data_fourier"])))
                        intensity_pattern = res["entry_1"]["data_1"]["data"]

                        fourier_space = res["entry_1"]["data_1"]["data_fourier"]
                        phases = np.angle(fourier_space)%(2*np.pi)
                        
                        # intensity scaling
                        intensity_pattern = np.power(intensity_pattern, 0.75)

                        # other choices for the intensity scaling
#                            intensity_pattern = np.log10(intensity_pattern)   
                        # sqrt is better for pristine but much worse for defective structures
#                            intensity_pattern = np.sqrt(intensity_pattern)
#                            intensity_pattern = np.cbrt(intensity_pattern)
#                            intensity_pattern = np.square(intensity_pattern)
#                            intensity_pattern = np.power(intensity_pattern, 4.0/5.0)
                        
                        # if we want to remove the central spot
                        # (like it is done experimentally most of the times)
                        if use_mask:  
                            # this values are valid only for 32x32 images
                            a, b = 13.5, 13.5
                            n = 28
                            r = 11

                            y, x = np.ogrid[-a:n-a, -b:n-b]
                            mask_ext = x*x + y*y <= r*r
                            # invert mask because we want to keep the inside
                            # of the circle
                            mask_ext = np.invert(mask_ext)
                            for i in range(28):
                                for j in range(28):
                                    if mask_ext[i,j]:
                                        intensity_pattern[i,j] =0.0
                       
#                                a, b = 14, 14
#                                n = 28
#                                r = 4
#
#                                y,x = np.ogrid[-a:n-a, -b:n-b]
#                                mask_int = x*x + y*y <= r*r
#                                # invert mask because we want to keep the inside
#                                # of the circle
#                                for i in range(28):
#                                    for j in range(28):
#                                        if mask_int[i,j]:
#                                            intensity_pattern[i,j] =0.0         

                        if grayscale:     

                            I8 = (((intensity_pattern - intensity_pattern.min()) / (intensity_pattern.max() - intensity_pattern.min())) * 255.0).astype(np.uint8)
                            
#                            rs8 = (((real_space - real_space.min()) / (real_space.max() - real_space.min())) * 255.0).astype(np.uint8)
                            rs8 = (((real_space.real - real_space.real.min()) / (real_space.real.max() - real_space.real.min())) * 255.0).astype(np.uint8)

                            ph8 = (((phases - phases.min()) / (phases.max() - phases.min())) * 255.0).astype(np.uint8)
  
                            # export numpy array (for any dimension)
                            if self.desc_space == 'k-space':
                                np.save(filename_npy, I8)
                            elif self.desc_space == 'r-space':
                                np.save(filename_npy, rs8)
                            else:
                                raise Exception("Not exporting descriptor. Wrong desc_space specified.")
      
                            # export images and npy arrays
                            if self.ndim == 2:
                                img = Image.fromarray(I8)
                                # invert image to have black background
                                img = ImageOps.invert(img)
                                img.save(filename_xray)

                                img = Image.fromarray(rs8)
                                img.save(filename_rs)

                                img = Image.fromarray(ph8)
                                img.save(filename_ph)
                            
                            else:    

#                                # print 2d images (not meaningful)                                 
#                                img = Image.fromarray(I8[0,:,:])
#                                img.save(filename_xray)
#
#                                img = Image.fromarray(rs8[0,:,:])
#                                img.save(filename_rs)
#
#                                img = Image.fromarray(ph8[0,:,:])
#                                img.save(filename_ph)
                                
                                #do not plot 3d to speed up things
                                from mayavi import mlab
                                x, y, z = np.mgrid[-5:5:64j, -5:5:64j, -5:5:64j]
                                azimuth = 0.0
                                elevation = 0.0
                
                                # export images without opening mayavi viewer
                                mlab.options.offscreen = True
#                                mlab.options.offscreen = False
                            
                                mlab.clf()
                                obj = mlab.contour3d(rs8, contours=10, opacity=.2)                                
                                obj.scene.disable_render = True
                                obj.scene.anti_aliasing_frames = 0
                                mlab.view(azimuth=azimuth, elevation=elevation)
                                mlab.savefig(filename_rs)
#                                mlab.show()

                                mlab.clf()        
                                obj = mlab.contour3d(I8, contours=10, opacity=.2)
                                obj.scene.disable_render = True
                                obj.scene.anti_aliasing_frames = 0
                                mlab.view(azimuth=azimuth, elevation=elevation)
                                mlab.savefig(filename_xray)
#                                mlab.show()
                                
                                mlab.close(all=True)

#                                mlab.colorbar(title='Potential', orientation='vertical')

#                                img = Image.fromarray(ph8[0,:,:])
#                                img.save(filename_ph)

#                                mlab.clf()        
#                                obj = mlab.contour3d(ph8, contours=5, opacity=.2)
#                                obj.scene.disable_render = True
#                                obj.scene.anti_aliasing_frames = 0
#                                mlab.savefig(filename_ph)
#                                mlab.show()



                        else:
                            pypl.imsave(filename_xray, np.log10(intensity_pattern))
                            #pypl.imsave(filename_xray, intensity_pattern)

                            np.save(filename_npy, intensity_pattern)
                            #pypl.imsave(filename_xray, np.log10(intensity_pattern), vmin=vmin)
                            pypl.imsave(filename_ph, phases)
                            pypl.imsave(filename_rs, abs(real_space))
                        
#                        if configs["isBeaker"] == "True":
                        if False:
                            # only for Beaker Notebook
                            filename_xray = os.path.abspath(os.path.normpath(os.path.join('/user/tmp/', '%s_%d_%d_%d%s' % (self.name, gIndexRun, gIndexDesc, angle_d, filename_suffix))))

                        # store the normalized absolute path to the png file in the class
                        structure.xray_file[gIndexRun, gIndexDesc] = filename_xray
                        structure.xray_npy_file[gIndexRun, gIndexDesc] = filename_npy
                        structure.xray_rs_file[gIndexRun, gIndexDesc] = filename_rs
    
    
                        filename_list_img.append(filename_xray)
                        filename_list_npy.append(filename_npy)
                        filename_list_rs.append(filename_rs)
                        
                    # write geometry (outside the angle loop because it does 
                    # not depend on the angle)
                    # NB: works only for structures in aims_format
                    structure.write_geometry(path=path,
                        filename_suffix='_aims.in',
                        format='aims',
                        operation_number=operation_number)

                    filename_geo = os.path.abspath(os.path.normpath(os.path.join(path, '%s_%d_%d_op%d%s' % (structure.name, gIndexRun, gIndexDesc, operation_number, '_aims.in'))))
                    structure.geo_file[gIndexRun, gIndexDesc] = filename_geo
                    filename_list_geo.append(filename_geo)


                else:
                    logger.error("Could not find atoms in %s" % structure.name)
                    sys.exit(1)

        else: 
            logger.error("Please specify a valid file format. Possible format is 'NOMAD'.")
            sys.exit(1)

        return filename_list_img, filename_list_npy, filename_list_rs, filename_list_geo
        
        
    def calculate(self, structure):
        pass
    

    def compress_and_write(self, filename_img_list, filename_npy_list, filename_rs_list, 
        desc_folder=None, desc_file=None, target_name=None, target_categorical=None,
        n_bins = None, disc_type = 'uniform'):    

        data_set, n_classes = read_data_sets(target_categorical=target_categorical, 
            desc_folder=desc_folder, desc_file=desc_file, one_hot=True, 
            flatten_images=False, n_bins=n_bins, target_name=target_name, disc_type=disc_type)
        
        n_rows = data_set.train.images[0].shape[0]
        n_columns = data_set.train.images[0].shape[1]

        logger.debug("Number of unique classes: {0}".format(n_classes))        
        run_keras_autoencoder(data_set, filename_img_list, filename_npy_list, filename_rs_list,
            n_classes=n_classes, n_rows=n_rows, n_columns=n_columns)
        
    
    
class AtomicFeatures(Descriptor):
    '''
    Conversion supported only for features read from collection (not mendeleev)
    '''
    def __init__(self, path_to_collection, feature_order_by=None,
        energy_unit='eV', length_unit='angstrom', metadata_info=None,
        materials_class=None,
        **params):
        super(AtomicFeatures, self).__init__()
        
        params = Descriptor.params(self)
        
        self.path_to_collection = path_to_collection 
        self.feature_order_by = feature_order_by
        self.energy_unit = energy_unit
        self.length_unit = length_unit
        self.selected_feature_list = None
        self.metadata_info = metadata_info
        self.collection = AtomicCollection("binaries", collections=self.path_to_collection)
        self.materials_class = materials_class
        
    def calculate(self, structure, selected_feature_list):
        
        self.selected_feature_list = selected_feature_list
                
        if (structure.file_format=='NOMAD') or (structure.file_format=='xyz'):
            value_list = []
            for (gIndexRun, gIndexDesc), atoms in structure.atoms.items():
                if atoms is not None:                                 
                    columns = ['chemical_formula']

                    # for a complete list see https://pypi.python.org/pypi/mendeleev)                    
                    features_mendeleev = ['atomic_number', 'group_id', 'period']
                    
                    chemical_symbols = atoms.get_chemical_symbols()
                    
                    
                    if self.materials_class == 'binaries':
                        # reduce chemical symbols and formula to binary
                        chemical_symbols = list(set(chemical_symbols))
                        if len(chemical_symbols)==1:
                            chemical_symbols *= 2 

                        values = [get_chemical_formula_binaries(atoms)]

                    else:
                        values = [atoms.get_chemical_formula(mode='hill')]

                    if len(chemical_symbols)!=2:
                        raise ValueError("More than 2 different atoms in file {0}. \n"
                                        "At the moment only binaries are possible. \n"
                                        "The chemical symbols are {1}. "
                                        "selection.".format(structure.in_file, chemical_symbols))

                                       
                    # in a given structure, order by the user-specified atomic_metadata
                    p = self.collection.get(self.feature_order_by)
                    value_order_by = p.value(chemical_symbols)
                    
                    # add lambda because the key that is being used to sort 
                    # is (val,sym), and not just value. 
                    # in case of sorting of multiple arrays this is needed
                    chemical_symbols = [sym for (val, sym) in 
                        sorted(zip(value_order_by, chemical_symbols), 
                        key=lambda pair: pair[0])]

                    
                    for idx, el_symb in enumerate(chemical_symbols):
                        for feature in selected_feature_list:
                            
                            # divide features for mendeleev and collection
                            if feature in features_mendeleev:
                                #print 'reading', feature, 'in mendeleev'
                                elem_medeleev = element(el_symb)
                                value = getattr(elem_medeleev, feature)
                                values.append(value)
                                columns.append(feature+'('+str(list(string.ascii_uppercase)[idx])+')')
                            
                                if pd.isnull(value):
                                    raise ValueError("NaN value for {0} of {1}. "
                                        "Please exclude this feature (or this "
                                        "chemical element) from your " 
                                        "selection.".format(feature, el_symb))
                                    
                            else:
                                
                                # add features from collection
                                #print el_symb
                                #print self.collection.get_atomic_properties()
                                p = self.collection.get(feature)
                                value = p.value(el_symb)
                                
                                # convert to desired units
                                unit = p.value(el_symb, 'units') 
                                
                                value = convert_energy_substance(unit, value, 
                                ureg=ureg, energy_unit=self.energy_unit, 
                                length_unit=self.length_unit)
                                    
                                values.append(value)
                                
#                                if use_short_names:
#                                    if self.metadata_info is not None:
#                                        #  check if shortname is present for feature
#                                        try:
#                                            feature = self.metadata_info[str(feature)]['shortname']
#                                        except:
#                                            pass
#                                        
#                                        columns.append(feature+'('+str(list(string.ascii_uppercase)[idx])+')')
#                                else:
                                columns.append(feature+'('+str(list(string.ascii_uppercase)[idx])+')')
                                
                    values = tuple(values)
                    value_list.append(values)
                    
                    if isBeaker:
                        # only for Beaker Notebook
                        #filename_xray = os.path.abspath(os.path.normpath(os.path.join('/user/tmp/', '%s_%d_%d_%d%s' % (self.name, gIndexRun, gIndexDesc, angle_d, filename_suffix))))
                        pass
    

        df_desc_structure = pd.DataFrame.from_records(value_list, columns=columns)
        
        return df_desc_structure
               
               
    def write(self, structure, selected_feature_list, df, dict_delta_e=None, 
        path=None, filename_suffix='.json', json_file=None):                            
        """Given the chemical composition, build the descriptor made of atomic features only.
    
        Includes all the frames in the same json file.
    
        .. todo:: Check if it works for multiple frames.
        """    
        
        # make dictionary {primary_feature: value} for each structure
        # dictionary of a dictionary, key: Mat, value: atomic_features
        dict_features = df.set_index('chemical_formula').T.to_dict()
    
        if structure.isPeriodic == True:
            for (gIndexRun, gIndexDesc), atoms in structure.atoms.items():
                if atoms is not None:
                    # filename is the normalized absolute path 
                    filename = os.path.abspath(os.path.normpath(os.path.join(path, 
                        '{0}{1}'.format(structure.name, filename_suffix))))
    
                    outF=file(filename,'w')
                    outF.write("""
            {
                  "data":[""")
        
                    cell = structure.atoms[gIndexRun,gIndexDesc].get_cell()
#                    cell = np.transpose(cell)
                    atoms = structure.atoms[gIndexRun,gIndexDesc]
                    if self.materials_class == 'binaries':
                        chemical_formula = get_chemical_formula_binaries(atoms)
                    else:
                        chemical_formula = structure.chemical_formula[gIndexRun,gIndexDesc]
                    energy_total__eV = structure.energy_total__eV[gIndexRun,gIndexDesc]/len(atoms)
                    energy_total = structure.energy_total[gIndexRun,gIndexDesc]/len(atoms)
                    #label = label_[gIndexRun,gIndexDesc]             
                    #target = dict_delta_e.get(chemical_formula_[gIndexRun, gIndexDesc])[0]
                    target = dict_delta_e.get(chemical_formula)
                    atomic_features =  dict_features[chemical_formula]
                    
    
                    res={
                        "checksum": structure.name,
                        #"label": label,
                        "energy_total": energy_total,
                        "energy_total__eV": energy_total__eV,
                        #"is_lowest_energy": is_lowest_energy,
                        "delta_e_target": target,
                        "chemical_formula": chemical_formula,
                        "gIndexRun": gIndexRun,
                        "gIndexDesc": gIndexDesc,
                        "cell": cell.tolist(),
                        "particle_atom_number": map(lambda x: x.number, atoms),
                        "particle_position": map(lambda x: [x.x,x.y,x.z], atoms),
                        "atomic_features": atomic_features,
                        "main_json_file_name": structure.in_file,
                        "energy_unit": self.energy_unit,
                        "length_unit": self.length_unit,
                        "feature_order_by": self.feature_order_by,
                        
                    }
                        
                    json.dump(res, outF, indent=2)
                    outF.write("""
            ] }""")
                    outF.flush()

            
            return filename    

        
class Prdf(Descriptor):
    pass
    

class XrayDiffraction1D(Descriptor):
    def __init__(self, **params):
        super(XrayDiffraction1D, self).__init__()
        params = Descriptor.params(self)
                    
    def calculate(self, structure):
        if (structure.file_format=='NOMAD') or (structure.file_format=='xyz'):
            for (gIndexRun, gIndexDesc), atoms in structure.atoms.items():
                if atoms is not None:        
                    mg_atoms = AseAtomsAdaptor.get_structure(atoms)        
                    #c = XRDCalculator(wavelength=3.0)
                    #c = XRDCalculator(wavelength="AgKb1")
                    c = XRDCalculator()
        #c.get_xrd_plot(mg_atoms) 
        #c.show_xrd_plot(mg_structure)
        return c.get_xrd_data(mg_atoms)

    def write(self, structure, path):

        filename = os.path.abspath(os.path.normpath(os.path.join(path, '%s_%s' % (structure.name, 'xrd1d.json'))))
        
        out_file = file(filename, 'w')
        
        out_file.write("""
    {
          "data":[""")

        for (gIndexRun, gIndexDesc), atoms in structure.atoms.items():
            if atoms is not None:
                    cell = structure.atoms[gIndexRun, gIndexDesc].get_cell()
#                    cell = np.transpose(cell)
                    atoms = structure.atoms[gIndexRun, gIndexDesc]
                    energy_eV = structure.energy_eV[gIndexRun, gIndexDesc]
                    
                    writeColon = False

                    xrd_1d_data = self.calculate(structure)

                    two_theta = [item[0] for item in xrd_1d_data]
                    intensity = [item[1] for item in xrd_1d_data]
                    miller_idx = [item[2] for item in xrd_1d_data]                        
                    d_hkl = [item[3] for item in xrd_1d_data]

                    # convert Miller idx key to string to print with json
                    for idx, item in enumerate(miller_idx):
                        miller_idx[idx] = {str(key):value for key, value in item.items()}
                    
                    
                    res = {
                        "calc_id": 'NaN',
                        "checksum": structure.name,
                        "energy_tot": energy_eV,
                        "path": filename,
                        "step": gIndexDesc,
                        "final": 'NaN',
                        "struct_id": 'NaN',
                        "cell": cell.tolist(),
                        "particle_atom_number": map(
                            lambda x: x.number,
                            atoms),
                        "particle_position": map(
                            lambda x: [
                                x.x,
                                x.y,
                                x.z],
                            atoms),
                        "two_theta": two_theta,
                        "intensity": intensity,
                        "miller_idx": miller_idx,
                        "d_hkl": d_hkl
                            }
                    if (writeColon):
                        out_file.write(", ")
                    writeColon = True
                    json.dump(res, out_file, indent=2)
            out_file.write("""
    ] }""")
            out_file.flush()

        return filename
        
        
    def read(self, archive):
        pass
    
    

    
    
    
    
    
    
    
