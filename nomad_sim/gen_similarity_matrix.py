#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import absolute_import

__author__ = "Angelo Ziletti"
__copyright__ = "Copyright 2016, The NOMAD Project"
__maintainer__ = "Angelo Ziletti"
__email__ = "ziletti@fhi-berlin.mpg.de"
__date__ = "21/10/16"


import sys,logging
from time import time
import matplotlib.pyplot as plot
import numpy as np
import scipy.sparse
from scipy.sparse import csr_matrix
import sklearn.manifold
from sklearn import decomposition
from sklearn import preprocessing

from nomad_sim.config import configs

try:
    hdlr = logging.FileHandler(configs["output_file"], mode='a')
except:
    hdlr = logging.FileHandler(configs["output_file"], mode='w')

level = logging.getLevelName(configs["log_level_general"])

logger = logging.getLogger(__name__)
logger.setLevel(level)
logging.basicConfig(level=level)
FORMAT = "%(levelname)s: %(message)s"
formatter = logging.Formatter(fmt=FORMAT)
handler = logging.StreamHandler()
handler.setFormatter(formatter)
hdlr.setFormatter(formatter)
logger.addHandler(handler)
logger.addHandler(hdlr)
logger.setLevel(level)
logger.propagate = False

__metainfopath__ = configs["meta_info_file"]




def save_sparse_csr(filename, array):
    np.savez(filename,
             data=array.data,
             indices=array.indices,
             indptr=array.indptr,
             shape=array.shape)


def load_sparse_csr(filename):
    loader = np.load(filename)
    return csr_matrix((loader['data'], loader['indices'], loader['indptr']),
                      shape=loader['shape'])



def load_sim_matrix(matrix_file=None, desc_type=None):
    """ Reads the similarity matrix from file.
        TO DO: add labels for prdf descriptor
    """

    if matrix_file is None:
        matrix_file = '/tmp/data.npz'

    loader = np.load(matrix_file)


    if desc_type=='prdf':
        X = scipy.sparse.csr_matrix((loader['X_train_data'],
                                     loader['X_train_indices'],
                                     loader['X_train_indptr']),
                                    shape=loader['X_train_shape'])
        y = loader['y_train'].astype(np.int64)
        lookup = loader['lookup']
    
        classes = np.unique(y)
        counts = np.bincount(y)
        idx = np.argsort(counts)
 
        X.data = np.nan_to_num(X.data)
        X = X.data
        
        X_labels = None
    
    elif desc_type=='atomic_features':
        X = loader['X_train']
        y = loader['y_train'].astype(np.int64)
        lookup = loader['lookup']
        X_labels = np.reshape(loader['X_labels'],-1).tolist()
        
        classes = np.unique(y)
        idx = np.zeros_like(y)

        target = []
    
        for i in range(len(y)):
            target.append((
                # target value 
                float(lookup[i][3]),
                # label
            ))        


        X = np.nan_to_num(X)
        
        
    elif desc_type=='xray':
        X = loader['X_train']
        y = loader['y_train']
        lookup = loader['lookup']
        
        classes = np.unique(y)
        logger.debug("Number of (unique) classes according to target: {0}".format(classes.shape[0]))
        idx = np.zeros_like(y)

        target = []
    
        for i in range(len(y)):
            target.append((
                # target value 
                str(lookup[i][3]),
                # label
            ))        

        target = np.asarray(target)
        X_labels = None        
        
        
    return X, X_labels, target, lookup
        
    


def sim_matrix_to_embedding(method=None, matrix_file=None, plot_file=None, lookup_file=None, 
    class_count=None, outlier_limit=None, embed_params=None, desc_type=None, standardize=False,
    target=None):

    """
    filename: data.npz file generated with convert.py.
    plot_file: filename for the plot file.
    class_count: number of classes to plot, will use the x most populous ones
    outlier_limit: [optional] cut of outliers to not destroy some plots
    """

    if matrix_file is None:
        matrix_file = '/tmp/data.npz'

    if plot_file is None:
        plot_file = '/tmp/plot.png'

    if lookup_file is None:
        lookup_file = '/tmp/lookup.dat'

    if outlier_limit is None:
        outlier_limit = 0
        
    if method is None:
        method = 'tsne'
        
    if class_count is None:
        class_count = int(1E6)


    loader = np.load(matrix_file)

    if desc_type=='xray_1d':
        outlier_limit = int(outlier_limit)
        class_count = int(class_count)
    
    
        X = scipy.sparse.csr_matrix((loader['X_train_data'],
                                     loader['X_train_indices'],
                                     loader['X_train_indptr']),
                                    shape=loader['X_train_shape'])
        y = loader['y_train'].astype(np.int64)
        lookup = loader['lookup']
    
        X.data = np.nan_to_num(X.data)
        

    elif desc_type=='prdf':
        outlier_limit = int(outlier_limit)
        class_count = int(class_count)
    
    
        X = scipy.sparse.csr_matrix((loader['X_train_data'],
                                     loader['X_train_indices'],
                                     loader['X_train_indptr']),
                                    shape=loader['X_train_shape'])
        y = loader['y_train'].astype(np.int64)
        lookup = loader['lookup']
    
        classes = np.unique(y)
        counts = np.bincount(y)
        idx = np.argsort(counts)
    
        used_classes = classes[idx][-class_count:]
        idx = np.zeros_like(y)
        for cls in used_classes:
            idx = np.logical_or(idx, y == cls)
    
        X = X[idx]
        y = y[idx]
    
        X.data = np.nan_to_num(X.data)


    elif desc_type=='rdf':
        outlier_limit = int(outlier_limit)
        class_count = int(class_count)
    
    
        X = scipy.sparse.csr_matrix((loader['X_train_data'],
                                     loader['X_train_indices'],
                                     loader['X_train_indptr']),
                                    shape=loader['X_train_shape'])
        y = loader['y_train'].astype(np.int64)
        lookup = loader['lookup']
    
        classes = np.unique(y)
        counts = np.bincount(y)
        idx = np.argsort(counts)
    
        used_classes = classes[idx][-class_count:]
        idx = np.zeros_like(y)
        for cls in used_classes:
            idx = np.logical_or(idx, y == cls)
    
        X = X[idx]
        y = y[idx]
    
        X.data = np.nan_to_num(X.data)
        
        
    elif desc_type=='atomic_features':
        X = loader['X_train']
        y = loader['y_train'].astype(np.int64)
        lookup = loader['lookup']
                
        classes = np.unique(y)
        idx = np.zeros_like(y)

        used_classes = classes[idx][-class_count:]
        idx = np.zeros_like(y)
        for cls in used_classes:
            idx = np.logical_or(idx, y == cls)
    
        X = np.nan_to_num(X)
        
    elif desc_type=='xray':
        X = loader['X_train']
        y = loader['y_train'].astype(np.int64)
        lookup = loader['lookup']
       
        logger.debug("Feature matrix dimension: {0}".format(X.shape))
                 
        classes = np.unique(y)
        idx = np.zeros_like(y)

        used_classes = classes[idx][-class_count:]
        idx = np.zeros_like(y)
        for cls in used_classes:
            idx = np.logical_or(idx, y == cls)
    
        X = np.nan_to_num(X)


    # convert sparse to dense if needed because the standard scaler does not 
    # work for sparse arrays
    try:
        X = X.toarray()
    except:
        pass

    # we need ==True because the value is passed by the Beaker Notebook
    # and not recognized as boolean (maybe this could be fixed)
    if standardize=='True':
        scaler = preprocessing.StandardScaler(copy=False, with_mean=True, with_std=True).fit(X)
        logger.info('Data standardized by removing the mean and scaling to unit variance.')
    else:
        scaler = preprocessing.StandardScaler(copy=False, with_mean=True, with_std=False).fit(X)
        logger.info('Data standardized by removing the mean; no scaling to unit variance.')
    
    X = scaler.transform(X)
    

    t0 = time()

    if method=='tsne':
        # t-sne random initialization (X can be sparse)
        logger.info('Using t-SNE as embedding method. Random initialization.')

        params = {  
            'method': 'exact', 
            'init': 'random', 
            'random_state': 42, 
            'verbose': 0, 
        }
        # update default parameters with the ones supplied by the user
        # if a keyword has two values, the value supplied by the user is retained
        params_default = params.copy()

        try:
            params.update(embed_params)
            tsne = sklearn.manifold.TSNE(**params)
        except:        
            logger.debug('Could not load user-defined parameters. Please check if the passed paramters are allowed for the method chosen')
            logger.debug('Using default values.')
            tsne = sklearn.manifold.TSNE(**params_default)
            pass  

        try:
            X = X.toarray()
        except:
            pass
        tsne = sklearn.manifold.TSNE(**params) 
        mapping = tsne.fit_transform(X)

    elif method=='tsne_approx':
        # t-sne random initialization, approximate method (X can be sparse)
        logger.info("Using t-SNE (approximate) as embedding method.")

        params = {
            'method': 'barnes_hut',
            'init': 'random',
            'random_state': 42,
            'verbose': 0,
        }
        params_default = params.copy()

        try:
            params.update(embed_params)
            tsne = sklearn.manifold.TSNE(**params)
        except:        
            logger.debug('Could not load user-defined parameters. Please check if the passed paramters are allowed for the method chosen')
            logger.debug('Using default values.')
            tsne = sklearn.manifold.TSNE(**params_default)
            pass  
        
        try:
            X = X.toarray()
        except:
            pass   
        mapping = tsne.fit_transform(X)


    elif method=='tsne_pca':
        # t-sne PCA initialization (X needs to be dense)
        logger.info('Using t-SNE as embedding method. PCA initialization.')
        params = {
            'method': 'exact',
            'init': 'pca',
            'random_state': 42,
            'verbose': 0,
        }
        params_default = params.copy()

        try:
            params.update(embed_params)
            tsne = sklearn.manifold.TSNE(**params)
        except:        
            logger.debug('Could not load user-defined parameters. Please check if the passed paramters are allowed for the method chosen')
            logger.debug('Using default values.')
            tsne = sklearn.manifold.TSNE(**params_default)
            pass  
        
        # convert sparse matrix to dense if needed
        try:
            X = X.toarray()
        except:
            pass
        mapping = tsne.fit_transform(X)

    elif method=='spectral_embedding':
        logger.info('Using Spectral Embedding as embedding method')
        params = {
            'n_components': 2,
            'n_neighbors': 10,
            'random_state': 42,
        }
        params_default = params.copy()

        try:
            params.update(embed_params)
            spect_embed = sklearn.manifold.SpectralEmbedding(**params)
        except:        
            logger.debug('Could not load user-defined parameters. Please check if the passed paramters are allowed for the method chosen')
            logger.debug('Using default values.')
            spect_embed = sklearn.manifold.SpectralEmbedding(**params_default)
            pass  
        
        # convert sparse matrix to dense if needed
        try:
            X = X.toarray()
        except:
            pass
        mapping = spect_embed.fit_transform(X)

    elif method=='mds':
        logger.info('Using Multidimensional scaling as embedding method')
        params = {
            'n_components': 2,
            'n_init': 1,
            'random_state': 42,
            'verbose': 0,
        }
        params_default = params.copy()

        try:
            params.update(embed_params)
            mds = sklearn.manifold.MDS(**params)
        except:        
            logger.debug('Could not load user-defined parameters. Please check if the passed paramters are allowed for the method chosen')
            logger.debug('Using default values.')
            mds = sklearn.manifold.MDS(**params_default)
            pass  
        
        # convert sparse matrix to dense if needed
        try:
            X = X.toarray()
        except:
            pass
        mapping = mds.fit_transform(X)

    elif method=='isomap':
        logger.info('Using Isomap as embedding method')
        params = {
            'n_components': 2,
            'n_neighbors' : 5
        }
        params_default = params.copy()

        try:
            params.update(embed_params)
            isomap = sklearn.manifold.Isomap(**params)
        except:        
            logger.debug('Could not load user-defined parameters. Please check if the passed paramters are allowed for the method chosen')
            logger.debug('Using default values.')
            isomap = sklearn.manifold.Isomap(**params_default)
            pass      
        
        # convert sparse matrix to dense if needed
        try:
            X = X.toarray()
        except:
            pass
        mapping = isomap.fit_transform(X)

    elif method=='hessian':
        logger.info('Using Hessian Locally Linear Embedding as embedding method')
        params = {
            'method': 'hessian',
            'n_components': 2,
            'n_neighbors' : 6,
            'random_state': 42,
        }
        params_default = params.copy()

        try:
            params.update(embed_params)
            hessian = sklearn.manifold.LocallyLinearEmbedding(**params)
        except:        
            logger.debug('Could not load user-defined parameters. Please check if the passed paramters are allowed for the method chosen')
            logger.debug('Using default values.')
            hessian = sklearn.manifold.LocallyLinearEmbedding(**params_default)
            pass

        # convert sparse matrix to dense if needed
        try:
            X = X.toarray()
        except:
            pass
        mapping = hessian.fit_transform(X)

    elif method=='pca':
        logger.info('Using Principal Component Analysis (PCA) as embedding method')
        params = {
            'n_components': 2,
        }
        params_default = params.copy()

        try:
            params.update(embed_params)
            pca = decomposition.PCA(**params)
        except:        
            logger.debug('Could not load user-defined parameters. Please check if the passed paramters are allowed for the method chosen')
            logger.debug('Using default values.')
            pca = decomposition.PCA(**params_default)
            pass

        # convert sparse matrix to dense if needed
        try:
            X = X.toarray()
        except:
            pass
        mapping = pca.fit_transform(X)
        
        
        tot_exp_var = sum(pca.explained_variance_ratio_)
        logger.info('Explained variance by each component (%):{}'.format(pca.explained_variance_ratio_*100.0))
        logger.info('Total variance explained (%): {}'.format(tot_exp_var*100.0))
        logger.info('Eigenvectors: ')
        logger.info('Eigenvector 1: \n{}'.format(pca.components_[0]))
        logger.info('Eigenvector 2: \n{}'.format(pca.components_[1]))

        
        
    elif method=='ipca':
        logger.info('Using Incremental Principal Component Analysis (PCA) as embedding method')
        params = {
            'n_components': 2,
        }

        params_default = params.copy()

        try:
            params.update(embed_params)
            ipca = decomposition.IncrementalPCA(**params)
        except:        
            logger.debug('Could not load user-defined parameters. Please check if the passed paramters are allowed for the method chosen')
            logger.debug('Using default values.')
            ipca = decomposition.IncrementalPCA(**params_default)
            pass        
        
        # convert sparse matrix to dense if needed
        try:
            X = X.toarray()
        except:
            pass
        mapping = ipca.fit_transform(X)
        
        tot_exp_var = sum(ipca.explained_variance_ratio_)
        logger.info('Explained variance by each component (%):{}'.format(ipca.explained_variance_ratio_*100.0))
        logger.info('Tot variance explained (%): {}'.format(tot_exp_var*100.0))
        
    elif method=='kernel_pca':
        logger.info('Using Kernel Principal Component Analysis as embedding method')
        params = {
            'n_components': 2,
            'kernel': 'rbf',
        }
        params_default = params.copy()

        try:
            params.update(embed_params)
            kpca = decomposition.KernelPCA(**params)
        except:        
            logger.debug('Could not load user-defined parameters. Please check if the passed paramters are allowed for the method chosen')
            logger.debug('Using default values.')
            kpca = decomposition.KernelPCA(**params_default)
            pass   

        # convert sparse matrix to dense if needed
        try:
            X = X.toarray()
        except:
            pass
        mapping = kpca.fit_transform(X)

    elif method=='truncated_svd':
        logger.info('Using TruncatedSVD as embedding method')
        params = {
            'n_components': 2,
        }  
        params_default = params.copy()

        try:
            params.update(embed_params)
            svd = decomposition.TruncatedSVD(**params)
        except:        
            logger.debug('Could not load user-defined parameters. Please check if the passed paramters are allowed for the method chosen')
            logger.debug('Using default values.')
            svd = decomposition.TruncatedSVD(**params_default)
            pass   

        # convert sparse matrix to dense if needed
        try:
            X = X.toarray()
        except:
            pass
        mapping = svd.fit_transform(X)
        

    else:
        logger.error("Please specify a valid method for embedding. Possible methods are:"
            + "'tsne', 'tsne_approx', 'tsne_pca', 'spectral_embedding', 'mds', 'isomap'.")
        sys.exit(1)

    t1 = time()
    logger.info("Time to compute 2d-embedding: %.2g sec" % (t1 - t0))

        
    if desc_type=='prdf':

        lookup_out = []
        for i in range(len(y)):
            lookup_out.append((
                # json_file path
                lookup[idx][i][1],
                # frame_number
                lookup[idx][i][2],
                # x_clustering   
                str(mapping[i][0]),
                # y_clustering   
                str(mapping[i][1]),
                # energy value 
                lookup[idx][i][3],
                # checksum
                lookup[idx][i][4],
                # chemical_formula
                'None',
            ))        

    elif desc_type=='rdf':

        lookup_out = []
        for i in range(len(y)):
            lookup_out.append((
                # json_file path
                lookup[idx][i][1],
                # frame_number
                lookup[idx][i][2],
                # x_clustering   
                str(mapping[i][0]),
                # y_clustering   
                str(mapping[i][1]),
                # energy value 
                lookup[idx][i][3],
                # checksum
                lookup[idx][i][4],
                # chemical_formula
                'None',
            ))           
        
        
    elif desc_type=='atomic_features':

        lookup_out = []
        for i in range(len(y)):
            lookup_out.append((
                # json_file path
                lookup[i][1],   
                # frame_number
                lookup[i][2],
                # x_clustering   
                str(mapping[i][0]), 
                # y_clustering
                str(mapping[i][1]),
                # target value 
                #lookup[i][3],
                str(target[i][0]),
                # label
                lookup[i][4],
                # chemical_formula
                lookup[i][5]
            ))

    elif desc_type=='xray':

        lookup_out = []
        for i in range(len(y)):
            lookup_out.append((
                # json_file path
                lookup[i][1],   
                # frame_number
                lookup[i][2],
                # operation_number
                #lookup[i][3],
                # x_clustering   
                str(mapping[i][0]), 
                # y_clustering
                str(mapping[i][1]),
                # target value 
                lookup[i][3],
                # chemical_formula
                lookup[i][4]

            ))  
            
    elif desc_type=='xray_1d':

        lookup_out = []
        for i in range(len(y)):
            lookup_out.append((
                # json_file path
                lookup[i][1],   
                # frame_number
                str(lookup[i][2]),
                # x_clustering   
                str(mapping[i][0]), 
                # y_clustering
                str(mapping[i][1]),
                # target value 
                #lookup[i][3],
                # chemical_formula
                #lookup[i][4]

            ))  
            

   
    with open(lookup_file, "w") as f:
        f.write("\n".join([" ".join(x) for x in lookup_out]))        


# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

