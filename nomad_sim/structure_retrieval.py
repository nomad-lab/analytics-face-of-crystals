#! /usr/bin/env python
from __future__ import absolute_import

__author__ = "Daria M. Tomecka and Angelo Ziletti"
__copyright__ = "Copyright 2017, The NOMAD Project"
__maintainer__ = "Daria M. Tomecka and Angelo Ziletti"
__email__ = "tomeckadm@gmail.com; ziletti@fhi-berlin.mpg.de"
__date__ = "06/04/16"

import soap
import sys
import ase.io
import json
import numpy as np
import pickle
import h5py
import time
import datetime
import os
import logging





sim_treshold = 0.2
max_nr_structures = 10

                  
def structure_retrieval (kmat, json_list, structure_idx, sim_treshold=0.2, max_nr_structures=10):

    kmat_slice = kmat [structure_idx,:]
    idx_list_sim = np.asarray(np.where(kmat_slice > sim_treshold)).ravel()

    #idx_list_sim =  numpy.where( sim_scores > sim_treshold )
    sim_scores = kmat_slice[idx_list_sim]
    print ("indices of scores above treshold:",idx_list_sim)
    print ("sim_scores", sim_scores)

    # 2nd approach (probably slower)
    #structure_idx = enumerate(sim_scores)
    #print (structure_idx)
    #idx_list_sim =[i for i,v in enumerate(sim_scores) if v > sim_treshold]
    

    #print('sim_treshold:', sim_treshold)
    #print ("sim_scores:", sim_scores)
    #print ("indices of scores above treshold:",idx_list_sim)
    #sim_scores[ numpy.where( sim_scores > sim_treshold ) ]

    return idx_list_sim, sim_scores

idx_list_sim, sim_scores = structure_retrieval(kmat, json_list, structure_idx=1, sim_treshold=sim_treshold, max_nr_structures=max_nr_structures)
json_list_retrieved = list(np.asarray(json_list)[idx_list_sim])

ase_atoms_list_retrieved, _ = read_data(json_list_retrieved)
kmat_reduced, dmat_reduced = calc_soap_similarity(ase_atoms_list_retrieved, tmp_folder, h5_file, soap_options)

print kmat_reduced
