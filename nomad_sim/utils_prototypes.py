#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Includes function that reads the prototypes and returns list of ase structures
 (a list of 286 elements), and also the list of labels (with the formatting like
 in the classify_by_norm_wyckoff function:
 space_group-prototype_name-Pearsons_symbol).
"""

from __future__ import absolute_import

__author__ = "Daria M. Tomecka and Angelo Ziletti"
__copyright__ = "Copyright 2017, The NOMAD Project"
__maintainer__ = "Daria M. Tomecka and Angelo Ziletti"
__email__ = "tomeckadm@gmail.com; ziletti@fhi-berlin.mpg.de"
__date__ = "12/04/17"

import soap
import sys
import ase.io
import json
import numpy as np
import pickle
import h5py
import time
import datetime
import os
import logging

from nomad_sim.wrappers import get_json_list
from nomad_sim.wrappers import plot, logger
from nomad_sim.utils_crystals import get_spacegroup
from nomad_sim.utils_crystals import create_supercell
from nomad_sim.wrappers import get_json_list
from nomad_sim.nomad_structures import NOMADStructure
from soap.tools import AseConfig

LOGGER = logging.getLogger(__name__)
logging.basicConfig(level=logging.ERROR)


#prototypes_file = os.path.normpath("/nomad-lab-base/analysis-tools/structural-similarity/python-modules/nomad_sim/structure_types.py")

def read_prototypes (prototypes_list):
    frame_list = None
    frame_list_idcs = [ (0,0) ]


    #data_file_format='NOMAD'


    op_list = np.zeros(len(prototypes_list))
    logger.info("Converting data (%d archives)..." % len(prototypes_list))
    ase_prototypes_list = []
    prototypes_label_list = []
    nmd_struct_list = []
    frame_list_idx_list = []
    target_list = []
    label_list = []
    z_count_global = {}
    # for space_group.prototypes in structure_types_by_spacegroup:
    #    for prototype in prototypes

    strSpG = structure_types_by_spacegroup
    for spaceGroupNr, protos in strSpG.items():
        for protoDict in protos:
                #print protos
                simulation_cell = protoDict.get('lattice_vectors')
                labels = protoDict.get('atom_labels')
                atom_pos = protoDict.get('atom_positions')
                #print simulation_cell, labels, atom_pos
                prototype_name = protoDict.get('Prototype')
                #print ("Reading prototypes:",prototype_name)
                if not simulation_cell or not labels or not atom_pos:
                    raise Exception("Invalid geometry for space group %d\, %s" % (spaceGroupNr, protoDict))
                atom_pos = 1.0e10 * np.asarray(atom_pos)
                simulation_cell = 1.0e10 * np.asarray(simulation_cell)
                ase_prototypes = ase.Atoms(symbols = labels, positions = atom_pos, cell = simulation_cell,pbc = True)
                #ase_prototypes = ase.Atoms(symbols = labels, positions = atom_pos, cell = np.transpose(simulation_cell), pbc=False)
                prototypes_label = '%d-%s-%s' % (spaceGroupNr, protoDict['Prototype'], protoDict['Pearsons Symbol'])
                #print prototypes_label

                #for idx, c in enumerate(ase_prototypes_list):
                #    c.info['idx'] = idx
                #    c.info['label'] = prototypes_label


                prototypes_label_list.append(prototypes_label)
                ase_prototypes_list.append(ase_prototypes)

    for idx, c in enumerate(ase_prototypes_list):
        c.info['idx'] = idx
        c.info['label'] = prototypes_label_list[idx]




    #if len(ase_prototypes_list) == len(prototypes_label_list):
    #    logging.info("Lenghts are OK")
    #else:
    #    logging.error("failure, differenct lenghts %d and %d", (len(ase_prototypes_list), len(prototypes_label_list)))

    return ase_prototypes_list, prototypes_label_list


read_prototypes (prototypes_list)



