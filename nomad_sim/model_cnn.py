#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import absolute_import
from __future__ import division

__author__ = "Angelo Ziletti"
__copyright__ = "Copyright 2016, The NOMAD Project"
__maintainer__ = "Angelo Ziletti"
__email__ = "ziletti@fhi-berlin.mpg.de"
__date__ = "21/10/16"

import cPickle as pickle
import os
import logging
import itertools
import sys
import stat
import json
import tarfile
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import copy
from scipy import constants
from keras.models import model_from_json
import keras.backend as K
K.set_image_dim_ordering('th')

from sklearn.metrics.pairwise import cosine_similarity
from nomad_sim.nomad_structures import NOMADStructure
from nomad_sim.viewer import Viewer
from nomad_sim.gen_similarity_matrix import sim_matrix_to_embedding, load_sim_matrix
from nomad_sim.l1_l0 import choose_atomic_features, write_atomic_features, classify_rs_zb
from nomad_sim.l1_l0 import get_energy_diff, combine_features, l1_l0_minimization
#from nomad_sim.sis import SIS
from bokeh.util.browser import view
from nomad_sim.cnn_preprocessing import read_data_sets
from nomad_sim.cnn_preprocessing import make_data_sets
from nomad_sim.cnn_preprocessing import load_data_from_pickle
from nomad_sim.tf_autoencoder import run_autoencoder
from nomad_sim.cnn import run_cnn_tensorflow
from nomad_sim.cnn import train_cnn_keras, train_3d_cnn_keras
from nomad_sim.cnn import predict_cnn_keras
from nomad_sim.keras_autoencoder import run_keras_autoencoder
from nomad_sim.utils_plotting import plot_save_cnn_results
from nomad_sim.utils_crystals import create_vacancies
from nomad_sim.descriptors import XrayDiffraction
from nomad_sim.descriptors import XrayDiffraction1D
from nomad_sim.descriptors import Descriptor
from nomad_sim.descriptors import AtomicFeatures
from nomad_sim.utils_crystals import get_spacegroup
from nomad_sim.utils_crystals import modify_crystal
from nomad_sim.utils_crystals import create_supercell
from nomad_sim.utils_crystals import get_target_diff_dic
from nomad_sim.utils_crystals import select_diff_from_dic
from nomad_sim.utils_crystals import convert_energy_substance
from nomad_sim.utils_plotting import insert_newlines, plot_confusion_matrix
#from nomad_sim.cnn_preprocessing import DataSets
import nomadcore.unit_conversion.unit_conversion as uc
import nomadcore.parser_backend as parser
from nomadcore.local_meta_info import loadJsonFile, InfoKindEl


import warnings 
from pint import UnitRegistry
# disable DeprecationWarning for scikit learn
warnings.filterwarnings('ignore', category=DeprecationWarning)
# disable warnings from pint
logging.getLogger("pint").setLevel(logging.ERROR)


from nomad_sim.config import configs

try:
    hdlr = logging.FileHandler(configs["output_file"], mode='a')
except:
    hdlr = logging.FileHandler(configs["output_file"], mode='w')

ureg = UnitRegistry(os.path.normpath(configs["ureg_file"]))
level = logging.getLevelName(configs["log_level_general"])

logger = logging.getLogger(__name__)
logger.setLevel(level)
logging.basicConfig(level=level)
FORMAT = "%(levelname)s: %(message)s"
formatter = logging.Formatter(fmt=FORMAT)
handler = logging.StreamHandler()
handler.setFormatter(formatter)
hdlr.setFormatter(formatter)
logger.addHandler(handler)
logger.addHandler(hdlr)
logger.setLevel(level)
logger.propagate = False

__metainfopath__ = configs["meta_info_file"]

    
#def iterate_minibatches(inputs, targets, batchsize, shuffle=False):
#    assert len(inputs) == len(targets)
#    if shuffle:
#        indices = np.arange(len(inputs))
#        np.random.shuffle(indices)
#        
#    for start_idx in range(0, len(inputs) - batchsize + 1, batchsize):
#        print start_idx, len(inputs) - batchsize
#        if len(inputs) % batchsize == 0:
#
#        if shuffle:
#            excerpt = indices[start_idx:start_idx + batchsize]
#        else:
#            excerpt = slice(start_idx, start_idx + batchsize)
#        yield inputs[excerpt], targets[excerpt]
        
def iterate_minibatches(inputs, targets, batchsize, shuffle=False):
    assert len(inputs) == len(targets)
    if shuffle:
        indices = np.arange(len(inputs))
        np.random.shuffle(indices)
        
    last_batchsize = len(inputs) % batchsize 
    if last_batchsize == 0:
        nb_batch = len(inputs)//batchsize
    else:
        nb_batch = len(inputs)//batchsize + 1 
    
    for start_idx in range(0, nb_batch, 1):
        if start_idx == len(inputs)//batchsize:
            if shuffle:
                excerpt = indices[start_idx:start_idx + last_batchsize]
            else:
                excerpt = slice(start_idx, start_idx + last_batchsize)
            yield inputs[excerpt], targets[excerpt]
        else:
            if shuffle:
                excerpt = indices[start_idx:start_idx + batchsize]
            else:
                excerpt = slice(start_idx, start_idx + batchsize)
            yield inputs[excerpt], targets[excerpt]            
 
def get_features(dataset, model, target_layer, input_dims=None, batch_size=None):
    """
    This function takes three variables:
    model = your deep learning model.
    data = batch of input data, correctly formated according to your keras model input shape
    target_layer: index of the target layer, default is the second layer from the end
    
    OUTPUT: gives layer activations/features of a given layer in keras model identified by it's index in model.layers[index].output, 
    in the example below we are caluclating output for layer indexed 7 (indexing in this case starts from 0, i.e., the input or first layer has a index zero)
    To calculate/see the index of a particular layer, call "model_summary()"
    	
     **NOTE** : if you directly want to use this function, right after your training or with some other code, you can comment the load_model line and pass the model directly with data.
     Otherwise you can also take the last two lines on the function, that will do that job as well.
    
    """
    # load the data    
    X = dataset.images
    y = dataset.labels

    activations_list = []
    batch_num = 0
    
    for batch in iterate_minibatches(X, y, batch_size, shuffle=False):
        logger.debug("Batch number: {0}/{1}".format(batch_num+1, X.shape[0]//batch_size))
        X_batch, y_batch = batch
        
#    X_batch, y_batch = dataset.next_batch(batch_size=batch_size)
        
        # reshape according to the Keras rule
        if len(input_dims) == 2:    
            X_batch = X_batch.reshape(X_batch.shape[0], 1, input_dims[0], input_dims[1])
        elif len(input_dims) == 3:
            X_batch = X_batch.reshape(X_batch.shape[0], 1, input_dims[0], input_dims[1], input_dims[2])
        else:
            raise Exception("Wrong number of dimensions.")
        batch_num += 1

        get_activations = K.function([model.layers[0].input, K.learning_phase()], model.layers[target_layer].output)
        activations_batch = get_activations([X_batch, 0])

        activations_list.append(activations_batch)

    activations = np.asarray(activations_list)
    activations = np.vstack(activations)
    activations = np.reshape(activations, (X.shape[0], -1))

    return activations
    

def load_model(model_arch_file, model_weights_file):
    """
    Load and compile model

    args: weights_path (str) trained weights file path

    returns model (Keras model)
    """

    json_file = open(model_arch_file, 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    model = model_from_json(loaded_model_json)
    logger.info('Loading model weights..')
    model.load_weights(model_weights_file)

    return model
    

def run_cnn_model(
        input_dims=None,
        method=None,
        desc_folder=None,
        tmp_folder=None,
        matrix_file=None,
        desc_file_list_train=None,
        desc_file_list_test=None,
        partial_model_architecture=None,
        plot_conf_matrix=False,
        conf_matrix_file=None,
        desc_type=None,
        descriptor=None,
        lookup_file=None,
        energy_unit=None,
        length_unit=None,
        allowed_operations=None,
        derived_features=None,
        max_dim=None,
        sis_control=None,
        control_file=None,
        results_file=None,
        target_name=None,
        target_categorical=None,
        disc_type=None,
        n_bins=None,
        checkpoint_dir=None,
        checkpoint_filename=None,
        n_steps=None,
        train=True,
        nb_epoch=None,
        batch_size=None,
        early_stopping=None,
        data_augmentation=False,
        show_model_acc=True,
        split_train_val=True,
        read_from_file=False,
        path_to_x_train=None,
        path_to_y_train=None,
        path_to_x_val=None,
        path_to_y_val=None,
        path_to_x_test=None,
        path_to_y_test=None):
    
    if train:
        if not read_from_file:
            data_set, n_classes, num_labels, class_labels = read_data_sets(target_categorical=target_categorical,
                                                 desc_folder=desc_folder, desc_file_list=desc_file_list_train,
                                                 one_hot=False, flatten_images=False, n_bins=n_bins, target_name=target_name,
                                                 disc_type=disc_type, input_dims=input_dims,
                                                 tmp_folder=tmp_folder,
#                                                 split_train_val=True)
# this was changed to have an independent test set at run time
                                                 split_train_val=split_train_val)
            
                                                 
            data_set_test, n_classes_test, num_labels, class_labels = read_data_sets(target_categorical=target_categorical,
                                                 desc_folder=desc_folder, desc_file_list=desc_file_list_test,
                                                 one_hot=False, flatten_images=False, n_bins=n_bins, target_name=target_name,
                                                 disc_type=disc_type, input_dims=input_dims,
                                                 tmp_folder=tmp_folder,
                                                 split_train_val=False)

    # flatten_images=False, n_bins=10, target_name='homo_gw',
    # disc_type='uniform')

            # data_set, n_classes = read_data_sets(target_categorical=True, desc_folder=desc_folder, desc_file=desc_file, one_hot=False,
            #    flatten_images=False, bins=20, target_name='chemical_formula')
            
            logger.debug("Number of unique classes in training/validation set: {0}".format(n_classes))
            logger.debug("Number of unique classes in test set: {0}".format(n_classes_test))

            logger.info("-----Training the convolutional neural network model.-----")

            if split_train_val:

                logger.debug("Shape of the train images: {0}".format(data_set.train.images.shape))  
                size = np.prod(data_set.train.images.shape)*8.0
                logger.debug("Matrix dimension: {0:.1f} MB".format(size / (1024**2)))
            
                logger.debug("Shape of the validation images: {0}".format(data_set.val.images.shape))    
                size = np.prod(data_set.val.images.shape)*8.0
                logger.debug("Matrix dimension: {0:.1f} MB".format(size / (1024**2)))
  
            else:
                
                logger.debug("Shape of the train images: {0}".format(data_set.images.shape))  
                size = np.prod(data_set.images)*8.0
                logger.debug("Matrix dimension: {0:.1f} MB".format(size / (1024**2)))
                
            
            if split_train_val:
                logger.debug("Shape of the train labels: {0}".format(data_set.train.labels.shape))  
                size = np.prod(data_set.train.labels.shape)*8.0
                logger.debug("Matrix dimension: {0:.1f} MB".format(size / (1024**2)))       
                
                logger.debug("Shape of the validation labels: {0}".format(data_set.val.images.shape))    
                size = np.prod(data_set.val.labels.shape)*8.0
                logger.debug("Matrix dimension: {0:.1f} MB".format(size / (1024**2)))     

            else:
                
                logger.debug("Shape of the train labels: {0}".format(data_set.labels.shape))  
                size = np.prod(data_set.labels.shape)*8.0
                logger.debug("Matrix dimension: {0:.1f} MB".format(size / (1024**2)))


            logger.debug("Shape of the test images: {0}".format(data_set_test.images.shape))    
            size = np.prod(data_set_test.images.shape)*8.0
            logger.debug("Matrix dimension: {0:.1f} MB".format(size / (1024**2)))  

            logger.debug("Shape of the test labels: {0}".format(data_set_test.images.shape))    
            size = np.prod(data_set_test.labels.shape)*8.0
            logger.debug("Matrix dimension: {0:.1f} MB".format(size / (1024**2)))     
                
            # save dataset object to file
            logger.debug("Writing pickle to file.")

            if split_train_val:
                with open(path_to_x_train, 'wb') as output:
                     pickle.dump(data_set.train.images, output, pickle.HIGHEST_PROTOCOL)
                logger.debug("Writing x_train to {0}".format(path_to_x_train))
                
                with open(path_to_y_train, 'wb') as output:
                     pickle.dump(data_set.train.labels, output, pickle.HIGHEST_PROTOCOL)
                logger.debug("Writing y_train to {0}".format(path_to_y_train))
            
                with open(path_to_x_val, 'wb') as output:
                     pickle.dump(data_set.val.images, output, pickle.HIGHEST_PROTOCOL)
                logger.debug("Writing x_validation to {0}".format(path_to_x_val))
                
                with open(path_to_y_val, 'wb') as output:
                     pickle.dump(data_set.val.labels, output, pickle.HIGHEST_PROTOCOL)
                logger.debug("Writing y_validation to {0}".format(path_to_y_val))

            else:
                with open(path_to_x_train, 'wb') as output:
                     pickle.dump(data_set.images, output, pickle.HIGHEST_PROTOCOL)
                logger.debug("Writing x_train to {0}".format(path_to_x_train))
                
                with open(path_to_y_train, 'wb') as output:
                     pickle.dump(data_set.labels, output, pickle.HIGHEST_PROTOCOL)
                logger.debug("Writing y_train to {0}".format(path_to_y_train))

            with open(path_to_x_test, 'wb') as output:
                 pickle.dump(data_set_test.images, output, pickle.HIGHEST_PROTOCOL)
            logger.debug("Writing x_test to {0}".format(path_to_x_test))
            
            with open(path_to_y_test, 'wb') as output:
                 pickle.dump(data_set_test.labels, output, pickle.HIGHEST_PROTOCOL)
            logger.debug("Writing y_test to {0}".format(path_to_y_test))
        
        # train but read datasets from file (pickle)
        else:
            logger.debug("Reading datasets from file.")
#            raise Exception("Reading from pickle is not available.")
            if split_train_val:
                x_train, y_train, x_val, y_val, x_test, y_test = load_data_from_pickle(
                    path_to_x_train=path_to_x_train, 
                    path_to_x_test=path_to_x_test,
                    path_to_y_train=path_to_y_train,
                    path_to_y_test=path_to_y_test,
                    path_to_x_val=path_to_x_val,
                    path_to_y_val=path_to_y_val)
            else:
                x_train, y_train, x_val, y_val, x_test, y_test = load_data_from_pickle(
                    path_to_x_train=path_to_x_train, 
                    path_to_x_test=path_to_x_test,
                    path_to_y_train=path_to_y_train,
                    path_to_y_test=path_to_y_test,
                    path_to_x_val=None,
                    path_to_y_val=None)                    
                
        
            # make the dataset if you read from file
            data_set = make_data_sets(input_dims=input_dims,
                train_images=x_train, 
                train_labels=y_train,
                test_images=x_test, test_labels=y_test, 
                val_images=x_val, val_labels=y_val, flatten_images=False)
#                                      
                       
        if len(input_dims) ==2: 
            df_results = train_cnn_keras(
                batch_size=batch_size,
                data_set=data_set,
                data_set_test=data_set_test,
                n_classes=n_classes,   
                input_dims=input_dims,
                partial_model_architecture=partial_model_architecture,
                img_channels=1,
                checkpoint_dir=checkpoint_dir,
                checkpoint_filename=checkpoint_filename,
                nb_epoch=nb_epoch, 
                early_stopping=early_stopping,
                data_augmentation=data_augmentation)
                        
        elif len(input_dims) ==3:
            df_results = train_3d_cnn_keras(
                batch_size=batch_size,
                data_set=data_set,
                data_set_test=data_set_test,
                n_classes=n_classes,   
                input_dims=input_dims,
                partial_model_architecture=partial_model_architecture,
                img_channels=1,
                checkpoint_dir=checkpoint_dir,
                checkpoint_filename=checkpoint_filename,
                nb_epoch=nb_epoch, 
                early_stopping=early_stopping,
                data_augmentation=data_augmentation)    
        else:
            raise Exception("Wrong number of dimensions.")
        
        # write results to disk
        # NOTE: to be implemented
        #df_results.to_csv(results_file, index=False)
        
#        sys.exit(1)
        
        # make plot
        #plot_save_cnn_results(results_file, show_plot=True)
#        return None

#        K.clear_session()

    # do no train - predict only
    else:
        data_set, n_classes, num_labels, class_labels = read_data_sets(target_categorical=target_categorical,
                                             desc_folder=desc_folder, desc_file_list=desc_file_list_test,
                                             one_hot=True, flatten_images=False, n_bins=n_bins, target_name=target_name,
                                             disc_type=disc_type, input_dims=input_dims,
                                             split_train_val=False,
                                             tmp_folder=tmp_folder)

        logger.debug("Number of unique classes in test set: {0}".format(n_classes))


        # predict on (possibly) another dataset
        prob_predictions, conf_matrix = predict_cnn_keras(data_set, n_classes, input_dims, 
            img_channels=1, batch_size=batch_size,
            checkpoint_dir=checkpoint_dir, checkpoint_filename=checkpoint_filename,
            show_model_acc=show_model_acc)

        target_pred_class = np.argmax(prob_predictions, axis=1).tolist()
        # predictions are an (n, m) array where 
        # n: # of samples, m: # of classes             
        # create dataframe with results
        df_cols = ['target_pred_class']
        for idx in range(prob_predictions.shape[1]):
            df_cols.append('prob_predictions_'+str(idx))
        df_cols.append('num_labels')
        df_cols.append('class_labels')        
                        
        df_results = pd.DataFrame(
            np.column_stack((target_pred_class, prob_predictions, num_labels, class_labels)),
            columns=df_cols)

        df_results.to_csv(results_file, index=False)

    
        # make a dataframe with the results and write it to file


        # transform it in a list of n strings to be used by the viewer
        target_pred_probs = [str(['p'+str(i)+':{0:.4f} '.format(item[i]) for i in range(n_classes)]) for item in prob_predictions]
        
        
        # insert new line if string too long
        #for item in target_pred_probs:
        #    item = insert_newlines(item, every=10)
        
        class_labels = class_labels.tolist() 
        
        unique_class_labels = list(set(class_labels))
        
        if plot_conf_matrix:
            logger.info("Calculating confusion matrix.")
            # to avoid to be shown in the Beaker notebook
            conf_matrix_plot = plot_confusion_matrix(conf_matrix, conf_matrix_file=conf_matrix_file, 
                classes=unique_class_labels, normalize=False,
                title='Confusion matrix')
    
    
        return target_pred_class, target_pred_probs, num_labels, class_labels
