#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import absolute_import
from __future__ import division

__author__ = "Angelo Ziletti"
__copyright__ = "Copyright 2016, The NOMAD Project"
__maintainer__ = "Angelo Ziletti"
__email__ = "ziletti@fhi-berlin.mpg.de"
__date__ = "21/10/16"

""" Note: the functions:
    1) get_target_diff_dic
    2) select_diff_from_dic
    
    were written by Emre Ahmetcik
(ahmetcik@fhi-berlin.mpg.de)"""


import ase
import ase.io
from ase.neighborlist import NeighborList
import json, logging, os, sys
import numpy as np
import random
from pymatgen import Lattice, Structure
from pymatgen.io.ase import AseAtomsAdaptor
from pymatgen.symmetry.analyzer import SpacegroupAnalyzer
from pymatgen import Lattice, Structure
from pymatgen.analysis.diffraction.xrd import XRDCalculator
import math
from scipy import constants
import scipy
import pandas as pd
import matplotlib.pyplot as pypl
from PIL import Image
from sklearn import preprocessing
from sklearn.metrics.pairwise import pairwise_distances
from itertools import izip
from itertools import combinations, permutations,product
from decimal import Decimal
import nomad_sim
from nomad_sim.utils_binaries import get_chemical_formula_binaries
#from nomad_sim.nomad_structures import NOMADStructure

import nomadcore.unit_conversion.unit_conversion as uc
import nomadcore.parser_backend as parser


import warnings 
from pint import UnitRegistry
# disable DeprecationWarning for scikit learn
warnings.filterwarnings('ignore', category=DeprecationWarning)
# disable warnings from pint
logging.getLogger("pint").setLevel(logging.ERROR)



from nomad_sim.config import configs

try:
    hdlr = logging.FileHandler(configs["output_file"], mode='a')
except:
    hdlr = logging.FileHandler(configs["output_file"], mode='w')

ureg = UnitRegistry(os.path.normpath(configs["ureg_file"]))
level = logging.getLevelName(configs["log_level_general"])

logger = logging.getLogger(__name__)
logger.setLevel(level)
logging.basicConfig(level=level)
FORMAT = "%(levelname)s: %(message)s"
formatter = logging.Formatter(fmt=FORMAT)
handler = logging.StreamHandler()
handler.setFormatter(formatter)
hdlr.setFormatter(formatter)
logger.addHandler(handler)
logger.addHandler(hdlr)
logger.setLevel(level)
logger.propagate = False

__metainfopath__ = configs["meta_info_file"]

base_dir = os.path.dirname(os.path.abspath(__file__))
common_dir = os.path.normpath(os.path.join(base_dir,"../../../../python-common/common/python"))
nomad_sim_dir = os.path.normpath(os.path.join(base_dir,"../../python-modules/"))
atomic_data_dir = os.path.normpath(os.path.join(base_dir, '../../../atomic-data')) 

if not common_dir in sys.path:
    sys.path.insert(0, common_dir) 
    sys.path.insert(0, nomad_sim_dir) 
    sys.path.insert(0, atomic_data_dir) 
    

def get_conventional_standard_atoms(atoms):
    """Given an ASE atoms object, return the ASE atoms object in the conventional standard cell"""
    
    mg_structure = AseAtomsAdaptor.get_structure(atoms)        
    finder = SpacegroupAnalyzer(mg_structure)
    
    mg_structure = finder.get_conventional_standard_structure()
    conventional_standard_atoms = AseAtomsAdaptor.get_atoms(mg_structure)
                
    return conventional_standard_atoms                


def get_primitive_standard_atoms(atoms):
    """Given an ASE atoms object, return the ASE atoms object in the primitive standard cell"""
    
    mg_structure = AseAtomsAdaptor.get_structure(atoms)        
    finder = SpacegroupAnalyzer(mg_structure)
    
    mg_structure = finder.get_primitive_standard_structure()
    primitive_standard_atoms = AseAtomsAdaptor.get_atoms(mg_structure)
                
    return primitive_standard_atoms 

def get_xrd_diffraction(atoms):
    """Given an ASE atoms object, return the 1D xray diffraction pattern as a function of the Bragg angle"""

    mg_structure = AseAtomsAdaptor.get_structure(atoms)        
    #c = XRDCalculator(wavelength=3.0)
    #c = XRDCalculator(wavelength="AgKb1")
    c = XRDCalculator()
    
    #c.show_xrd_plot(mg_structure)
    return c.get_xrd_plot(mg_structure), c.get_xrd_data(mg_structure)


def modify_crystal(structure, function_to_apply, **kwargs):

    if (structure.file_format=='NOMAD') or (structure.file_format=='xyz'):
        for (gIndexRun, gIndexDesc), atoms in structure.atoms.items():
            if atoms is not None:
                atoms = function_to_apply(atoms, **kwargs)
                structure.atoms[gIndexRun, gIndexDesc] = atoms

    return structure

def spacegroup_a_to_spacegroup_b(
    atoms, spgroup_a, spgroup_b, target_b_contribution,
    create_replicas_by, 
    min_nb_atoms=None, target_nb_atoms=None, max_diff_nb_atoms=None,
    radii=None, target_replicas=None, max_rel_error=0.01):
        
    """Remove central atoms for bcc to sc"""
                            
    if create_replicas_by == "nb_atoms":
        replicas =  nb_atoms_to_replicas(atoms, min_nb_atoms, target_nb_atoms, max_diff_nb_atoms)
    elif create_replicas_by == "radii":
        replicas = radius_to_replicas(atoms, min_nb_atoms, radii)
    elif create_replicas_by == "user-defined":
        replicas = target_replicas
    
#    # make a spgroup_a-type supercell before removing atoms
    atoms_a = atoms.copy()
#    print 'Spacegroup_a-type structure'
#    print atoms_a
#    for idx in range(atoms_a.get_number_of_atoms()):
#        print idx, atoms_a.positions[idx]
#
#    print '------------Spacegroup_a-type structure------------'
    
    atoms_a = atoms_a*replicas       

    # check initial spacegroup
    mg_structure = AseAtomsAdaptor.get_structure(atoms)        
    finder = SpacegroupAnalyzer(mg_structure)
    init_spgroup = finder.get_space_group_symbol()

    if init_spgroup == spgroup_a:    
        logger.debug('Initial spacegroup is {0} as expected'.format(init_spgroup))
    else:
        raise Exception("Initial spacegroup is {0} "
            "while the expected spacegroup is {1}".format(init_spgroup, spgroup_a))        
        
    # initially the mix structure has all the spgroup_a atoms
    atoms_mix = atoms_a.copy()


    idx_remove_list = []
    TOL=1e-03
    
    if (spgroup_a == 'Im-3m' and spgroup_b == 'Pm-3m'):
        # from bcc to simple cubic
        for idx in range(atoms.get_number_of_atoms()):
            # deleting all atoms from spgroup_a to go in spgroup_b
            # removing the atoms that are in position (0.0, 0.0, 0.0)
            if (abs(atoms.positions[idx][0]) <= TOL and
                abs(atoms.positions[idx][1]) <= TOL and 
                abs(atoms.positions[idx][2]) <= TOL):  
                pass
#                idx_remove_list.append(idx)
            else:
                idx_remove_list.append(idx)
#                pass 
            
    elif (spgroup_a == 'Fd-3m' and spgroup_b == 'Fm-3m'):
        # from diamond to fcc
        for idx in range(atoms.get_number_of_atoms()):
            # deleting all atoms from spgroup_a to go in spgroup_b
            # removing the atoms that are "inside" the cube
            # keep only the atoms that have one coordinate which is
            # 1/2 of the cell length or position (0.0, 0.0, 0.0)
            cell_length = atoms.get_cell_lengths_and_angles()[0]
            if (abs(atoms.positions[idx][0]-cell_length/2.0) <= TOL or 
                abs(atoms.positions[idx][1]-cell_length/2.0) <= TOL or 
                abs(atoms.positions[idx][2]-cell_length/2.0) <= TOL):                
                pass
            elif (abs(atoms.positions[idx][0]) <= TOL and
                abs(atoms.positions[idx][1]) <= TOL and 
                abs(atoms.positions[idx][2]) <= TOL):  
                pass
            else:
                idx_remove_list.append(idx)
    else:
        raise Exception("Transformation from spacegroup {0} to spacegroup {1}" 
            "is not implemented".format(spgroup_a, spgroup_b))

    # delete all the indeces added to the list        
    del atoms[[atom.index for atom in atoms if atom.index in idx_remove_list]]
    
    
#    print 'Spacegroup_b-type structure'
#    print atoms
#    for idx in range(atoms.get_number_of_atoms()):
#        print idx, atoms.positions[idx]
##
#    print '------------Spacegroup_b-type structure------------'
    
    # make spgroup_b-type supercell after atoms were removed
    atoms_b = atoms*replicas    
    
    # check final spacegroup
    mg_structure = AseAtomsAdaptor.get_structure(atoms_b)        
    finder = SpacegroupAnalyzer(mg_structure)
    final_spgroup = finder.get_space_group_symbol()

    if final_spgroup == spgroup_b:    
        logger.debug('Final spacegroup is {0} as expected'.format(final_spgroup))
    else:
        logger.debug("Final spacegroup is {0}".format(final_spgroup))
        logger.debug("Expected final spacegroup is {0}".format(spgroup_b))
        raise Exception("The transformation provided does not give the expected final "
            " spacegroup. Expected: {0}; obtained: {1}".format(spgroup_b, final_spgroup))
    

#    print 'Body centered cubic'
#
#    for idx in range(spgroup_a.get_number_of_atoms()):
#        print idx, spgroup_a.positions[idx]   
#
#    print '------------end Body centered cubic------------'

    # find the rows that are in bcc-type supercell and not in sc
    atoms_a_rows = atoms_a.positions.view([('', atoms_a.positions.dtype)] * atoms_a.positions.shape[1])
    atoms_b_rows = atoms_b.positions.view([('', atoms_b.positions.dtype)] * atoms_b.positions.shape[1])
    a_b_diff_pos = np.setdiff1d(atoms_a_rows, atoms_b_rows).view(atoms_a.positions.dtype).reshape(-1, atoms_a.positions.shape[1])
             
    print len(atoms_a_rows)
    print len(atoms_b_rows)
    print len(a_b_diff_pos)

    atoms_a_only_ids = []    
    for idx in range(atoms_a.get_number_of_atoms()):
        for row_idx in range(a_b_diff_pos.shape[0]):
            if np.allclose(atoms_a.positions[idx], a_b_diff_pos[row_idx, :], rtol=1e-03): 
                atoms_a_only_ids.append(idx)
                break
            else:
                pass
    
    print len(atoms_a_only_ids)
    # take a random subset of atoms to remove
    nb_atoms_to_rm = int(len(atoms_a_only_ids)*target_b_contribution)
    actual_b_contribution = nb_atoms_to_rm/len(atoms_a_only_ids)
        
    if target_b_contribution != 0.0:
        rel_error = abs(target_b_contribution - actual_b_contribution)/target_b_contribution
    
        if rel_error > max_rel_error:
            logger.warning("Difference between target and actual vacancy ratio "
                "bigger than the threshold ({0}%).\n"
                "Target/actual vacancy ratio: {1}%/{2}%."
                .format(max_rel_error*100.0, target_b_contribution*100.0, actual_b_contribution*100.0))
                    
    # random sampling of the list without replacement
    atoms_a_only_ids_subset = random.sample(atoms_a_only_ids, nb_atoms_to_rm)
    
    # remove atoms from the bcc_atoms_only_ids
    del atoms_mix[[atom.index for atom in atoms_mix if atom.index in atoms_a_only_ids_subset]]
    
#    print 'Intermediate bcc-sc'
#    for idx in range(atoms_bcc_sc.get_number_of_atoms()):
#        print idx, atoms_bcc_sc.positions[idx]   
            
#    print '------------end Intermediate bcc-sc------------'
    
    return atoms_mix
    

    

def create_supercell(atoms, replicas):
    """Create a supercell for each ASE atoms in the NOMADstructure instance"""
    # example: replicas=[3, 3, 3] replicates the system 3 times 
    atoms = atoms*replicas    
    logger.debug("Number of atoms in the current structure: {0}".format(atoms.get_number_of_atoms()))
#    print 'Spacegroup_a-type structure'
#    print atoms
#    for idx in range(atoms.get_number_of_atoms()):
#        print idx, atoms.positions[idx]
    return atoms


def nb_atoms_to_replicas(atoms, min_nb_atoms, target_nb_atoms, 
        silent=True, max_diff_nb_atoms=100):
    """Calculate how many replicas are closer to max_nb atoms.
    
    It approaches max_nb_atoms from below.
    Works only for having the same number of replicas in all directions."""
    
    nb_atoms = len(atoms)

    if nb_atoms < target_nb_atoms:
        replicas_x = int(np.cbrt(target_nb_atoms/nb_atoms))
        replicas = [replicas_x, replicas_x, replicas_x]
    else:
        replicas = [1, 1, 1]
    
    nb_atoms_replicas = nb_atoms*replicas[0]*replicas[1]*replicas[2]    
    
    diff_nb_atoms = target_nb_atoms - nb_atoms_replicas
    
    if not silent:
        if diff_nb_atoms > max_diff_nb_atoms:
            logger.info("Difference between target and actual nb_atoms in "
                "supercell generation is greater than threshold. \n"
                "Initial nb_atoms: {0}; target nb_atoms: {1}; actual nb_atoms: {2}; \n"
                "actual_nb_difference: {3}; max nb_difference: {4} \n"
                "replicas: {5}"
                .format(nb_atoms, target_nb_atoms, nb_atoms_replicas, 
                    diff_nb_atoms, max_diff_nb_atoms,
                    replicas))

    if nb_atoms_replicas < min_nb_atoms:
        raise ValueError("Structure has less than the required number of atoms. "
        "It has {0} atoms instead of {1} ".format(nb_atoms_replicas, min_nb_atoms))
        
    return replicas

    
def radius_to_replicas(atoms, min_nb_atoms, radius):
    """Calculate how many replicas are closer to the required radius.
    
    It approaches radii from below."""
    
    cell_lengths = atoms.get_cell_lengths_and_angles()[:3]
    replicas = [ int(radius_i // cell_length) for cell_length, radius_i in zip(cell_lengths, radius)]
    
    nb_atoms = len(atoms)    
    nb_atoms_replicas = nb_atoms*replicas[0]*replicas[1]*replicas[2]    

    if nb_atoms_replicas < min_nb_atoms:
        raise ValueError("Structure has less than the required number of atoms. "
        "It has {0} atoms instead of {1} ".format(nb_atoms_replicas, min_nb_atoms))
                
    return replicas
    
    
def create_supercell_by_nb_atoms(atoms, min_nb_atoms, target_nb_atoms, max_diff_nb_atoms=100):
    """Create a supercell for each ASE atoms in the NOMADstructure instance"""
    
    replicas = nb_atoms_to_replicas(atoms, min_nb_atoms, target_nb_atoms, max_diff_nb_atoms)
    atoms = atoms*replicas    

    return atoms


def create_supercell_by_radius(atoms, min_nb_atoms, radius):
    """Create a supercell for each ASE atoms in the NOMADstructure instance.
    
    Radii in angstrom"""
    
    replicas = radius_to_replicas(atoms, min_nb_atoms, radius)
    atoms = atoms*replicas    
    
    return atoms


def create_vacancies(atoms, target_vacancy_ratio, 
    create_replicas_by, 
    min_nb_atoms=None, target_nb_atoms=None, max_diff_nb_atoms=None,
    radii=None, target_replicas=None,
    max_rel_error=0.25):
    """Create vacancies for each ASE atoms in the NOMADstructure instance
    
    By defaults a 3x3 supercell is used to have enough atoms to create 
    vacancies in a sensible way.
    Note: it is implicetely without replacement because the atoms are deleted
    at each iteration. This is why the implementation is different w.r.t.
    substitute atoms.
    """

    if create_replicas_by == "nb_atoms":
        replicas =  nb_atoms_to_replicas(atoms, min_nb_atoms, target_nb_atoms, max_diff_nb_atoms)
    elif create_replicas_by == "radii":
        replicas = radius_to_replicas(atoms, min_nb_atoms, radii)
    elif create_replicas_by == "user-defined":
        replicas = target_replicas
        
    atoms = atoms * replicas
    
    if target_vacancy_ratio > 0:
        # calculate the number of vancancies to make given the ratio
        nb_atoms = len(atoms)   
        nb_vacancies = int(nb_atoms*target_vacancy_ratio)                        
        actual_vacancy_ratio = nb_vacancies/nb_atoms
        
        # randomly remove one atom from the supercell        
        for i in range(nb_vacancies):
            idx_atom_to_delete = random.choice([atom.index for atom in atoms])
            del atoms[idx_atom_to_delete]
        
        rel_error = abs(target_vacancy_ratio - actual_vacancy_ratio)/target_vacancy_ratio
        
        if rel_error > max_rel_error:
            logger.warning("Difference between target and actual vacancy ratio "
                "bigger than the threshold ({0}%).\n"
                "Target/actual vacancy ratio: {1}%/{2}%."
                .format(max_rel_error*100.0, target_vacancy_ratio*100.0, actual_vacancy_ratio*100.0))
    else:
        raise ValueError('The vacancy ratio needs to be positive.')

    return atoms

def substitute_atoms(atoms, 
    create_replicas_by, 
    min_nb_atoms=None, target_nb_atoms=None, max_diff_nb_atoms=None,
    radii=None, target_replicas=None,                     
    target_sub_ratio=0.0, max_n_sub_species=94,
    max_rel_error=0.25):
    """Substitute atoms for each ASE atoms in the NOMADstructure instance
    
    """
    
    if create_replicas_by == "nb_atoms":
        replicas =  nb_atoms_to_replicas(atoms, min_nb_atoms, target_nb_atoms, max_diff_nb_atoms)
    elif create_replicas_by == "radii":
        replicas = radius_to_replicas(atoms, min_nb_atoms, radii)
    elif create_replicas_by == "user-defined":
        replicas = target_replicas
        
    atoms = atoms * replicas
            
    if target_sub_ratio > 0:
        # calculate the number of atoms to substitute to make given the ratio
        nb_atoms = len(atoms)
        nb_subs = int(nb_atoms*target_sub_ratio)     
        actual_sub_ratio = nb_subs/nb_atoms
                   
        # pick atomic numbers from 1 to 94 
        possible_atom_numbers = np.random.choice(np.arange(1,95, dtype=np.int16), 
            size=max_n_sub_species)
        new_atomic_numbers = np.random.choice(possible_atom_numbers, 
            size=nb_subs)

        # without replacement
        idx_atom_to_change = np.random.choice(np.arange(0, len(atoms)), 
            size=nb_subs, replace=False)

        for i in range(nb_subs):
            atoms[idx_atom_to_change[i]].number = new_atomic_numbers[i]
            
        rel_error = abs(target_sub_ratio - actual_sub_ratio)/target_sub_ratio
        
        if rel_error > max_rel_error:
            logger.warning("Difference between target and actual substitution ratio "
                "bigger than the threshold ({0}%).\n"
                "Target/actual substitution ratio: {1}%/{2}%."
                .format(max_rel_error*100.0, target_sub_ratio*100.0, actual_sub_ratio*100.0))
        
    else:
        raise ValueError('The substitution ratio needs to be positive.')

    return atoms
    
    
def random_displace_atoms(atoms, 
    displacement,
    noise_distribution,
    create_replicas_by, 
    min_nb_atoms=None, target_nb_atoms=None, max_diff_nb_atoms=None,
    radii=None, target_replicas=None):
    """Random displace atoms for each ASE atoms in the NOMADstructure instance
    
    Gaussian random displacement. displacement is the standard deviation 
    of the distribution.    
    """

    if create_replicas_by == "nb_atoms":
        replicas =  nb_atoms_to_replicas(atoms, min_nb_atoms, target_nb_atoms, max_diff_nb_atoms)
    elif create_replicas_by == "radii":
        replicas = radius_to_replicas(atoms, min_nb_atoms, radii)
    elif create_replicas_by == "user-defined":
        replicas = target_replicas
        
    atoms = atoms * replicas
    
    if noise_distribution == "gaussian":
        noise = np.random.normal(loc=0.0, scale=displacement, size=(len(atoms), 3))
    elif noise_distribution == "uniform":
        noise = np.random.uniform(low=-displacement, high=displacement, size=(len(atoms), 3))
    elif noise_distribution == "gaussian_scaled":
        avg_nn_distance = get_avg_nn_distance(atoms)
        displacement = displacement * avg_nn_distance
        noise = np.random.normal(loc=0.0, scale=displacement, size=(len(atoms), 3))
    else:
        raise Exception("The noise distribution chosen is not implemented.")
#    logger.debug("Max displacement: {0}\n"
#        "Min displacement {1}\n"
#        "Min displacement (abs values) {2}\n"
#        "Mean displacement {3}\n"
#        "Mean displacement (abs values) {4}\n"
#        .format(np.amax(noise), np.amin(noise),
#            np.amin(np.abs(noise)), np.mean(noise),
#            np.mean(np.abs(noise))))
    
    atoms.set_positions(atoms.get_positions()+noise)

    # wrap atomic positions inside unit cell
    atoms.wrap()    

    return atoms
    


def grouped(iterable, n):
    "s -> (s0,s1,s2,...sn-1), (sn,sn+1,sn+2,...s2n-1), (s2n,s2n+1,s2n+2,...s3n-1), ..."
    return izip(*[iter(iterable)]*n)



def get_min_distance(atoms, nb_splits=100):
    """Calculate all the distances of atoms for a ASE atoms structure.
    
    This is memory intensive, so we need to split the distances if
    we run out of memory."""

    X = atoms.get_positions()
#    logger.debug("Number of atoms:", X.shape[0])
    num_elems = np.prod(X.shape)
    size_mb = (num_elems**2)*8.0/ (1024**2)
    #logger.debug("Matrix dimension: {0:.1f} MB".format(size_mb))    
    
    
    if size_mb < 1.0*1024:
        # less than 1Gb
        Y = scipy.spatial.distance.pdist(X, 'euclidean')
        Y_sort = np.sort(Y)
        shortest_distance = Y_sort[0] 
    else:
        # if the matrix is too big, use alternative strategy
#        logger.debug('Switching to low-memory requirement algorithm (slower).')
        nb_splits = int(size_mb//(1.0*1024))+1
        X_list = np.array_split(X, nb_splits)
        Y_sort = []
        for X_1, X_2 in grouped(X_list, 2):
            X = np.vstack((X_1, X_2))
            Y = scipy.spatial.distance.pdist(X, 'euclidean')
            Y_sort.append(np.sort(Y)[0])

        shortest_distance = min(Y_sort) 
                
    del Y


    ''' 
    #scikit-learn    
    Y2 = pairwise_distances(X, metric='euclidean')
    
    #ase 
    dist = atoms.get_all_distances()

    '''
    
    return shortest_distance
    
    

def get_avg_nn_distance(atoms, cutoff=12.0, nb_splits=100):
    """Calculate the average bondlengths of the nearest neighbors atoms fro a ASE atoms structure.

    """

    X = atoms.get_positions()
#    logger.debug("Number of atoms: {0}".format(X.shape))
    num_elems = np.prod(X.shape)
    size_mb = (num_elems**2)*8.0/ (1024**2)
    #logger.debug("Matrix dimension: {0:.1f} MB".format(size_mb))    
    
    num_atoms = atoms.get_number_of_atoms()
    
    # cutoffs for each atoms (in angstrom)
    cutoffs = np.ones(num_atoms)*cutoff
    nl = NeighborList(cutoffs, skin=0.1, self_interaction=False)
    nl.build(atoms)
    
    nn_dist = []
    
    for idx in range(num_atoms):
#        logger.debug("List of neighbors of atom number {0}".format(idx))
        indices, offsets = nl.get_neighbors(idx)
        
        coord_central_atom = atoms.positions[idx]

        # get positions of nearest neighbors within the cut-off
        dist_list = []
        for i, offset in zip(indices, offsets):
            # center each neighbors wrt the central atoms 
            coord_neighbor = atoms.positions[i] + np.dot(offset, atoms.get_cell())
            # calculate distance between the central atoms and the neighbors
            dist = np.linalg.norm(coord_neighbor-coord_central_atom)
            dist_list.append(dist)            
            
        # dist_list is the list of distances from the central_atoms
        if len(sorted(dist_list))>0:
            # get nearest neighbor distance
            nn_dist.append(sorted(dist_list)[0])
        else:
            raise Exception("List of neighbors is empty. Try to increase the cutoff.") 
        
#        logger.debug("-----------------------")

    # nn_dist contains the nearest neighbor distance for each atom
    avg_nn_distance = np.mean(nn_dist)        
    min_nn_distance = np.amin(nn_dist)        

#    if abs(avg_nn_distance -min_nn_distance) > 1e-03:
#        print avg_nn_distance, min_nn_distance
#    print avg_nn_distance        
#    print min_nn_distance

    
    return avg_nn_distance
    
    

def scale_structure(atoms, scale_factor=1.0):
    """Scale the atomic structure by a given number.
    
    Isotropic scaling."""

    atoms.set_positions(atoms.get_positions()*scale_factor)

    return atoms


def get_spacegroup(structure, materials_class = None):
    '''Get spacegroup from a list of NOMAD structure.
    (one json file). Supports multiple frames (TO DO: check that). 

    '''
    energy_total = {}
    chemical_formula = {}
    spacegroup_number = {}

    #print structure.atoms
    #print type(structure.atoms)

    #print type(structure.energy_eV)    
    #print structure.energy_eV    
    #print structure.chemical_formula    
    
    #print type(structure.atoms)
    #print structure.atoms
    
    #print structure.name

    #gIndexRun=0
    #gIndexDesc=1
    #print structure.energy_eV[(gIndexRun,gIndexDesc)]

    for (gIndexRun, gIndexDesc), atoms in structure.atoms.iteritems():
        if atoms is not None:

            if materials_class == 'binaries':
                # Get chemical formula with two elements end total energy per binary.
                chemical_formula[gIndexRun,gIndexDesc] = get_chemical_formula_binaries(atoms)
                energy_total[gIndexRun,gIndexDesc] = 2*structure.energy_total[(gIndexRun,gIndexDesc)]/len(atoms)
            else:
                chemical_formula[gIndexRun,gIndexDesc] = structure.chemical_formula[(gIndexRun,gIndexDesc)]
                energy_total[gIndexRun,gIndexDesc] = structure.energy_total[(gIndexRun,gIndexDesc)]

            spacegroup_number[gIndexRun,gIndexDesc] = structure.spacegroup_analyzer[gIndexRun, gIndexDesc].get_space_group_number()                   

            break
                
    return chemical_formula[0, 0], energy_total[0, 0], spacegroup_number[0, 0]

def get_lattice_type(structure):
    '''Get lattice_type from a list of NOMAD structure.
    (one json file). Supports multiple frames (TO DO: check that). 

    '''
    energy_total = {}
    chemical_formula = {}
    lattice_type = {}

    #print structure.atoms
    #print type(structure.atoms)

    #print type(structure.energy_eV)    
    #print structure.energy_eV    
    #print structure.chemical_formula    
    
    #print type(structure.atoms)
    #print structure.atoms
    
    #print structure.name

    #gIndexRun=0
    #gIndexDesc=1
    #print structure.energy_eV[(gIndexRun,gIndexDesc)]

    for (gIndexRun, gIndexDesc), atoms in structure.atoms.iteritems():
        if atoms is not None:
            energy_total[gIndexRun,gIndexDesc] = structure.energy_total[(gIndexRun,gIndexDesc)]
            chemical_formula[gIndexRun,gIndexDesc] = structure.chemical_formula[(gIndexRun,gIndexDesc)]

            lattice_type[gIndexRun,gIndexDesc] = structure.spacegroup_analyzer[gIndexRun, gIndexDesc].get_lattice_type()                                    
            break
                
    return chemical_formula[0, 0], energy_total[0, 0], lattice_type[0, 0]
    
    

def get_target_diff_dic(df, sample_key=None, energy=None, spacegroup=None):
    """ Get a dictionary of dictionaries: samples -> space group tuples -> energy differences.

    Dropping all rows which do not correspond to the minimum energy per sample AND space group,
    then making a new data frame with space groups as columns. Finally constructing the dictionary
    of dictionaries.

    Parameters
    ----------
    df : pandas data frame 
        with columns=[samples_title, energies_title, SG_title]
    
    sample_key : string
        Needs to be column title of samples of input df

    energy : string
        Needs to be column title of energies of input df

    spacegroup : string
        Needs to be column title of space groups of input df

    Returns
    -------
    dic_out : dictionary of dictionaries:
        In the form: 
        { 
        sample_a: { (SG_1,SG_2):E_diff_a12, (SG_1,SG_3):E_diff_a13,...},  
        sample_b: { (SG_1,SG_2):E_diff_b12, (SG_1,SG_3):E_diff_b13,... },
        ...
        }
        E_diff_a12 = energy_SG_1 - energy_SG_2   of sample a.
        Both (SG_1,SG_2) and (SG_2,SG_1) are considered.
        If SG_1 or SG_2 is NaN, energy difference to it is ignored.


    """


    # use only rows with minimum energies
    idx = df.groupby([sample_key, spacegroup])[energy].transform(min) == df[energy]
    df = df[idx]
    df = df.drop_duplicates()

    # make new table with the different supgroups as columns
    df = df.pivot_table(energy, [sample_key], spacegroup)

    # make dictionary of dictionaries
    SG_list = df.columns.values
    Samples_list = df.index.values
    matrix = np.array(df)
    dic_out = dict.fromkeys(Samples_list)
    for i, sample in enumerate(Samples_list):
        row = matrix[i]
        not_nan_indices = np.argwhere(~np.isnan(row)).flatten()
        sample_dic = {}
        for j_1,j_2 in permutations(not_nan_indices, 2):
                SG_1,SG_2 = SG_list[j_1],SG_list[j_2]
                Energy_diff = row[j_1] - row[j_2]
                sample_dic.update({ (SG_1,SG_2) : Energy_diff })
        if sample_dic:
            dic_out[sample] = sample_dic
    return dic_out



def select_diff_from_dic(dic, spacegroup_tuples, sample_key='Mat', drop_nan=None):
    """ Get data frame of selected spacegroup_tuples from dictionary of dictionaries.

        Creating a pandas data frame with columns of samples and selected space group tuples (energy differnces).

        Parameters
        ----------

        dic : dict {samples -> space group tuples -> energy differences.}

        spacegroup_tuples : tuple, list of tuples, tuples of tuples
            Each tuple has to contain two space groups numbers, 
            to be looked up in the input dic.
        
        sample_key : string
            Will be the column title of the samples of the created data frame

        drop_nan : string, optional {'rows', 'SG_tuples'}
            Drops all rows or columns (SG_tuples) containing NaN.

    """


    if isinstance(spacegroup_tuples, tuple) and all(isinstance(item, (float,int)) for item in spacegroup_tuples):
        spacegroup_tuples = [spacegroup_tuples]
    df_out = pd.DataFrame(dic, index=spacegroup_tuples).T
    
    if not drop_nan==None:
        if drop_nan == 'rows': 
            df_out.dropna(axis=0, inplace=True)
        elif drop_nan == 'SG_tuples':
            df_out.dropna(axis=1, inplace=True)
        else:
            raise ValueError("Argument 'drop_nan' has to be 'None', 'rows' or 'SG_tuples'.")
    
    # check if df_out is empty
    len_columns = len(df_out.columns)
    len_rows = len(df_out.index)
    if len_columns == 0 or len_rows == 0:
        if len_rows == 0:
            string = 'rows'
        else:
            string = 'spacegroup_tuples'
        logger.error('Dropping {0} with NaNs leads to empty data frame.'.format(string))
        logger.error('Hint: Select different spacegroup_tuples or set drop_nan=None')
        sys.exit(1)
    df_out.reset_index(inplace=True)
    df_out.rename(columns = {'index': sample_key}, inplace=True)
    return df_out



def convert_energy_substance(unit, value, 
    ureg=None, energy_unit=None, length_unit=None):
    "Convert energy or energy/substance"

    unit_def = ureg(unit)    

    energy_unit_def = ureg(energy_unit)
    #print energy_unit_def.dimensionality

    if unit_def.dimensionality == '[length] ** 2 * [mass] / [time] ** 2':
        if energy_unit_def.dimensionality == '[length] ** 2 * [mass] / [substance] / [time] ** 2':
            # kj/mol and kcal/mol includes also [substance]
            # divide value by mol (multiply by Avogadro number)
            if isinstance(value, list):
                value = [x*constants.Avogadro for x in value]
            else:
                value = value*constants.Avogadro

            unit = unit+'/mol'
            value = uc.convert_unit(value, unit, target_unit=energy_unit)
            #print feature, value, self.energy_unit
        else:
            # already an actual energy
            value = uc.convert_unit(value, unit, target_unit=energy_unit)
            #print feature, value, self.energy_unit                                
    elif unit_def.dimensionality == '[length]':
        value = uc.convert_unit(value, unit, target_unit=length_unit)
        #print feature, value, self.length_unit
    else:
        # keep original units (or no units, e.g. Zvalence)
        pass
        #raise ValueError("Unit needs to be [energy], [energy]/[substance] or [length]")

    return value


def format_e(n):
    a = '{0:.3E}'.format(Decimal(n))
#    return a.split('E')[0].rstrip('0').rstrip('.') + 'E' + a.split('E')[1]
    return a
    
    
def filter_json_list(
        json_list=None,
        file_format='NOMAD',
        desc_folder=None,
        tmp_folder=None,
        take_first=True,
        target_list=None,
        filter_by=None,
        accepted_labels=None,
        cell_type=None,
        operations_on_structure=None,
        write_to_file=None,
        filtered_file=None,
        **kwargs):
    """ Filter the json_list for the descriptor according to a property value.
        Works only for single frame.
    """
    
    filtered_file = os.path.abspath(os.path.normpath(os.path.join(desc_folder, filtered_file))) 
    
    labels = {}
    labels_filtered = {}
    filtered_json_list = []
    
    # list of the spacegroups (independent from the fact that spacegroup is
    # in the filter)
    spacegroup_list = []
    # create empty list for each filter_by value to append values later and check
    # which unique values are selected
    for filter_ in filter_by:
        labels_filtered.update({str(filter_): []})

    for idx, json_file in enumerate(json_list):
        # read structure
        if file_format == 'NOMAD':
            structure = nomad_sim.nomad_structures.NOMADStructure(
                in_file=json_file,
                frame_list=None,
                file_format=file_format,
                take_first=take_first,
                cell_type=cell_type)

            labels.update({'json_file': json_file})

            if filter_by is not None:                                
                # works only for first frame
                gIndexRun=0
                gIndexDesc=0

                spacegroup_symbol = structure.spacegroup_analyzer[gIndexRun, gIndexDesc].get_space_group_symbol()                    
                spacegroup_number = structure.spacegroup_analyzer[gIndexRun, gIndexDesc].get_space_group_number()                    
                pointgroup_symbol = structure.spacegroup_analyzer[gIndexRun, gIndexDesc].get_point_group_symbol()                    
                crystal_system = structure.spacegroup_analyzer[gIndexRun, gIndexDesc].get_crystal_system() 
                # Get the lattice for the structure, e.g., (triclinic, orthorhombic, cubic, etc.).
                #This is the same than the crystal system with the exception of the
                #hexagonal/rhombohedral lattice
                lattice_type = structure.spacegroup_analyzer[gIndexRun, gIndexDesc].get_lattice_type() 
                hall_symbol = structure.spacegroup_analyzer[gIndexRun, gIndexDesc].get_hall()                    
                lattice_centering = structure.spacegroup_analyzer[gIndexRun, gIndexDesc].get_space_group_symbol()[0]                    
                
                # add all known properties to labels
                labels.update({'spacegroup_symbol': spacegroup_symbol})
                labels.update({'spacegroup_number': spacegroup_number})
                labels.update({'pointgroup_symbol': pointgroup_symbol})                
                labels.update({'crystal_system': crystal_system})
                labels.update({'lattice_type': lattice_type})
                labels.update({'hall_symbol': hall_symbol})
                labels.update({'lattice_centering': lattice_centering})

                filter_count = 0
                for id_filter, filter_ in enumerate(filter_by):
                    # look if the property which we want to filter by 
                    # is included in the labels dictionary with the
                    # correct value
                    if labels[filter_] not in accepted_labels[id_filter]:
                        break
                    else:
                        labels_filtered[str(filter_)].append(labels[filter_])
                        spacegroup_list.append(spacegroup_symbol)
                        filter_count += 1
                
                # append to the filtered json only if everything is matching 
                if filter_count == len(filter_by):
                    filtered_json_list.append(json_file)
                
            if (idx % (int(len(json_list) / 10) + 1) == 0):
                logger.info(
                    "Reading: file {0}/{1} "
                    "to filter the json list".format(idx + 1, len(json_list)))
    
    
    for filter_ in filter_by:
        logger.info("List of unique values for filter '{0}': "
            "{1}".format(str(filter_), list(set(labels_filtered[str(filter_)]))))
            
    logger.info("List of 'accepted labels': "
        "{0}".format(accepted_labels))
        
    logger.info("Length of the whole json list: "
        "{0}".format(len(json_list)))

    logger.info("Length of the filtered json list: "
        "{0}".format(len(filtered_json_list)))                
        
    if write_to_file:
        logger.info("Writing filtered json list to file: "
            "{0}".format(filtered_file))       
            
        results={
            "filtered_json_list": filtered_json_list,
            "spacegroup_list": spacegroup_list,
            "unique_spacegroup_list": list(set(spacegroup_list)),
            "accepted_labels": accepted_labels,
            "values_filter": labels_filtered,
            "filter_by": filter_by,
        }
                    
        with open(filtered_file, "w") as f:
            f.write("""
    {
          "data":[""")
    
            writeColon = False

            if (writeColon):
                f.write(", ")
            writeColon = True
            json.dump(results, f, indent=2)
            f.write("""
    ] }""")
            f.flush()
            
    
    return filtered_json_list


