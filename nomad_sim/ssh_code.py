import paramiko
import os
from stat import S_ISDIR

class SSH(object):
    def __init__(self,hostname='172.17.0.3',username='tutorial', port=22, key_file="/home/beaker/docker.openmpi/ssh/id_rsa.mpi",password=None):
        if password==None:
            pkey_path=key_file
            key=paramiko.RSAKey.from_private_key_file(pkey_path)
            self.ssh = paramiko.SSHClient()
            self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            self.ssh.connect(hostname, username=username, port=port, pkey=key)
        else:
            self.ssh = paramiko.SSHClient()
            self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            self.ssh.connect(hostname, username=username, port=port, password=password)
        self.sftp = self.ssh.open_sftp()
    def close(self):
        self.sftp.close()
    def command(self,cmd):
        stdin, stdout, stderr = self.ssh.exec_command(cmd)
        return stdout.read()
    def mkdir(self,path):
        try: 
            self.sftp.mkdir(path)
        except: 
            pass
    def remove(self,path):
        if self.isdir(path):
            files = self.sftp.listdir(path=path)
            for f in files:
                filepath = os.path.join(path, f)
                if self.isdir(filepath):
                    self.rm(filepath)
                else:
                    self.sftp.remove(filepath)
            self.sftp.rmdir(path)
        else:
            self.sftp.remove(path)
    def rm(self,path):
        try:
            self.remove(path)
        except:
            pass
    def exists(self,path):
        try:
            self.sftp.stat(path)
            return True
        except:
            return False
    def isdir(self,path):
        try:
            return S_ISDIR(self.sftp.stat(path).st_mode)
        except IOError:
            return False

    def open_file(self,filename):
        return self.sftp.open(filename)

    def rename(self,remotefile_1,remotefile_2):
        self.sftp.rename(remotefile_1,remotefile_2)

    def put(self,localfile,remotefile):
        self.sftp.put(localfile,remotefile)

    def put_all(self,localpath,remotepath):
        #  recursively upload a full directory
        os.chdir(os.path.split(localpath)[0])
        parent=os.path.split(localpath)[1]
        for walker in os.walk(parent):
            try:
                self.sftp.mkdir(os.path.join(remotepath,walker[0]))
            except:
                pass
            for f in walker[2]:
                self.put(os.path.join(walker[0],f),os.path.join(remotepath,walker[0],f))

    def get(self,remotefile,localfile):
        #  Copy remotefile to localfile, overwriting or creating as needed.
        self.sftp.get(remotefile,localfile)
    def sftp_walk(self,remotepath):
        # Kindof a stripped down  version of os.walk, implemented for 
        # sftp.  Tried running it flat without the yields, but it really
        # chokes on big directories.
        path=remotepath
        files=[]
        folders=[]
        for f in self.sftp.listdir_attr(remotepath):
            if S_ISDIR(f.st_mode):
                folders.append(f.filename)
            else:
                files.append(f.filename)
        yield path,folders,files
        for folder in folders:
            new_path=os.path.join(remotepath,folder)
            for x in self.sftp_walk(new_path):
                yield x

    def get_all(self,remotepath,localpath):
        #  recursively download a full directory
        #  Harder than it sounded at first, since paramiko won't walk
        #
        # For the record, something like this would gennerally be faster:
        # ssh user@host 'tar -cz /source/folder' | tar -xz

        self.sftp.chdir(os.path.split(remotepath)[0])
        parent=os.path.split(remotepath)[1]
        try:
            os.mkdir(localpath)
        except:
            pass
        for walker in self.sftp_walk(parent):
            try:
                os.mkdir(os.path.join(localpath,walker[0]))
            except:
                pass
            for file in walker[2]:
                self.get(os.path.join(walker[0],file),os.path.join(localpath,walker[0],file))

