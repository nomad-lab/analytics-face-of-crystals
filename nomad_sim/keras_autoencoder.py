# -*- coding: utf-8 -*-

__author__ = "Angelo Ziletti"
__copyright__ = "Copyright 2016, The NOMAD Project"
__maintainer__ = "Angelo Ziletti"
__email__ = "ziletti@fhi-berlin.mpg.de"
__date__ = "21/10/16"


""" Based on the Keras tutorial:
https://blog.keras.io/building-autoencoders-in-keras.html"""

from keras.layers import Input, Dense, Convolution2D, MaxPooling2D, UpSampling2D
from keras.models import Model
from keras.callbacks import TensorBoard
import matplotlib.pyplot as plt
import numpy as np
from keras.datasets import mnist
import logging
from PIL import Image

from nomad_sim.config import configs

try:
    hdlr = logging.FileHandler(configs["output_file"], mode='a')
except:
    hdlr = logging.FileHandler(configs["output_file"], mode='w')

level = logging.getLevelName(configs["log_level_general"])

logger = logging.getLogger(__name__)
logger.setLevel(level)
logging.basicConfig(level=level)
FORMAT = "%(levelname)s: %(message)s"
formatter = logging.Formatter(fmt=FORMAT)
handler = logging.StreamHandler()
handler.setFormatter(formatter)
hdlr.setFormatter(formatter)
logger.addHandler(handler)
logger.addHandler(hdlr)
logger.setLevel(level)
logger.propagate = False

__metainfopath__ = configs["meta_info_file"]


def run_keras_autoencoder(data_set, filename_img_list, filename_npy_list, filename_rs_list, 
        n_classes, n_rows, n_columns, batch_size=100, train=True,
        checkpoint_dir='./'):
                    
    input_img = Input(shape=(n_rows, n_columns, 1))
    

    
    # encoding layers
    # Note:
    #     #dim_ordering: 'th' or 'tf'. 
    #   in 'th' mode, the channels dimension (the depth) is at index 1
    #   in 'tf' mode is it at index 3

    logger.debug('Encoder')
    x = Convolution2D(16, 3, 3, activation='relu', border_mode='same', dim_ordering='tf')(input_img)
    logger.debug(x)
    x = MaxPooling2D((2, 2), border_mode='same')(x)
    logger.debug(x)
    x = Convolution2D(8, 3, 3, activation='relu', border_mode='same', dim_ordering='tf')(x)
    logger.debug(x)
    x = MaxPooling2D((2, 2), border_mode='same')(x)
    logger.debug(x)
    x = Convolution2D(4, 3, 3, activation='relu', border_mode='same', dim_ordering='tf')(x)
    logger.debug(x)
    x = Convolution2D(1, 3, 3, activation='relu', border_mode='same', dim_ordering='tf')(x)
    encoded = MaxPooling2D((2, 2), border_mode='same', dim_ordering='tf')(x)
    logger.debug(encoded)

    encoder = Model(input_img, encoded)

    logger.debug('----------------------')

    
    # at this point the representation is (4, 4, 1) i.e. 128-dimensional
    
    # decoding layers
    logger.debug('Decoder')
    x = Convolution2D(1, 3, 3, activation='relu', border_mode='same', dim_ordering='tf')(encoded)
    logger.debug(x)
    x = UpSampling2D((2, 2))(x)
    logger.debug(x)
    x = Convolution2D(4, 3, 3, activation='relu', border_mode='same', dim_ordering='tf')(encoded)
    logger.debug(x)
    x = UpSampling2D((2, 2))(x)
    logger.debug(x)
    x = Convolution2D(8, 3, 3, activation='relu', border_mode='same', dim_ordering='tf')(x)
    logger.debug(x)
    x = UpSampling2D((2, 2))(x)
    logger.debug(x)
    x = Convolution2D(16, 3, 3, activation='relu', dim_ordering='tf')(x)
    logger.debug(x)
    x = UpSampling2D((2, 2))(x)
    logger.debug(x)
    decoded = Convolution2D(1, 3, 3, activation='sigmoid', border_mode='same', dim_ordering='tf')(x)
    logger.debug(decoded)
    
    autoencoder = Model(input_img, decoded)
    # binary cross entropy is good for binary images like mnist dataset or diffraction
    # patterns. Mean squared error gives very bad results
    autoencoder.compile(optimizer='adadelta', loss='binary_crossentropy')

    x_train = data_set.train.images
    x_test = data_set.test.images

    autoencoder.fit(x_train, x_train,
                    nb_epoch=50,
                    batch_size=256,
                    shuffle=True,
                    validation_data=(x_test, x_test),
                    callbacks=[TensorBoard(log_dir='/tmp/autoencoder')])

    encoded_imgs_train = encoder.predict(x_train)                    
    decoded_imgs_train = autoencoder.predict(x_train)
    
    encoded_imgs_test = encoder.predict(x_test)                    
    decoded_imgs_test = autoencoder.predict(x_test)

    encoded_imgs = np.append(encoded_imgs_train, encoded_imgs_test, axis=0)
    decoded_imgs = np.append(decoded_imgs_train, decoded_imgs_test, axis=0)

    logger.debug('Shape encoded train image matrix: {0}'.format(encoded_imgs_train.shape))
    logger.debug('Shape encoded test image matrix: {0}'.format(encoded_imgs_test.shape))
    logger.debug('Shape encoded image matrix: {0}'.format(encoded_imgs.shape))

    logger.debug('Shape decoded train image matrix: {0}'.format(decoded_imgs_train.shape))
    logger.debug('Shape decoded test image matrix: {0}'.format(decoded_imgs_test.shape))
    logger.debug('Shape decoded image matrix: {0}'.format(decoded_imgs.shape))


               

    '''                            img = Image.fromarray(rs8)
                            img.save(filename_rs)

                            img = Image.fromarray(ph8)
                            img.save(filename_ph)
    '''                            
                            
    # make sure list lengths are consistent
    assert len(filename_img_list) == len(encoded_imgs), "Descriptor list and encoded images do not have same length"
    assert len(filename_img_list) == len(decoded_imgs), "Descriptor list and encoded images do not have same length"
    

    for i in range(len(encoded_imgs)):
        
        encoded_img_i8 = (((encoded_imgs[i] - encoded_imgs[i].min()) / (encoded_imgs[i].max() - encoded_imgs[i].min())) * 255.0).astype(np.uint8)
        encoded_img_i8 = encoded_img_i8.reshape(encoded_img_i8.shape[0], encoded_img_i8.shape[1])

        img = Image.fromarray(encoded_img_i8)
        img.save(filename_img_list[i], overwrite=True)
        
        np.save(filename_npy_list[i], encoded_img_i8)


    '''        

    n = 10

    for j in range(3):
        plt.figure(figsize=(20, 4))
        for i in range(n):
            # display original
            ax = plt.subplot(2, n, i+1)
            plt.imshow(x_test[j*n+i].reshape(n_rows, n_columns))
            plt.gray()
            ax.get_xaxis().set_visible(False)
            ax.get_yaxis().set_visible(False)
        
            # display reconstruction
            ax = plt.subplot(2, n, n + i +1)
            plt.imshow(decoded_imgs_test[j*n+i].reshape(n_rows, n_columns))
            plt.gray()
            ax.get_xaxis().set_visible(False)
            ax.get_yaxis().set_visible(False)
        #plt.show()

    
    
    n = 10
    plt.figure(figsize=(20, 8))
    for i in range(n):
        ax = plt.subplot(1, n, i+1)
        plt.imshow(encoded_imgs_test[i].reshape(7, 7 * 1).T)
        plt.gray()
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)
    #plt.show()    
    '''
    
    
    