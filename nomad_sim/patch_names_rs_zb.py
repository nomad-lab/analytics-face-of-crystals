# -*- coding: utf-8 -*-
"""
Created on Tue Aug  2 15:39:12 2016

@author: ziletti
"""

def rename_material(chem_f):
    
    for idx, el in enumerate(chem_f):
        if el=='FLi':
            chem_f[idx] = 'LiF'
        if el=='ClLi':
            chem_f[idx] = 'LiCl'
        if el=='BrLi':
            chem_f[idx] = 'LiBr'
        if el=='ILi':
            chem_f[idx] = 'LiI'
        if el=='AsB':
            chem_f[idx] = 'BAs'
        if el=='FNa':
            chem_f[idx] = 'NaF'
        if el=='ClNa':
            chem_f[idx] = 'NaCl'
        if el=='BrNa':
            chem_f[idx] = 'NaBr'
        if el=='INa':
            chem_f[idx] = 'NaI'
        if el=='CSi':
            chem_f[idx] = 'SiC'
        if el=='FK':
            chem_f[idx] = 'KF'
        if el=='ClK':
            chem_f[idx] = 'KCl'
        if el=='BrK':
            chem_f[idx] = 'KBr'
        if el=='IK':
            chem_f[idx] = 'KI'
        if el=='ClCu':
            chem_f[idx] = 'CuCl'
        if el=='BrCu':
            chem_f[idx] = 'CuBr'
        if el=='OZn':
            chem_f[idx] = 'ZnO'
        if el=='SZn':
            chem_f[idx] = 'ZnS'
        if el=='SeZn':
            chem_f[idx] = 'ZnSe'
        if el=='TeZn':
            chem_f[idx] = 'ZnTe'
        if el=='AsGe':
            chem_f[idx] = 'GeAs'
        if el=='FRb':
            chem_f[idx] = 'RbF'
        if el=='ClRb':
            chem_f[idx] = 'RbCl'
        if el=='BrRb':
            chem_f[idx] = 'RbBr'
        if el=='IRb':
            chem_f[idx] = 'RbI'
        if el=='OSr':
            chem_f[idx] = 'SrO'
        if el=='SSr':
            chem_f[idx] = 'SrS'
        if el=='SeSr':
            chem_f[idx] = 'SrSe'
        if el=='AsIn':
            chem_f[idx] = 'InAs'
        if el=='ClCs':
            chem_f[idx] = 'CsCl'
        if el=='BrCs':
            chem_f[idx] = 'CsBr'
        if el=='SiSn':
            chem_f[idx] = 'SnSi'
        if el=='CGe':
            chem_f[idx] = 'GeC'
        if el=='GeSn':
            chem_f[idx] = 'SnGe'
        if el=='CSn':
            chem_f[idx] = 'SnC'

    return chem_f

'''            
    ['SiSn', 'BrLi', 'BrRb', 'ClCs', 'BSb', 'ILi', 'CaS',
    'SrTe', 'CaO', 'BaTe', 'OSr', 'GaN', 'CaTe', 'AsIn', 
    'AgCl', 'BrK', 'FLi', 'AgI', 'ClCu', 'AgBr', 'BrNa', 
    'AsGa', 'CuF', 'BeSe', 'CuI', 'BaSe', 'FNa', 'BrCs', 
    'AsB', 'BeS', 'ClNa', 'CSn', 'INa', 'GeSn', 'BeO', 
    'CdSe', 'CaSe', 'SeSr', 'ClK', 'Ge2', 'CdO', 'TeZn',
    'MgO', 'BeTe', 'ClLi', 'BN', 'SZn', 'C2', 'IRb', 'CSi', 
    'GaP', 'OZn', 'BaS', 'Sn2', 'AlAs', 'MgS', 'BrCu', 'AgF', 
    'GaSb', 'ClRb', 'SeZn', 'CsF', 'BaO', 'CdS', 'MgTe', 'BP', 
    'AlSb', 'SSr', 'FRb', 'InP', 'InN', 'InSb', 'CGe', 'GeSi', 
    'CsI', 'Si2', 'AlN', 'MgSe', 'FK', 'IK', 'CdTe', 'AlP']
''' 
    
