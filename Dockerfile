FROM jupyter/minimal-notebook:latest
LABEL maintainer="Adam Fekete <adam.fekete@kcl.ac.uk>"

USER root
RUN apt-get update \
 && apt-get install -y -q --no-install-recommends \
        gcc \
        gfortran \
        openmpi-bin \
        libopenmpi-dev \
        liblapack-dev \
        libblas-dev \
        libnetcdf-dev \
        netcdf-bin \
        libxpm-dev \
        libgsl-dev \
        vim \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*
USER $NB_UID

# sudo apt-get install libcunit1 libcunit1-doc libcunit1-dev
# sudo apt-get install fftw3 fftw3-dev pkg-config

# Initialising conda framwork
RUN conda init

# ================================================================================
# Python 2.x environment
# ================================================================================
# Create a Python 2.x environment using conda including at least the ipython kernel
# and the kernda utility. Add any additional packages you want available for use
# in a Python 2 notebook to the first line here (e.g., pandas, matplotlib, etc.)
RUN . $CONDA_DIR/etc/profile.d/conda.sh \
 && conda  create --name python2  -c conda-forge  --quiet  --yes \
    'python=2.7' \
    'conda-forge::blas=*=openblas' \
    'ipython' \
    'ipykernel' \
    'kernda' \
    'numpy' \
    'pandas' \
    'scipy' \
    'scikit-learn>=0.17.1' \
    'pytorch::pytorch-cpu' \
    'pytorch::torchvision-cpu' \
    'networkx' \
    'matplotlib' \
    'nglview' \
    'ipywidgets' \
    'cython' \
    'numba' \
    'ase' \
    'asap3' \
    'hdf5' \
    'h5py' \
    'tqdm' \
    'matsci::pymatgen==2017.11.9' \
#    #'matsci::pymatgen' \
    'pint==0.7.2' \
    'future' \
    'mdtraj==1.7.2' \
    'bokeh==0.11.1' \
    'enum34' \
    'keras' \
    'pillow' \
    'sqlalchemy' \
    'lmmentel::mendeleev' \
    'paramiko' \
    'plotly' \
    'lxml' \
    'networkx==2.2' \
 && conda activate python2 \
 && pip install keras-tqdm pyquaternion \
 && conda remove --quiet --yes --force qt pyqt \
 && conda clean -tipsy \
 && fix-permissions $CONDA_DIR \
 && fix-permissions /home/$NB_USER

# Create a global kernelspec in the image and modify it so that it properly activates
# the python2 conda environment.
USER root
RUN . $CONDA_DIR/etc/profile.d/conda.sh \
 && conda activate python2 \
 && ipython kernel install --name python2 --display-name 'Python 2'
USER $NB_USER

## Import matplotlib the first time to build the font cache.
#ENV XDG_CACHE_HOME=/home/$NB_USER/.cache/
#RUN MPLBACKEND=Agg python -c "import matplotlib.pyplot" \
# && fix-permissions /home/$NB_USER

# ================================================================================
# nomad_sim + nomadcore
# ================================================================================

WORKDIR /opt/nomad_pks
COPY 3rdparty/nomad_pks .

USER root
RUN chown -R $NB_USER:$NB_GID $PWD
USER $NB_USER

WORKDIR /opt/nomad_pks/nomadcore_pks
RUN . $CONDA_DIR/etc/profile.d/conda.sh \
 && conda activate python2 \
 && python setup.py install

WORKDIR /opt/nomad_pks/nomad_sim_pks
RUN . $CONDA_DIR/etc/profile.d/conda.sh \
 && conda activate python2 \
 && python setup.py install

# ================================================================================
#  QUIP + GAP + quippy
# ================================================================================

# All the QUIPs go here; added to path in the end.
WORKDIR /opt/quip

# QUIP for general use is the OpenMP version.
ENV QUIP_ARCH linux_x86_64_gfortran_openmp

ENV QUIP_INSTALLDIR /opt/quip/bin
ENV PATH $QUIP_INSTALLDIR:$PATH

COPY 3rdparty/QUIP .
COPY 3rdparty/GAP src/GAP
COPY 3rdparty/GAP-filler src/GAP-filler
COPY Makefile.inc build/$QUIP_ARCH/
COPY GIT_VERSION .
COPY GAP_VERSION src/GAP/

USER root
RUN chown -R $NB_USER:$NB_GID $PWD
USER $NB_USER

# Installs with no suffix, e.g. quip
RUN make > /dev/null 2>&1 \
 && make install \
 # Installs quippy using python2 environment
 && . $CONDA_DIR/etc/profile.d/conda.sh \
 && conda activate python2 \
 && make install-quippy > /dev/null 2>&1

# ================================================================================
# Python 3.x environment
# ================================================================================

# Add any additional packages you want available for use in a Python 3 notebook
# to the first line here (e.g., pandas, matplotlib, etc.)
# Remove pyqt and qt pulled in for matplotlib since we're only ever going to
# use notebook-friendly backends in these images
RUN conda install --quiet --yes \
    'conda-forge::blas=*=openblas' \
    'numpy' \
    'pandas' \
    'scipy' \
    'scikit-learn' \
    'pytorch::pytorch-cpu' \
    'pytorch::torchvision-cpu' \
    'bokeh' \
    'matplotlib' \
    'ipywidgets' \
    'nglview' \
    'cython' \
    'numba' \
    'ase' \
    'tqdm' \
    'tensorflow' \
    'keras'  \
 && pip install --quiet git+https://github.com/raghakot/keras-vis.git \
 && conda remove --quiet --yes --force qt pyqt \
 && conda clean -tipsy \
 # Activate ipywidgets extension in the environment that runs the notebook server
 && jupyter nbextension enable --py widgetsnbextension --sys-prefix \
 && npm cache clean --force \
 && rm -rf $CONDA_DIR/share/jupyter/lab/staging \
 && rm -rf /home/$NB_USER/.cache/yarn \
 && rm -rf /home/$NB_USER/.node-gyp \
 && fix-permissions $CONDA_DIR \
 && fix-permissions /home/$NB_USER

# Import matplotlib the first time to build the font cache.
ENV XDG_CACHE_HOME=/home/$NB_USER/.cache/
RUN MPLBACKEND=Agg python -c "import matplotlib.pyplot" \
 && fix-permissions /home/$NB_USER

# ================================================================================
# Python 3.6 environment
# ================================================================================

RUN . $CONDA_DIR/etc/profile.d/conda.sh \
 && conda  create --name python36  -c conda-forge --quiet  --yes \
    'python=3.6' \
    'conda-forge::blas=*=openblas' \
    'ipython' \
    'ipykernel' \
    'ipywidgets' \
    'kernda' \
    'numpy' \
    'scipy' \
    'scikit-learn' \
    'matplotlib' \
    'cython' \
    'numba' \
    'ase' \
    'asap3' \
    'nglview' \
 && conda activate python36 \
 && jupyter nbextension enable --py --sys-prefix widgetsnbextension \
 && jupyter-nbextension enable --py --sys-prefix nglview \
 && conda remove --quiet --yes --force qt pyqt \
 && conda clean -tipsy \
 && fix-permissions $CONDA_DIR \
 && fix-permissions /home/$NB_USER

# Create a global kernelspec in the image and modify it so that it properly activates
# the python36 conda environment.
USER root
RUN . $CONDA_DIR/etc/profile.d/conda.sh \
 && conda activate python36 \
 && ipython kernel install --name python36 --display-name 'Python 3.6'
USER $NB_USER

# https://github.com/ipython-contrib/jupyter_contrib_nbextensions
RUN conda install --quiet --yes \
    'jupyter_contrib_nbextensions' \
    'jupyter_nbextensions_configurator' \
 && conda clean -tipsy \
 && jupyter nbextensions_configurator enable --user \
 && jupyter nbextension install nglview --py --sys-prefix \
 && jupyter nbextension enable nglview --py --sys-prefix \
 && jupyter nbextension enable execute_time/ExecuteTime \
 && jupyter nbextension enable init_cell/main


# ================================================================================
# Switch back to jovyan to avoid accidental container runs as root
WORKDIR $HOME
USER $NB_UID
